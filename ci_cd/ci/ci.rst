.. index::
   pair: Continuous ; Integration
   pair: Continue ; Integration
   ! CI

.. _ci:

==============================
CI (Continuous Integration)
==============================


.. seealso::

   - https://fr.wikipedia.org/wiki/Int%C3%A9gration_continue
   - https://en.wikipedia.org/wiki/Continuous_integration
   - https://docs.gitlab.com/ee/ci/introduction/#continuous-integration
   - https://about.gitlab.com/product/continuous-integration/




Définition
===========

.. seealso::

   - https://fr.wikipedia.org/wiki/Int%C3%A9gration_continue


L'intégration continue est un ensemble de pratiques utilisées en génie
logiciel consistant à vérifier à chaque modification de code source que
le résultat des modifications ne produit pas de régression dans
l'application développée.

Le concept a pour la première fois été mentionné par Grady Booch et se
réfère généralement à la pratique de l'extreme programming.

Le principal but de cette pratique est de détecter les problèmes
d'intégration au plus tôt lors du développement.

De plus, elle permet d'automatiser l'exécution des suites de tests et
de voir l'évolution du développement du logiciel.

L'intégration continue est de plus en plus utilisée en entreprise afin
d'améliorer la qualité du code et du produit final.


Continuous integration
========================

.. seealso::

   - https://en.wikipedia.org/wiki/Continuous_integration


In software engineering, continuous integration (CI) is the practice of
merging all developer working copies to a shared mainline several times
a day.

Grady Booch first proposed the term CI in his 1991 method, although he
did not advocate integrating several times a day.

Extreme programming (XP) adopted the concept of CI and did advocate
integrating more than once per day – perhaps as many as tens of times
per day.


Continuous Integration (gitlab definition)
============================================

.. seealso::

   - https://docs.gitlab.com/ee/ci/introduction/#continuous-integration

Consider an application which has its code stored in a Git repository in
GitLab. Developers push code changes every day, multiple times a day.

For every push to the repository, you can create a set of scripts to build
and test your application automatically, decreasing the chance of
introducing errors to your app.

This practice is known as Continuous Integration; for every change
submitted to an application - even to development branches - it’s built
and tested automatically and continuously, ensuring the introduced
changes pass all tests, guidelines, and code compliance standards you
established for your app.

GitLab itself is an example of using Continuous Integration as a software
development method. For every push to the project, there’s a set of
scripts the code is checked against.
