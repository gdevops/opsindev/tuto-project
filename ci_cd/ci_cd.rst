.. index::
   ! CI/CD

.. _ci_cd:

====================
CI/CD
====================

.. seealso::

   - https://en.wikipedia.org/wiki/CI/CD

.. toctree::
   :maxdepth: 3

   ci/ci
   cd/cd
   gitlab/gitlab
   github/github
