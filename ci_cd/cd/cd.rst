.. index::
   pair: Continuous ; Delivery
   pair: Continu ; Déploiement
   ! CD

.. _cd:
.. _continuous_delivery:

==============================
CD (Continuous Delivery)
==============================

.. seealso::

   - https://continuousdelivery.com/
   - https://martinfowler.com/bliki/ContinuousDelivery.html



Martin Fowler Définition (2013-05)
====================================

Continuous Delivery is a software development discipline where you build
software in such a way that the software can be released to production
at any time.

You’re doing continuous delivery when:

- Your software is deployable throughout its lifecycle
- Your team prioritizes keeping the software deployable over working on
  new features
- Anybody can get fast, automated feedback on the production readiness
  of their systems any time somebody makes a change to them
- You can perform push-button deployments of any version of the software
  to any environment on demand

You achieve continuous delivery by continuously integrating the software
done by the development team, building executables, and running automated
tests on those executables to detect problems.

Furthermore you push the executables into increasingly production-like
environments to ensure the software will work in production.

To do this you use a DeploymentPipeline.
