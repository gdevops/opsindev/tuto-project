.. index::
   pair: Project management ; Github

.. _github:

====================
Github
====================

.. seealso::

   - https://github.com/


.. toctree::
   :maxdepth: 3

   actions/actions
   cli/cli
