.. index::
   pair: Actions ; Github

.. _github_actions:

====================
Github actions
====================

.. seealso::

   - https://docs.github.com/en/actions



Examples
==========

simonwillison
----------------

.. seealso::

   - https://simonwillison.net/2020/Jul/10/self-updating-profile-readme/
   - https://github.com/simonw/simonw/blob/master/.github/workflows/build.yml
   - https://github.com/simonw/simonw/blob/master/build_readme.py
