.. index::
   ! Test Driven Development

.. _tdd:

=================================
Test Driven Development (TDD)
=================================

.. seealso::

   - https://en.wikipedia.org/wiki/Test-driven_development





English definition
===================

Test-driven development (TDD) is a software development process that
relies on the repetition of a very short development cycle: requirements
are turned into very specific test cases, then the software is improved
to pass the new tests, only.


This is opposed to software development that allows software to be
added that is not proven to meet requirements.


Définition en français
=========================

Le Test-Driven Development (TDD), ou développements pilotés par les tests
en français, est une méthode de développement de logiciel qui consiste
à écrire chaque test avant d'écrire le code source d'un logiciel, de
façon itérative.


Processus cyclique de développement
-------------------------------------

Le processus préconisé par TDD comporte cinq étapes :

- écrire un seul test qui décrit une partie du problème à résoudre ;
- vérifier que le test échoue, autrement dit qu'il est valide,
  c'est-à-dire que le code se rapportant à ce test n'existe pas ;
- écrire juste assez de code pour que le test réussisse ;
- vérifier que le test passe, ainsi que les autres tests existants ;
- puis remanier le code, c'est-à-dire l'améliorer sans en altérer le comportement.

Ce processus est répété en plusieurs cycles, jusqu'à résoudre le problème
d'origine dans son intégralité.

Ces cycles itératifs de développement sont appelés des micro-cycles de TDD.
