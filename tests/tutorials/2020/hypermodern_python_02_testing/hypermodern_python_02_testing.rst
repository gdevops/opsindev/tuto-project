
.. index::
   pair: Tests ; Hypermodern Python Chapter 2: Testing

.. _hypermodern_python_testing_2020:

============================================
**Hypermodern Python Chapter 2: Testing**
============================================

.. seealso::

   - https://cjolowicz.github.io/posts/hypermodern-python-02-testing/
   - https://cjolowicz.github.io/posts/hypermodern-python-01-setup
   - https://github.com/cjolowicz/hypermodern-python
   - https://x.com/cjolowicz/



Introduction
==============

.. seealso::

   - https://cjolowicz.github.io/posts/hypermodern-python-01-setup

In this second installment of the Hypermodern Python series, I’m going to discuss
how to add automated testing to your project, and how to teach the random fact
generator foreign languages.

Previously, we discussed How to set up a Python project. (If you start reading here,
you can also download the code for the previous chapter.)
