
.. index::
   pair: Tests ; Django-Styleguide Testing

.. _hacksoftware_testing_2020:

============================================
**HackSoftware Django-Styleguide Testing**
============================================

.. seealso::

   - https://github.com/HackSoftware/Django-Styleguide#testing-1
   - :ref:`testing_django_hacksoft`
