
.. index::
   pair: Tests ; 2016

.. _tests_tutorials_2016:

=================================
Tests tutorials 2016
=================================


.. toctree::
   :maxdepth: 3


   keeping-tests-dry-with-class-based-tests-in-python
