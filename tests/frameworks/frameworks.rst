.. index::
   pair: Tests ; Frameworks

.. _tests_frameworks:

=================================
Tests frameworks
=================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Qualit%C3%A9_logicielle
   - https://en.wikipedia.org/wiki/Software_quality


.. toctree::
   :maxdepth: 3

   pre_commit/pre_commit
   playwright/playwright
   pytest/pytest
   selenium/selenium
