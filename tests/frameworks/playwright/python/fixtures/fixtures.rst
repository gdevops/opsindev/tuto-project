.. index::
   pair: playwright ; Fixtures

.. _playwright_fixtures:

===========================================================
**fixtures for Python playwright python**
===========================================================

.. seealso::

   - https://github.com/microsoft/playwright-pytest#fixtures
   - https://github.com/microsoft/playwright-python
   - https://github.com/microsoft/playwright-pytest

