.. index::
   pair: playwright ; Python

.. _playwright_python:

===========================================================
**playwright for python**
===========================================================

.. seealso::

   - https://playwright.dev/python/docs/why-playwright
   - https://github.com/microsoft/playwright-python/graphs/contributors
   - https://github.com/microsoft/playwright-python
   - https://github.com/microsoft/playwright-pytest
   - https://github.com/microsoft/playwright-pytest#fixtures
   - https://github.com/ThompsonJonM/python-playwright
   - https://thompson-jonm.medium.com/handling-iframes-using-python-and-playwright-da46d1c64196
   - https://thompson-jonm.medium.com/intercepting-network-requests-with-python-and-playwright-7f621ad3935b
   - https://aandryashin.medium.com/playwright-launching-cross-browser-automation-to-the-stars-4a9cca8f0df0
   - https://medium.com/analytics-vidhya/page-object-modeling-with-python-and-playwright-3cbf259eedd3
   - https://medium.com/python-in-plain-english/understanding-browser-tabs-using-python-and-playwright-d81adcad536e
   - https://abstracta.us/blog/test-automation/beginners-guide-functional-test-automation/


.. toctree::
   :maxdepth: 3

   examples/examples
   fixtures/fixtures
   versions/versions
