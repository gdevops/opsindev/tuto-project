"""pw_recherche.py

- python -m playwright codegen http://localhost:8004/
- export MY_IMPRONONCIABLE=

"""
import logging
import os
import sys
import time
from pathlib import Path

from playwright.sync_api import Page
from playwright.sync_api import sync_playwright
from rich import inspect

logger = logging.getLogger(__name__)


def get_login(page: Page):
    # Click text="Login"
    page.click('text="Login"')
    # assert page.url == "http://localhost:8004/"
    # Click input[name="username"]
    page.click('input[name="username"]')
    # Fill input[name="username"]
    page.fill('input[name="username"]', "pvergain")
    # Click input[name="password"]
    page.click('input[name="password"]')
    # Fill input[name="password"]
    try:
        le_mot_quon_ne_prononce_pas = os.environ["MY_IMPRONONCIABLE"]
    except KeyError:
        logger.error(
            "La variable d'environnement 'MY_IMPRONONCIABLE' n'est pas initialisée."
        )
        logger.error("export MY_IMPRONONCIABLE=")
        sys.exit(-1)
    page.fill('input[name="password"]', le_mot_quon_ne_prononce_pas)
    # Click text="Log id3 intranet"
    page.click('text="Log id3 intranet"')
    assert page.url == "http://localhost:8004/"


playwright = sync_playwright().start()
browser = playwright.chromium.launch(headless=False)
context = browser.new_context()
page = context.new_page()
# Go to http://localhost:8004/
page.goto("http://localhost:8004/")
# Click text="Masquer »"
page.click('text="Masquer »"')
get_login(page)
# Click text="Demandes"
page.click('text="Demandes"')
# Click text="Liste de toutes les demandes"
page.click('text="Liste de toutes les demandes"')
assert page.url == "http://localhost:8004/articles/demandes/list/"
# Click input[name="description"]
page.click('input[name="description"]')
# Fill input[name="description"]
page.fill('input[name="description"]', "câble")
# Click input[name="btn_form_filtre"]
page.click('input[name="btn_form_filtre"]')
assert page.url == "http://localhost:8004/articles/demandes/list/"
# Click text="2"
page.fill('input[name="description"]', "câble")
page.goto(
    "http://localhost:8004/articles/demandes/list/?page=1&from_date=1/04/1990&to_date=17/02/2021&type_demande=None&demandeur=&projet=&description=câble&etat_demande=None"
)
rep_demandes = Path.cwd() / "demandes"
if not rep_demandes.exists():
    rep_demandes.mkdir()
page.screenshot(path=rep_demandes / "page_1_cable.png")
print(page.url)
time.sleep(3)
page.goto(
    "http://localhost:8004/articles/demandes/list/?page=5&from_date=1/04/1990&to_date=17/02/2021&type_demande=None&demandeur=&projet=&description=câble&etat_demande=None"
)
page.screenshot(path=rep_demandes / "page_5_cable.png")
time.sleep(3)
browser.close()
playwright.stop()
