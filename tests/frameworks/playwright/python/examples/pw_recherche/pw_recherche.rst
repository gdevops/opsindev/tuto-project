
.. _playwright_pw_recherche:

===========================================================
**Module Python pw_recherche.py**
===========================================================

.. seealso::

   - https://playwright.dev/python/docs/why-playwright
   - https://github.com/microsoft/playwright-python/graphs/contributors
   - https://github.com/microsoft/playwright-python


.. literalinclude:: pw_recherche.py
   :linenos:


.. figure:: demandes/page_1_cable.png
   :align: center


.. figure:: demandes/page_5_cable.png
   :align: center
