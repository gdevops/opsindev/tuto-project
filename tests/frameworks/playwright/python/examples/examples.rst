
.. _playwright_python_examples:

===========================================================
**playwright for python examples**
===========================================================

.. seealso::

   - https://playwright.dev/python/docs/why-playwright
   - https://github.com/microsoft/playwright-python/graphs/contributors
   - https://github.com/microsoft/playwright-python

.. toctree::
   :maxdepth: 3

   pw_recherche/pw_recherche
