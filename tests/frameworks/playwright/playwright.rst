.. index::
   pair: playwright ; Framework
   ! playwright

.. _playwright:

=================================================
**playwright** end-to-end testing for the web
=================================================

.. seealso::

   - https://playwright.dev/python/docs/why-playwright
   - https://github.com/mxschmitt/awesome-playwright
   - https://github.com/search?q=playwright
   - https://github.com/microsoft/playwright-python
   - https://github.com/microsoft/playwright-pytest
   - https://github.com/microsoft/playwright-pytest#fixtures
   - https://x.com/playwrightweb

.. figure:: logo_playwright.png
   :align: center


.. toctree::
   :maxdepth: 3

   definition/definition
   python/python
   ci/ci
