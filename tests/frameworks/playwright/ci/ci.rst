.. index::
   pair: playwright ; CI

.. _playwright_ci:

=================================
playwright CI
=================================

.. seealso::

   - https://playwright.dev/#version=v1.8.1
   - https://playwright.dev/#path=docs%2Fci.md&q=

