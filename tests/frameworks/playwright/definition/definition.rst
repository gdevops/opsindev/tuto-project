

.. _playwright_def:

=================================
playwright definition
=================================

.. seealso::

   - https://playwright.dev/python/docs/why-playwright
   - https://github.com/microsoft/playwright-python

.. toctree::
   :maxdepth: 3

   microsoft/microsoft
   programmez/programmez
