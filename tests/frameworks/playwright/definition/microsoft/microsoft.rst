

.. _playwright_def_microsoft:

===========================================================================================
2020-09-30 Announcing **Playwright for Python: Reliable end-to-end testing for the web**
===========================================================================================

.. seealso::

   - https://devblogs.microsoft.com/python/announcing-playwright-for-python-reliable-end-to-end-testing-for-the-web/
   - https://aandryashin.medium.com/playwright-launching-cross-browser-automation-to-the-stars-4a9cca8f0df0
   - https://x.com/arjunattam
   - https://github.com/marketplace/actions/run-playwright-tests




Introduction
=============


**Automated end-to-end tests are a powerful tool for your team to ship faster
and with more confidence**.

End-to-end tests automate UI interactions and can validate the functionality
of your applications.

To this end, we are announcing `Playwright for Python <https://github.com/microsoft/playwright-python>`_
in preview today.

Playwright enables developers and testers to write reliable end-to-end
tests in Python. Get started by installing Playwright from PyPI.

With the Playwright API, you can author end-to-end tests that run on all
modern web browsers. Playwright delivers automation that is faster, more
reliable and more capable than existing testing tools.

Automated end-to-end tests have become more important than ever before.
Teams are shipping faster and building apps that run on a growing set
of devices. This increase in velocity and web targets puts immense
pressure on the testing process, and automation is critical.

While automation is important, end-to-end tests are prone to being slow
and flaky. To fix this, we released `Playwright in JavaScript <https://github.com/microsoft/playwright>`_
earlier this year and have enabled thousands of developers and testers
to be successful at end-to-end testing.

**Today, we’re bringing the same capabilities to Python**.


How is Playwright different ?
================================

.. seealso::

   - https://whatwebcando.today/
   - https://playwright.dev/#path=docs%2Factionability.md&q=
   - https://playwright.dev/#path=docs%2Femulation.md&q=

1. Playwright delivers reliable, timeout-free automation
---------------------------------------------------------

Modern web apps are rich and responsive, issuing network requests and
DOM changes based on user interactions.

This asynchronous behavior makes modern apps harder to predictably automate.

Traditional automated tests rely on sleep timeouts to manage this
complexity, but timeouts often lead to unpredictable failures.

Playwright `automatically waits for the UI <https://playwright.dev/#path=docs%2Factionability.md&q=>`_
to be ready, which ensures tests are reliable to execute and simpler to author.

Under the hood, Playwright uses an **event-driven architecture** that can
listen to precise browser events like DOM changes, network requests and
page navigations.

2. Playwright is built for the modern web
---------------------------------------------

The web platform is `continuously evolving`_ and adding newer capabilities
every year.

Playwright is built to automate newer web features, including `emulation 
of mobile viewports <https://playwright.dev/#path=docs%2Femulation.md&q=>`_,
geolocation and web permissions.

Playwright scripts can `even intercept and modify network activity <https://playwright.dev/#path=docs%2Fnetwork.md&q=>`_
and automate scenarios across multiple pages.


.. _`continuously evolving`: https://whatwebcando.today/

3. Playwright works on all modern browsers
--------------------------------------------

With Playwright, you can author automated tests for all modern browser
engines: Chromium (for Google Chrome and the new Microsoft Edge), 
WebKit (for Apple Safari) and Mozilla Firefox.

WebKit is supported on all platforms, which enables you to test rendering
on Safari, even on Windows and Linux machines.


Use Playwright with pytest
===============================

With our pytest plugin for Playwright, you can get started easily with
end-to-end testing. To install Playwright, the plugin, and the browsers
to test on, run::

    pip install playwright pytest-playwright
    python -m playwright install

This plugin configures pytest fixtures that provide building blocks you
need for end-to-end browser testing.

For example, the page fixture provides a new web page to run a test.
For a complete list of fixtures, see `plugin docs <https://github.com/microsoft/playwright-pytest#fixtures>`_.


.. code-block:: python

    from playwright.sync_api import Page

    def test_example_is_working(page: Page):
        page.goto('https://example.com')
        assert page.innerText('h1') == 'Example Domain'


.. _use_playwright_with_django:

Use Playwright with Django
==============================

You can use Playwright to test views in Django web apps.

To install Playwright, and the browsers to test on, run::

    pip install playwright
    python –m playwright install

**Playwright integrates with the built-in testing tools in Django.**

Specifically, you can use the LiveServerTestCase class to launch a live
Django server and run browser tests against it.

.. code-block:: python
   :linenos:

    from django.contrib.staticfiles.testing import StaticLiveServerTestCase
    from playwright import sync_playwright

    class MyViewTests(StaticLiveServerTestCase):
        @classmethod
        def setUpClass(cls):
            super().setUpClass()
            cls.playwright = sync_playwright().start()
            cls.browser = cls.playwright.chromium.launch()

        @classmethod
        def tearDownClass(cls):
            cls.browser.close()
            cls.playwright.stop()
            super().tearDownClass()

        def test_login(self):
            page = self.browser.newPage()
            page.goto('%s%s' % (self.live_server_url, '/login/'))
            page.fill('#username', 'myuser')
            page.fill('#password', 'secret')
            page.click('text=Log in')
            assert page.url == '%s%s' % (self.live_server_url, '/profile/')
            page.close()

See other examples
--------------------

.. seealso::

   - :ref:`playwright_python_examples`


Deploy Playwright tests to CI/CD
===================================

.. seealso::

   - https://github.com/marketplace/actions/run-playwright-tests
   - https://playwright.dev/#path=docs%2Fci.md&q=
   - https://github.com/microsoft/playwright-python
   - https://github.com/microsoft/playwright-python/issues

Running end-to-end tests in your CI/CD pipelines helps catch issues
early.

You can deploy Playwright tests to CI/CD with the `Playwright GitHub Action <https://github.com/marketplace/actions/run-playwright-tests>`_
or with tools for other `CI/CD providers <https://playwright.dev/#path=docs%2Fci.md&q=>`_.

`Playwright for Python <https://github.com/microsoft/playwright-python>`_
is built in the open on GitHub, and we are eager to learn more on how
Playwright works for you.

Feel free to share feedback or feature requests on GitHub issues or join
the Playwright Slack community to connect with other users.



