

.. _playwright_def_programmez:

==========================================================================
2020-10-02 à 13:34, définition par https://python.developpez.com
==========================================================================

.. seealso::

   - https://python.developpez.com/actu/309353/Microsoft-annonce-Playwright-pour-le-langage-Python-permettant-de-tester-les-applications-Web-et-qui-fonctionne-dans-tous-les-principaux-navigateurs/


Microsoft a annoncé mercredi qu’il étendait son outil de test d’applications
Web Playwright du langage de programmation JavaScript à Python.

Il avait déjà étendu l’outil à la prise en charge du langage C# plus tôt
cette année.

Playwright est une bibliothèque permettant d'automatiser les tests des
applications Web dans Chrome, Firefox, Edge et WebKit avec une seule API.

Selon la documentation de l’outil, Playwright est conçu pour permettre
une automatisation Web entre les navigateurs, qui soit toujours verte,
performante, fiable et rapide.

«Les tests automatisés de bout en bout sont un outil puissant qui permet
à votre équipe d'expédier plus rapidement et avec plus de confiance.
Ils automatisent les interactions de l'interface utilisateur et peuvent
valider la fonctionnalité de vos applications »,

a déclaré Arjun de Microsoft dans un billet de blogue mercredi.

C’est pour cette raison que l’entreprise annonce en avant-première la
sortie de Playwright pour Python.

Playwright permet aux développeurs et aux testeurs d'écrire des tests
fiables de bout en bout en Python.

Playwright peut être installé à partir de PyPI. Voici ci-dessous quelques
caractéristiques de l’outil, selon Microsoft.

Playwright offre une automatisation fiable et sans perte de temps

Selon l’entreprise, les applications Web modernes sont riches et réactives,
cela en émettant des requêtes sur le réseau et des modifications de DOM
en fonction des interactions de l'utilisateur.

Ce comportement asynchrone les rend plus difficiles à automatiser de manière
prévisible.
Ainsi, les tests automatisés traditionnels reposent sur des temps d'arrêt
pour gérer cette complexité, mais d’après Microsoft, ces temps d’arrêt
entraînent souvent des défaillances imprévisibles.

Pour éviter ce problème, Playwright attend automatiquement que l'interface
utilisateur soit prête.

Selon l'entreprise, cela garantit que les tests sont plus simples à créer
et plus fiables pour être exécutés.

Sous le capot, Playwright utilise une architecture événementielle qui
lui permet d’écouter des événements précis du navigateur comme les
changements de DOM, les requêtes réseau et les navigations de pages.



