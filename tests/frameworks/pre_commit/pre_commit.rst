.. index::
   pair: pre-commit ; framework

.. _project_pre_commit:

===========================================================================================
**pre-commit** (a framework for managing and maintaining multi-language pre-commit hooks)
===========================================================================================

.. seealso::

   - :ref:`python_pre_commit_tool`
