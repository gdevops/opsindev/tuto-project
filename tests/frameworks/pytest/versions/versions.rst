.. index::
   pair: pytest ; Versions


.. _pytest_versions:

=================================
pytest versions
=================================

.. seealso::

   - https://github.com/pytest-dev/pytest/releases


.. toctree::
   :maxdepth: 3


   5.0.0/5.0.0
