.. index::
   pair: pytest ; Framework
   ! pytest

.. _pytest:

=================================
pytest
=================================

.. seealso::

   - https://github.com/pytest-dev/pytest


.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
   plugins/plugins
