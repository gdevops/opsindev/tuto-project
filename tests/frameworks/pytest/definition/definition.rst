

.. _pytest_def:

=================================
pytest definition
=================================

.. seealso::

   - https://github.com/pytest-dev/pytest




Description
============

The pytest framework makes it easy to write small tests, yet scales to
support complex functional testing.
