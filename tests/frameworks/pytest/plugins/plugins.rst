.. index::
   pair: pytest ; plugins
   ! pytest plugins

.. _pytest_plugins:

=================================
pytest plugins
=================================

.. seealso::

   - https://github.com/pytest-dev/pytest
   - https://docs.pytest.org/en/latest/plugins.html


.. toctree::
   :maxdepth: 3


   pytest_django/pytest_django
   pytest_cov/pytest_cov
   pytest_emoji/pytest_emoji
