.. index::
   pair: pytest-cov ; plugin
   pair: coverage ; plugin


.. _pytest_emoji:

==========================================
pytest-emoji  😃
==========================================

.. seealso::

   - https://github.com/hackebrot/pytest-emoji
   - https://www.unicode.org/emoji/charts/index.html
   - https://www.unicode.org/press/emoji.html


.. figure:: 1f60d.png
   :align: center

   https://www.unicode.org/emoji/charts/emoji-list.html#1f60d





Description
=============

A pytest plugin that adds emojis to your test result report 😃 .


Links
======

.. seealso::

   - https://www.unicode.org/emoji/charts/index.html
   - https://www.unicode.org/press/emoji.html
   - https://github.com/bsolomon1124/demoji


emojify
--------

.. seealso::

   - https://github.com/mrowa44/emojify


::

    emojify "Hey, I just :raising_hand: you, and this is :scream: , but here's my :calling: , so :telephone_receiver: me, maybe?"

::

    Hey, I just 🙋  you, and this is 😱  , but here's my 📲  , so 📞  me, maybe?
