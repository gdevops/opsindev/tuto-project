.. index::
   pair: pytest-cov ; plugin
   pair: coverage ; plugin


.. _pytest_cov:

==========================================
pytest-cov (Coverage plugin for pytest)
==========================================

.. seealso::

   - https://github.com/pytest-dev/pytest
   - https://github.com/pytest-dev/pytest-cov
   - https://en.wikipedia.org/wiki/Code_coverage
   - https://fr.wikipedia.org/wiki/Couverture_de_code





Description
=============

This plugin produces coverage reports.


Coverage
=========

In computer science, test coverage is a measure used to describe the
degree to which the source code of a program is executed when a particular
test suite runs.

A program with high test coverage, measured as a percentage, has had
more of its source code executed during testing, which suggests it has
a lower chance of containing undetected software bugs compared to a
program with low test coverage.[1][2] Many different metrics can be
used to calculate test coverage; some of the most basic are the percentage
of program subroutines and the percentage of program statements called
during execution of the test suite.

Test coverage was among the first methods invented for systematic
software testing.

En français
============

En génie logiciel, la couverture de code est une mesure utilisée pour
décrire le taux de code source exécuté d'un programme quand une suite
de test est lancée.
Un programme avec une haute couverture de code, mesurée en pourcentage,
a davantage de code exécuté durant les tests ce qui laisse à penser
qu'il a moins de chance de contenir de bugs logiciels non détectés,
comparativement à un programme avec une faible couverture de code.

Différentes métriques peuvent être utilisées pour calculer la couverture
de code ; les plus basiques sont le pourcentage de sous routine et le
pourcentage d'instructions appelées durant l'exécution de la suite de test.

La mesure de ce taux implique souvent l'utilisation de tests unitaires.

Usage
======

.. seealso::

   - :ref:`django_conftest`


::

    pipenv shell
    pytest --cov="." --cov-report=html  --emoji

::

    platform linux -- Python 3.7.2, pytest-4.3.1, py-1.8.0, pluggy-0.9.0
    Django settings: config.settings.development (from ini file)
    rootdir: /home/pvergain/projects/intranet/intranet/docker-django/intranet, inifile: pytest.ini
    plugins: emoji-0.2.0, django-3.4.8, cov-2.6.1
    collected 41 items


    accounts/tests.py 😃                                                                                                                                                                                                                   [  2%]
    articles/management/commands/test_clean_id3_line.py 😃                                                                                                                                                                                 [  4%]
    articles/tests/tests.py 😃 😃 😃 😃                                                                                                                                                                                                    [ 14%]
    articles/tests/tests_article_achat.py 😃                                                                                                                                                                                               [ 17%]
    articles/tests/tests_articles.py 😃                                                                                                                                                                                                    [ 19%]
    articles/tests/tests_codif_article.py 😃                                                                                                                                                                                               [ 21%]
    articles/tests/tests_media_fichier.py 😃 😃                                                                                                                                                                                            [ 26%]
    articles/tests/tests_order.py 😃                                                                                                                                                                                                       [ 29%]
    articles/tests/tests_url.py 😃                                                                                                                                                                                                         [ 31%]
    documents/tests.py 😃                                                                                                                                                                                                                  [ 34%]
    employes/tests/tests.py 😃                                                                                                                                                                                                             [ 36%]
    fiches_temps/tests/test_create_fiches_calcul.py 😃 😃 😃                                                                                                                                                                               [ 43%]
    fiches_temps/tests/test_form_fdt_calcul.py 😃                                                                                                                                                                                          [ 46%]
    fiches_temps/tests/test_select_fiches.py 😃 😃                                                                                                                                                                                         [ 51%]
    fiches_temps/tests/tests_fdt.py 😃 😃 😃 😃 😃 😃 😃 😃 😃 😃 😃 😃 😃 😃                                                                                                                                                              [ 85%]
    fiches_temps/tests/tests_fdt_time.py 😃                                                                                                                                                                                                [ 87%]
    fiches_temps/tests/tests_pendulum.py 😃                                                                                                                                                                                                [ 90%]
    tiers/test_tiers.py 😃 😃 😃 😃                                                                                                                                                                                                        [100%]

    ----------- coverage: platform linux, python 3.7.2-final-0 -----------
    Coverage HTML written to dir htmlcov
