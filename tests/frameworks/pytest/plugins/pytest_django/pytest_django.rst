.. index::
   pair: pytest-django ; plugin
   pair: pytest ; conftest.py
   pair: pytest ; PostgreSQL


.. _pytest_django:

=================================
pytest-django
=================================

.. seealso::

   - https://github.com/pytest-dev/pytest
   - https://github.com/pytest-dev/pytest-django
   - https://pytest-django.readthedocs.io/en/latest/





Description
=============

**pytest-django** is a plugin for pytest that provides a set of useful
tools for testing Django applications and projects.



.. _django_conftest:

conftest.py example
=====================


We want to test a staging database
--------------------------------------

.. code-block:: python
   :linenos:

    """conftest.py


    For information, we could also use this code::

        os.environ[
            "DATABASE_URL"
        ] = "postgresql://<username>:<password>@W.X.Y.Z:5432/db_XXXX"
        DATABASES = {"default": env.db("DATABASE_URL")}


        # on corrige par anticipation car django-environ fait pointer sur
        # 'django.db.backends.postgresql_psycopg2' au lieu de 'django.db.backends.postgresql'
        # A ce jour (2017-12-01 les 2 modules existent.
        # https://docs.docker.com/compose/django/#connect-the-database

        DATABASES["default"]["ENGINE"] = "django.db.backends.postgresql"


    Calling pytest::

        pytest --cov='.'

    """

    import pytest
    from django.conf import settings


    @pytest.fixture(scope="session")
    def django_db_setup():

        settings.DATABASES["default"] = {
            "ENGINE": "django.db.backends.postgresql",
            # Test database
            "HOST": "W.X.Y.Z",
            "NAME": "db_XXXX",
        }
