.. index::
   pair: selenium ; webdriver
   ! webdriver

.. _webdriver:

=================================================================================================
**webdriver** (Remote control interface that enables introspection and control of user agents)
=================================================================================================

.. seealso::

   - https://github.com/w3c/webdriver
   - https://w3c.github.io/webdriver/
   - https://www.selenium.dev/documentation/en/webdriver/
   - https://www.w3.org/TR/webdriver1/
   - https://github.com/w3c/webdriver-bidi



Definition
==============

WebDriver drives a browser natively, as a user would, either locally or
on a remote machine using the Selenium server, marks a leap forward in
terms of browser automation.

Selenium WebDriver refers to both the language bindings and the implementations
of the individual browser controlling code.

This is commonly referred to as just WebDriver.

Selenium WebDriver is a `W3C Recommendation <https://www.w3.org/TR/webdriver1/>`_

- WebDriver is designed as a simple and more concise programming interface.
- WebDriver is a compact object-oriented API.
- It drives the browser effectively.

