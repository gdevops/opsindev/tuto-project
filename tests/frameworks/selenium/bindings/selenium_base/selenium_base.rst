.. index::
   ! SeleniumBase


.. _seleniumbase:

================================================================================
**SeleniumBase**
================================================================================

.. seealso::

   - https://seleniumbase.io/
   - https://github.com/seleniumbase/SeleniumBase
   - https://github1s.com/seleniumbase/SeleniumBase


.. figure:: selenium_base.png
   :align: center

.. toctree::
   :maxdepth: 3

   description/description
   examples/examples
