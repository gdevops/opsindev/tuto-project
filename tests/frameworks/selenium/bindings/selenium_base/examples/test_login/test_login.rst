
.. _seleniumbase_test_login:

================================================================================
**test_login**
================================================================================

.. seealso::

   - https://github1s.com/seleniumbase/SeleniumBase/blob/master/examples/test_login.py




examples/test_login.py
=========================


.. literalinclude:: test_login.py
   :linenos:
