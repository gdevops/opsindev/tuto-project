
.. _seleniumbase_image_test:

================================================================================
**image_test**
================================================================================

.. seealso::

   - https://github1s.com/seleniumbase/SeleniumBase/blob/master/examples/image_test.py




Launch tests : **pytest image_test.py --gui**
===============================================

::

    ❯ pytest image_test.py --gui

::

    =============================================================================================== test session starts ===============================================================================================
    platform linux -- Python 3.9.1, pytest-6.2.2, py-1.10.0, pluggy-0.13.1
    rootdir: /home/pvergain/projets/intranet/tests/selenium_base/examples, configfile: pytest.ini
    plugins: xdist-2.2.1, cov-2.11.1, allure-pytest-2.8.33, ordering-0.6, rerunfailures-9.1.1, forked-1.3.0, seleniumbase-1.55.2, html-2.0.1, metadata-1.11.0
    collected 4 items

    image_test.py "images_exported/page_overlay.png" has been saved!
    ."images_exported/image_overlay.png" has been saved!
    ."images_exported/section_overlay.png" has been saved!
    ."images_exported/comic.png" has been saved!


tree -L 3 images_exported/
------------------------------

::

    ❯ tree -L 3 images_exported/

::


    images_exported/
    ├── comic.png
    ├── image_overlay.png
    ├── page_overlay.png
    └── section_overlay.png

    0 directories, 4 files


examples/image_test.py
=========================


.. literalinclude:: image_test.py
   :linenos:
