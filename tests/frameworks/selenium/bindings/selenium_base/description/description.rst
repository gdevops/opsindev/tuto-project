
.. _seleniumbase_description:

================================================================================
SeleniumBase ⛲ Features: 🗂️
================================================================================

.. seealso::

   - https://seleniumbase.io/help_docs/features_list/



⛲ Features: 🗂️
==================

- A complete test automation framework for web/mobile UI testing.
- Supports pytest, unittest, and nose for test discovery and running.
- No more flaky tests! (Smart-waiting methods make tests reliable.)
- Powerful console scripts. (Type seleniumbase or sbase to use.)
- Has the ability to translate tests into multiple spoken languages.
- Has a flexible command-line interface for customizing test runs.
- Includes logging tools for dashboards, reports, and screenshots.
- Has a global config file for configuring settings as needed.
- Includes a tool for creating interactive web presentations.
- Includes Chart Maker, a tool for creating interactive charts.
- Includes a website tour builder for creating interactive walkthroughs.
- Has a tool to export Katalon Recorder scripts into SeleniumBase format.
- Can run tests in Headless Mode to hide the browser. (Use --headless)
- Can run tests multithreaded from parallel browsers. (Use -n NUM_THREADS)
- Can reuse the same browser session between tests. (Use: --reuse-session)
- Can run tests using Chrome's mobile device emulator. (Use --mobile)
- Can run tests through a proxy server. (Use --proxy=IP_ADDRESS:PORT)
- Can use an authenticated proxy server. (--proxy=USER:PASS@IP_ADDRESS:PORT)
- Can change the web browser's user agent string. (Use --agent=USER_AGENT_STRING)
- Can load Chrome Extension ZIP files. (Use --extension_zip=ZIP)
- Can load Chrome Extension folders. (Use --extension_dir=DIR)
- Can set a Chrome User Data Directory / Profile to load. (Use --user_data_dir=DIR)
- Includes a hybrid-automation solution called MasterQA to speed up manual testing.
- Can handle Google Authenticator logins with Python's one-time password library.
- Is backwards-compatible with Python WebDriver methods. (Use: self.driver)
- Can execute JavaScript code from Python calls. (Use: self.execute_script())
- Includes integrations for MySQL, Selenium Grid, Azure, GCP, AWS, and Docker.
- Includes a tool for converting Selenium IDE recordings into SeleniumBase scripts.
- Can load and make assertions on PDF files from websites or the local file system.
- Includes useful Python decorators and password obfuscation methods.
