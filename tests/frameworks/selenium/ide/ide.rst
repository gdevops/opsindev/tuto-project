.. index::
   pair: selenium ; IDE
   ! selenium IDE

.. _selenium_ide:

================================================================================
**selenium IDE** Open Source record and playback test automation for the web.
================================================================================

.. seealso::

   - https://www.selenium.dev/selenium-ide/
   - https://github.com/seleniumhq/selenium-ide
   - https://github1s.com/seleniumhq/selenium-ide


.. figure:: selenium_ide.png
   :align: center

   https://www.selenium.dev/selenium-ide/


.. toctree::
   :maxdepth: 3

   cli/cli
   doc/doc
   firefox/firefox
