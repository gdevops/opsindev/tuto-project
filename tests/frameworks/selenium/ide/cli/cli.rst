.. index::
   CLI; Selenium IDE

.. _selenium_ide_cli:

=======================================
**selenium IDE Command-line Runner**
=======================================

.. seealso::

   - https://www.selenium.dev/selenium-ide/docs/en/introduction/command-line-runner



Command-line Runner
=====================

You can now run all of your Selenium IDE tests on any browser, in parallel,
and on a Grid without needing to write any code.

There's just the small matter of installing the Selenium IDE command line
runner, getting the necessary browser drivers (if running your tests locally),
and launching the runner from a command prompt with the options you want.


Prerequisites
===============

The following dependencies are needed for the command line runner to work:

- node (the Node.js programming language) version 8 or 10
- npm (the NodeJS package manager) which typically gets installed with node
- selenium-side-runner (the Selenium IDE command line runner)
- and the browser driver we want to use (more on that in the next section)
