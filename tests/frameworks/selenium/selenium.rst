.. index::
   pair: selenium ; Framework
   ! selenium

.. _selenium:

=================================
**selenium**
=================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Selenium_(informatique)
   - https://x.com/SeleniumHQ
   - https://www.selenium.dev/
   - https://www.selenium.dev/documentation/en/front_matter/copyright_and_attributions/


.. figure:: Selenium.png
   :align: center


.. toctree::
   :maxdepth: 3

   definition/definition
   doc/doc
   bindings/bindings
   ide/ide
   web_driver/web_driver
