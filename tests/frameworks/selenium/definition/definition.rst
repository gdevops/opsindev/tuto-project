

.. _selenium_def:

=================================
Definitions
=================================


.. seealso::

   - https://seleniumbase.io/integrations/katalon/ReadMe/




Definition wikipedia
======================


.. seealso::

   - https://fr.wikipedia.org/wiki/Selenium_(informatique)


Selenium est un framework de test informatique développé en Java.

Il permet d'interagir avec différents navigateurs web tel que Google
Chrome grâce au chromedriver ou Mozilla Firefox avec Gecko de même que
le ferait un utilisateur de l'application.

Il entre ainsi dans la catégorie des outils de test dynamique (à l'inverse
des tests statiques qui ne nécessitent pas l'exécution du logiciel)
facilitant le **test fonctionnel**.

Il est associé à:

- Selenium IDE, extension Firefox, pour l'utiliser ;
- Selenium WebDriver, successeur de Selenium Remote Control (RC) devenu
  depuis officiellement obsolète.
  Il permet d'écrire des tests automatisés en différents langages
  (PHP, Python, Ruby, .NET, Perl, Java, Rust et enfin Go).

Exemple en Rust
==================


.. code-block:: rust

    // ici on déclare le packet selenium_webdriver.
    use selenium_webdriver::*;
    // Le point de départ

    fn main() {
        // ici ça permet uniquement de créé une session et le navigateur indiqué est Chrome.
        let mut driver = Browser::start_session(BrowserName::Chrome, "--disable-popup-blocking", "--disable-extensions");
        // selenium va s'en chargé d'ouvrir une nouvelle page.
        driver.open("https://www.wikipedia.org/").unwrap();
        // ça permet uniquement de rechercher la barre de recherche.
        let search = driver.find_element(LocatorStrategy::CSS("#searchInput" as &'static str)).unwrap();
        // ça permet uniquement de taper Selenium (informatique) dans la barre de recherche.
        let _ = search.send_keys(&"Selenium (informatique)");
        // selenium va chercher le bouton.
        let btn = driver.find_element(LocatorStrategy::CSS("input[type=submit]" as &'static str)).unwrap();
        // et il va clicker !
        btn.click();
    }
