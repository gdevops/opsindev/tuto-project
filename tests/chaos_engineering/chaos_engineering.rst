.. index::
   pair: Chaos ; engineering

.. _chaos:

==============================
Chaos engineering
==============================

.. seealso::

   - https://fr.wikipedia.org/wiki/Chaos_Monkey
   - http://principlesofchaos.org/




Chaos Engineering
===================

::

    Chaos Engineering est la discipline de l'expérimentation sur un
    système distribué afin de renforcer la confiance dans la capacité
    du système à résister à des conditions turbulentes en production.

Il s'agit d'une communauté construite autour des principes définit sur
le site http://principlesofchaos.org/, initiée par Netflix.


Chaos Monkey et Devops
========================


Dans le cadre du mouvement Devops, une attention particulière est portée
à la sûreté de fonctionnement des systèmes informatiques permettant ainsi
d'apporter un niveau de confiance suffisant malgré les mises en production
fréquentes.

En contribuant à la Chaîne d'outils Devops, les Chaos Monkey répondent
au besoin de tests continus.

Ils s'inscrivent dans le modèle Design for failure 10,
« conçu pour supporter la défaillance » : une application informatique
doit être capable de supporter la panne de n’importe quel composant
logiciel ou matériel sous-jacent.
