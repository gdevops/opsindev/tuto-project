.. index::
   ! Tests

.. _tests:

======================
Tests
======================

.. seealso::

   - https://x.com/ministryoftest
   - https://x.com/FriendlyTester


.. figure:: dimensions_tests.jpeg
   :align: center

   https://x.com/FriendlyTester/status/1362629503248445440?s=20


.. figure:: Dev-Ops2_english.png
   :align: center

   https://danashby.co.uk/2016/10/19/continuous-testing-in-devops/


.. toctree::
   :maxdepth: 4

   chaos_engineering/chaos_engineering
   frameworks/frameworks
   tdd/tdd
   tutorials/tutorials
