

.. _method_pdd_ddd:

=====================================================================================================================
I believe in prototype driven development (PDD) with a focus on data driven design (DDD) by William (Bill) Kennedy
=====================================================================================================================

- https://x.com/goinggodotnet
- https://x.com/goinggodotnet/status/1570764101097500674?s=20&t=hx1jFYvU4VGleju4x_r_Ag


I've never kept it a secret that I'm not a fan of TDD.

I believe in prototype driven development (PDD) with a focus on data
driven design (DDD)

**I believe the general engineering cycle should be prototype, solidify,
refactor, test, minimize, reduce, and simplify in that order**.

