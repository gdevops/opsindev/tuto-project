.. index::
   pair: gitlab ; GitLab 17.7 GitLab 17.7 released with new Planner user role  (2024-12-19)

.. _gitlab_17_7_2024_12_19:

============================================================================
2024-12-19  GitLab 17.7 GitLab 17.7 released with new Planner user role 
============================================================================

- https://about.gitlab.com/releases/2024/12/19/gitlab-17-7-released/



