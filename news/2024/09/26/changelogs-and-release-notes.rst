
.. _changelogs_2024_09_26:

================================================
2024-09-26 **Changelogs and Release Notes**
================================================

- https://harihareswara.net/posts/2024/changelogs-and-release-notes/


My friend Ned Batchelder posted, `"A list of commits is not a changelog!" <https://hachyderm.io/@nedbat/113152493265812268>`_ 
and spurred this post.

Summary
===========

We'd all benefit from restoring the distinction between a detailed 
changelog and brief release notes, but that's hard to do if a project relies 
solely on GitHub as its communication platform.

Context
===========

Ned maintains and uses open source software. He was talking to 
fellow open source maintainers, as am I.

Wording: 
=============

I'm going to assume you know what open source software is, and understand 
what I mean when I use terms such as "commit" (the noun), "mailing list," 
"issue", "deprecation", and "beta" in this context.

Where I'm coming from: I have a lot of advice for maintainers on how to 
communicate with their users. I think changelogs suit some of those 
communication needs and not others. 

In my guide "Marketing, Publicity, Roadmaps, and Comms" I discuss

- your audiences and what you need to tell them
- the roles of roadmaps, mailing lists, blogs, mailing lists, 
  in-application notifications, group chats, videos, and more (with links to examples)
- a sample schedule for what to do 1 month before a release, 1 week before, 
  at release, and one month afterwards

So I'm thinking of changelogs in that context. 

A maintainer can use several announcement and documentation tools to 
systematically communicate to their users, upstreams, and other interested 
people about new releases, new features, deprecations, and so on. 

And one of them is the changelog.

Release notes
=================

- https://blog.zulip.com/2024/07/25/zulip-9-0-released/
- https://release.gnome.org/47/developers/index.html

We used to have a clear division, in open source software documentation, 
between changelogs and release notes. 

Release notes are a prose summary of what's changed, which exist in 
addition to a changelog (the release notes might link to the changelog 
or include a copy at the bottom), and which focus on changes the user might perceive.

`Zulip's 9.0 announcement <https://blog.zulip.com/2024/07/25/zulip-9-0-released/>`_ is a good example; scroll to "Release highlights". 

Note that the medium (a blog post) gives author Tim Abbott room to give 
context, share screenshots, connect this release to Zulip's product 
roadmap, ask for feedback, and so on. 

This approach makes sense for consumer-facing projects such as GUI applications. 

Another example: GNOME, which published separate user-facing and developer-facing 
`release notes for GNOME 47 ("Denver") as web pages <https://release.gnome.org/47/developers/index.html>`_.


Conclusion
=============

- https://docs.divio.com/documentation-system/
- https://signal.org/blog/the-ecosystem-is-moving/

The difference between a changelog and release notes is the difference 
between a "reference" and an "explanation" in `Divio's "four kinds of documentation" 
framework <https://docs.divio.com/documentation-system/>`_. 

A changelog, like an inventory of available API methods, is comprehensive, 
reliably consistent, and uses section headings and other formatting to 
be skimmable. 

A set of release notes, like a `"why we made this controversial architectural 
choice" blog post <https://signal.org/blog/the-ecosystem-is-moving/>`_, is distilled, often narrative, and uses brevity and 
focus on the reader's priorities to be quickly readable. 

In Heidi Waterhouse's words, it can be a derivation of the decision log.



