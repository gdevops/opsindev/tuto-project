.. index::
   pair: gitlab ; 17.2 (2024-07-18)

.. _gitlab_17_2_2024_07_18:

=====================================
2024-07-18 **GitLab 17.2 Release**
=====================================


Summary
=========

GitLab 17.2 released with log streaming, a new pipeline execution security policy, 
and vulnerability explanations now generally available 


Self-managed, free
===================

Log streaming for Kubernetes pods and containers
-------------------------------------------------

- https://about.gitlab.com/releases/2024/07/18/gitlab-17-2-released/#log-streaming-for-kubernetes-pods-and-containers
- https://docs.gitlab.com/ee/ci/environments/kubernetes_dashboard.html


In GitLab 16.1, we introduced the Kubernetes pod list and detail views. 

However, you still had to use third-party tools for an in-depth analysis 
of your workloads. 

GitLab now ships with a log streaming view for pods and containers, so 
you can quickly check and troubleshoot issues across your environments 
without leaving your application delivery tool

OAuth 2.0 device authorization grant support
------------------------------------------------

- https://datatracker.ietf.org/doc/html/rfc8628
- https://about.gitlab.com/releases/2024/07/18/gitlab-17-2-released/#oauth-20-device-authorization-grant-support
- https://docs.gitlab.com/ee/api/oauth2.html#device-authorization-grant-flow

GitLab now supports the OAuth 2.0 device authorization grant flow. 

This flow makes it possible to securely authenticate your GitLab identity 
from input constrained devices where browser interactions are not an option. 

This makes the device authorization grant flow ideal for users attempting 
to use GitLab services from headless servers or other devices with no, 
or limited, UI. 

Thank you John Parent for your contribution!


Find project settings by using the command palette
--------------------------------------------------------

- https://about.gitlab.com/releases/2024/07/18/gitlab-17-2-released/#find-project-settings-by-using-the-command-palette
- https://docs.gitlab.com/ee/user/search/command_palette.html


GitLab offers many settings across projects, groups, the instance, and 
for yourself personally. 

To find the setting you’re looking for, you often have to spend time 
clicking through many different areas of the UI.

With this release, **you can now search for project settings from the 
command palette**. 

Try it out by visiting a project, selecting Search or go to…, entering 
command mode with >, and typing the name of a settings section, like **Protected tags**. 

Select a result to jump right to the setting itself


Indicate imported items in UI
---------------------------------

- https://about.gitlab.com/releases/2024/07/18/gitlab-17-2-released/#indicate-imported-items-in-ui

You can import projects to GitLab from other SCM solutions. 

However, it was difficult to know if project items were imported or 
created on the GitLab instance.

With this release, we’ve added visual indicators to items imported from 
GitHub, Gitea, Bitbucket Server, and Bitbucket Cloud where the creator 
is identified as a specific user. For example, merge requests, issues, and notes.

Add type attribute to issues events webhook
--------------------------------------------

- https://about.gitlab.com/releases/2024/07/18/gitlab-17-2-released/#add-type-attribute-to-issues-events-webhook

Issues, tasks, incidents, requirements, objectives, and key results all 
trigger payloads under the Issues Events webhook category. 

Until now, there has been no way to quickly determine the type of object 
that triggered the webhook within the event payload. 

This release introduces an object_attributes.type attribute available on 
payloads within the Issues events, Comments, Confidential issues events, 
and Emoji events triggers.


OAuth authorization screen improvements
------------------------------------------

- https://about.gitlab.com/releases/2024/07/18/gitlab-17-2-released/#oauth-authorization-screen-improvements
- https://docs.gitlab.com/ee/integration/oauth_provider.html

The OAuth authorization screen now more clearly describes the authorization 
you are granting. 

It also includes a “verified by GitLab” section for applications that are 
provided by GitLab. 

Previously, the user experience was the same, regardless of whether an 
application was provided by GitLab or not. 

This new functionality provides an extra layer of trust.


Separate wiki page title and path fields
-----------------------------------------

- https://about.gitlab.com/releases/2024/07/18/gitlab-17-2-released/#separate-wiki-page-title-and-path-fields
- https://docs.gitlab.com/ee/user/project/wiki/


In GitLab 17.2, wiki page titles are separate from their paths. 

In previous releases, if a page title changed, the path would also change, 
which could cause links to the page to break. 

Now, if a wiki page’s title changes, the path remains unchanged. 

Even if a wiki page path changes, an automatic redirect is set up to prevent 
broken links.

Improvements to the wiki sidebar
-------------------------------------

- https://about.gitlab.com/releases/2024/07/18/gitlab-17-2-released/#improvements-to-the-wiki-sidebar
- https://docs.gitlab.com/ee/user/project/wiki/

GitLab 17.2 adds several enhancements to how wikis display the sidebar. 

Now, a wiki displays all pages in the sidebar (up to 5000 pages), displays 
a table of contents (TOC), and provides a search bar to quickly find pages.

Previously, the sidebar lacked a TOC, making it challenging to navigate 
to sections of a page. 

The new TOC feature helps to see the page structure clearly, as well as 
navigate quickly to different sections, greatly improving usability.

The addition of a search bar makes discovering content easier. 

And because the sidebar now displays all pages, you can seamlessly browse 
an entire wiki.

