.. index::
   pair: Project management ; 2018
   pair: Gitlab ; GNOME
   pair: Gitlab ; GIMP


.. _project_management_2018:

=========================
Project management 2018
=========================



Intégration continue/gitlab ci (cii best practices)
====================================================

Les projets basés sur Gnome comme Gimp passent massivement sous gitlab:

- l'annonce: https://about.gitlab.com/press/releases/2017-11-01-gitlab-transitions-contributor-license.html
- https://gitlab.gnome.org/GNOME
- https://wiki.gnome.org/GitLab
- https://gitlab.gnome.org/GNOME/gimp/boards

Je découvre que gimp_ pratique les "cii best practices"_

Annonce du passage complet de GNOME sous gitlab le 29 mai 2018
----------------------------------------------------------------

- https://x.com/csoriano1618/status/1001501640623640577
- https://en.wikipedia.org/wiki/GNOME

.. figure:: 2018_05_29/gnome_to_gitlab.png
   :align: center

coreinfrastructure/best-practices-badge
-----------------------------------------

- https://github.com/coreinfrastructure/best-practices-badge



.. _gimp: https://gitlab.gnome.org/GNOME/gimp
.. _"cii best practices":  https://github.com/coreinfrastructure/best-practices-badge



NETWAYS/gitlab-training
==========================

.. seealso::

   - https://github.com/NETWAYS/gitlab-training



This training is designed as a two days hands-on training introducing Git,
GitLab, Workflows, CI/CD and many best practices.

The training participants will get an in-depth insight into the Git basics,
configuration and "good" commits. They also learn about GitLab basics
with repository and user management and continue to practice Git version
control with real-life exercises.

Moving along from standalone environments, participants collaborate with
others and get an overview on different Git workflows. Continuing with
practices, the training dives deep into continuous integration and
delivery (CI/CD) with GitLab, runners and DevOps production pipelines.

On top of that, the training provides more hints on GitLab usage, tools
for working with Git and anything proven useful for daily best practice.

Target audience are developers and Linux administrators.
