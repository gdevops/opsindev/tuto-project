.. index::
   pair: pipeline ; Automatic pipeline cleanup

.. _automatic_pipeline_cleanup:

=========================================================
2025-02-24 **gitlab 17.9 Automatic pipeline cleanup**
=========================================================

- https://docs.gitlab.com/ci/pipelines/settings/#automatic-pipeline-cleanup

::


    remote: ========================================================================
    remote: 
    remote:          Framagit vient de passer en version 17.9. Entre autres
    remote:      améliorations, il est maintenant possible de [paramétrer la
    remote:      suppression automatique des anciens pipelines d’intégration
    remote: continue](https://docs.gitlab.com/ci/pipelines/settings/#automatic-pipeline-cleanup).
    remote:      Aidez-nous à assurer la pérennité de Framagit en supprimant
    remote:     automatiquement vos anciens pipelines. Merci ! 🙂 Framagit has
    remote:    just been updated to version 17.9. Among other improvements, it is
    remote:      now possible to [set the automatic deletion of old continuous
    remote:                               integration
    remote: pipelines](https://docs.gitlab.com/ci/pipelines/settings/#automatic-pipeline-cleanup).
    remote:   Please help us future-proof Framagit by automatically deleting your
    remote:                      old pipelines. Thank you 🙂
    remote: 
    remote: ========================================================================
    remote: 


Users with the Owner role can set a CI/CD pipeline expiry time to help manage 
pipeline storage and improve system performance. 

The system automatically deletes pipelines that were created before the configured value.

- On the left sidebar, select Search or go to and find your project.
- Select Settings > CI/CD.
- Expand General pipelines.
- In the Automatic pipeline cleanup field, enter the number of seconds, 
  or a human-readable value like 2 weeks. Must be one day or more, and less 
  than one year. 
  Leave empty to never delete pipelines automatically. Empty by default.
- Select Save changes.
