.. index::
   pair: gitlab ; GitLab 17.7 GitLab 17.7 released with new Planner user role  (2024-12-19)

.. _gitlab_17_8_2025_01_16:

============================================================================
2025-01-16  GitLab 17.8 released with improved container repository security
============================================================================

- https://about.gitlab.com/releases/2025/01/16/gitlab-17-8-released/
- https://forum.gitlab.com/t/gitlab-17-8-released/119917

List the deployments related to a release
===============================================

- https://docs.gitlab.com/ee/user/project/releases/


Track multiple to-do items in an issue or merge request
==========================================================

- https://docs.gitlab.com/ee/user/todos.html#actions-that-create-to-do-items


Primary domain redirect for GitLab Pages
=============================================
