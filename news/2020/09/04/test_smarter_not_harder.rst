

.. _ref_test_luke_plant:

=================================
**Test smarter, not harder**
=================================

.. seealso::

   - https://lukeplant.me.uk/blog/posts/test-smarter-not-harder/
   - https://www.reddit.com/r/programming/comments/imzawj/test_smarter_not_harder/
   - :ref:`test_luke_plant`
