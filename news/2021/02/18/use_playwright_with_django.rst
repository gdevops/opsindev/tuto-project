

.. _playwright_with_django_2021_02_18:

=========================================
2021-02-18 Use playwright with Django
=========================================

.. seealso::

   - :ref:`use_playwright_with_django`


Another example
===================


.. literalinclude:: pw_recherche.py
   :linenos:
