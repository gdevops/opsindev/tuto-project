.. index::
   pair: Project management ; News

.. _project_management_news:

=========================
News
=========================


.. toctree::
   :maxdepth: 5

   2025/2025
   2024/2024
   2023/2023
   2022/2022
   2021/2021
   2020/2020
   2018/2018
