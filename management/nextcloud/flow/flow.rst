.. index::
   pair: Nextcloud; flow

.. _project_nextcloud_flow:

====================
nextcloud **flow**
====================

.. seealso::

   - https://cloudeezy.com/blog-nextcloud/nextcloud-flow-facilite-l-automatisation-des-actions-et-des-workflows.html


.. toctree::
   :maxdepth: 3

   description/description
