
.. _project_nextcloud_flow_def:

==================================
nextcloud **flow** description
==================================



Description cloudeezy
========================

.. seealso::

   - https://cloudeezy.com/blog-nextcloud/nextcloud-flow-facilite-l-automatisation-des-actions-et-des-workflows.html


**Nextcloud Flow** est conçu pour vous aider à automatiser les tâches.

Par exemple, lorsqu'un fichier est ajouté à un dossier spécifique dans
Nextcloud, un lien partagé vers le fichier peut être créé et envoyé
via Nextcloud Talk dans une salle de discussion spécifiée.

Flow peut notamment aider le flux de travail des équipes collaborant
sur des documents, tels que les processus d'approbation et d'examen.

Voici un exemple concret de l'utilisation de Nextcloud Flow :

1. Un partenaire envoie un bon de commande client par mail à une boîte de
   réception spéciale (par ex.: 'commandes@example.com').
   Les pièces jointes de cette boîte sont téléchargées automatiquement
   sur Nextcloud et, parce qu'elles sont déposées dans un dossier spécifique,
   elles reçoivent une balise (TAG) spécifique.

2. Cette balise (TAG) entraîne alors l'ajout du fichier en tant que carte
   de tâche dans une planche de l'application Nextcloud Deck pour que
   l'équipe 'X' puisse la traiter ultérieurement.

   Le chat de l'équipe 'X' les informe de l'arrivée de la nouvelle tâche.

3. Un membre de l'équipe 'X' examine le formulaire et, une fois terminé,
   déplace la carte vers le status 'Terminé', ce qui ajoute alors
   automatiquement une balise « Révisée ».
   Cette balise entraîne une notification pour le gestionnaire.

4. Une fois que la balise « Approuvé » a été ajoutée, les fichiers sont
   transformés en PDF, puis partagés via un lien public, qui à son tour
   est ajouté à un courrier qui est envoyé au partenaire.


