.. index::
   pair: Nextcloud; Project management

.. _nextcloud_management:

====================
Nextcloud management
====================


- https://en.wikipedia.org/wiki/Project_management

.. toctree::
   :maxdepth: 3

   deck/deck
   flow/flow
   tasks/tasks
