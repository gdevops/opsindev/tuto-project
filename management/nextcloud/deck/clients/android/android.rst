.. index::
   pair: Android ; deck

.. _nextcloud_deck_android:

==========================================================
Android deck client
==========================================================

.. seealso::

   - https://github.com/stefan-niedermann/nextcloud-deck
   - https://f-droid.org/de/packages/it.niedermann.nextcloud.deck/
