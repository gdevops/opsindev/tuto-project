.. index::
   pair: deck ; nextcloud
   pair: deck ; kanban
   ! deck

.. _project_nextcloud_deck:

=============================================================================================
**deck** (Kanban-style project & personal management tool for Nextcloud, similar to Trello)
=============================================================================================

.. seealso::

   - https://github.com/nextcloud/deck
   - https://apps.nextcloud.com/apps/deck
   - https://deck.readthedocs.io/en/latest/




Description
===========

**Deck** is a kanban style organization tool aimed at personal planning
and project organization for teams integrated with Nextcloud.

- 📥 Add your tasks to cards and put them in order
- 📄 Write down additional notes in markdown
- 🔖 Assign labels for even better organization
- 👥 Share with your team, friends or family
- 📎 Attach files and embed them in your markdown description
- 💬 Discuss with your team using comments
- ⚡ Keep track of changes in the activity stream
- 🚀 Get your project organized


Documentation
===============

.. seealso::

   - https://deck.readthedocs.io/en/latest/
   - https://github.com/nextcloud/deck/blob/master/docs/User_documentation_en.md


Markdown editor
==================

.. toctree::
   :maxdepth: 3

   markdown/markdown

wiki
=====


Deck clients
=============

.. toctree::
   :maxdepth: 3

   clients/clients


issues
==========

Versions
==========

.. toctree::
   :maxdepth: 3

   versions/versions
