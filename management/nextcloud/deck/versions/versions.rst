
.. _nextcloud_deck_versions:

==========================================================
Versions
==========================================================

.. seealso::

   - https://github.com/nextcloud/deck/releases


.. toctree::
   :maxdepth: 3

   1.2.2/1.2.2
