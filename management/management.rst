.. index::
   ! Project management

.. _project_management:

====================
Management
====================

.. seealso::

   - https://en.wikipedia.org/wiki/Project_management

.. toctree::
   :maxdepth: 3

   gitlab/gitlab
   nextcloud/nextcloud
