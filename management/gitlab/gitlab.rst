.. index::
   pair: Project management ; Gitlab
   ! Gitlab

.. _gitlab:
.. _gitlab_software:

====================
Gitlab
====================


- https://gitlab.com/gitlab-org/gitlab-ce
- https://en.wikipedia.org/wiki/GitLab
- https://gitlab.com/gitlab-org/gitlab
- https://about.gitlab.com/
- https://forum.gitlab.com/

- https://x.com/gitlab
- https://x.com/sytses (Sid Sijbrandij, CEO de Gitlab)
- https://x.com/dzaporozhets (Gitlab cofounder, Dmitriy Zaporozhets)


.. figure:: GitLab_logo.png
   :align: center


.. toctree::
   :maxdepth: 3

   introduction/introduction
   ci/ci

.. toctree::
   :maxdepth: 6

   glab/glab

.. toctree::
   :maxdepth: 3

   issues/issues
   runner/runner
   versions/versions
