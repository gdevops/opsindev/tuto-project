.. index::
   pair: glab; repo

.. _glab_repo:

====================
glab repo
====================

- https://gitlab.com/gitlab-org/cli

glab repo --help
====================

Work with GitLab repositories and projects

USAGE
  glab repo <command> [flags]

ALIASES
  project

CORE COMMANDS
  archive:     Get an archive of the repository.
  clone:       Clone a GitLab repository/project
  contributors: Get repository contributors list.
  create:      Create a new GitLab project/repository.
  delete:      Delete an existing repository on GitLab.
  fork:        Create a fork of a GitLab repository
  list:        Get list of repositories.
  mirror:      Mirror a project/repository to the specified location using pull or push method.
  search:      Search for GitLab repositories and projects by name
  transfer:    Transfer a repository to a new namespace.
  view:        View a project/repository

INHERITED FLAGS
  --help   Show help for command

LEARN MORE
  Use 'glab <command> <subcommand> --help' for more information about a command.
