.. index::
   pair: glab; config

.. _glab_config:

====================================
glab config command
====================================

- https://gitlab.com/gitlab-org/cli

Description
==============

Get and set key/value strings.

Current respected settings:

- token: Your GitLab access token, defaults to environment variables
- gitlab_uri: if unset, defaults to `https://gitlab.com`
- browser: if unset, defaults to environment variables
- editor: if unset, defaults to environment variables.
- visual: alternative for editor. if unset, defaults to environment variables.
- glamour_style: Your desired Markdown renderer style. Options are dark, light,
  notty. Custom styles are allowed using [glamour](https://github.com/charmbracelet/glamour#styles)
- glab_pager: Your desired pager command to use (e.g. less -R)


USAGE
  glab config [flags]

ALIASES
  conf

CORE COMMANDS
  get:        Prints the value of a given configuration key.
  init:       Shows a prompt to set basic glab configuration
  set:        Updates configuration with the value of a given key

FLAGS
  -g, --global   use global config file

INHERITED FLAGS
  --help   Show help for command

LEARN MORE
  Use 'glab <command> <subcommand> --help' for more information about a command.




::

    glab config get token

