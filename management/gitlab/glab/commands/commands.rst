.. index::
   pair: glab; commands

.. _glab_commands:

====================
glab commands
====================

- https://gitlab.com/gitlab-org/cli


.. toctree::
   :maxdepth: 4

   api/api
   ci/ci
   config/config
   issue/issue
   mr/mr
   pipeline/pipeline
   repo/repo
   release/release
   variable/variable
