.. index::
   pair: glab; variable

.. _glab_variable:

====================
glab variable
====================

- https://gitlab.com/gitlab-org/cli

glab variable --help
=========================

Manage GitLab Project and Group Variables

USAGE
  glab variable [flags]

ALIASES
  var

CORE COMMANDS
  delete:     Delete a project or group variable
  get:        get a project or group variable
  list:       List project or group variables
  set:        Create a new project or group variable
  update:     Update an existing project or group variable

FLAGS
  -R, --repo OWNER/REPO   Select another repository using the OWNER/REPO or `GROUP/NAMESPACE/REPO` format or full URL or git URL

INHERITED FLAGS
  --help   Show help for command

LEARN MORE
  Use 'glab <command> <subcommand> --help' for more information about a command.


glab variable get SERVER
==========================


