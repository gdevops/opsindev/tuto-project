.. index::
   pair: glab; ci

.. _glab_ci_command:

====================
glab ci command
====================

- https://gitlab.com/gitlab-org/cli

glab ci --help
====================


Work with GitLab CI pipelines and jobs

USAGE
  glab ci <command> [flags]

ALIASES
  pipe, pipeline

CORE COMMANDS
  artifact:   Download all Artifacts from the last pipeline
  delete:     Delete a CI pipeline
  get:        Get JSON of a running CI pipeline on current or other branch specified
  lint:       Checks if your `.gitlab-ci.yml` file is valid.
  list:       Get the list of CI pipelines
  retry:      Retry a CI job
  run:        Create or run a new CI pipeline
  status:     View a running CI pipeline on current or other branch specified
  trace:      Trace a CI job log in real time
  view:       View, run, trace/logs, and cancel CI jobs current pipeline

FLAGS
  -R, --repo OWNER/REPO   Select another repository using the OWNER/REPO or `GROUP/NAMESPACE/REPO` format or full URL or git URL

INHERITED FLAGS
  --help   Show help for command

LEARN MORE
  Use 'glab <command> <subcommand> --help' for more information about a command.



glab ci lint
===============

::

    glab ci lint

::

    Getting contents in .gitlab-ci.yml
    Validating...
    ✓ CI yml is Valid!


glab ci list
=================

::

    glab ci list


glab ci view
=================

::

    glab ci view
