.. index::
   pair: glab; mr

.. _glab_mr_command:

====================================
glab mr (Merge Request) command
====================================

- https://gitlab.com/gitlab-org/cli


::

    glab mr view
    glab mr approve
    glab mr merge
