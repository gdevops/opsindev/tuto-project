.. index::
   pair: glab; issue

.. _glab_issue:

====================
glab issue
====================

- https://gitlab.com/gitlab-org/cli

glab issue --help
====================

Work with GitLab issues

USAGE
  glab issue [command] [flags]

CORE COMMANDS
  board:      Work with GitLab Issue Boards in the given project.
  close:      Close an issue
  create:     Create an issue
  delete:     Delete an issue
  list:       List project issues
  note:       Add a comment or note to an issue on GitLab
  reopen:     Reopen a closed issue
  subscribe:  Subscribe to an issue
  unsubscribe: Unsubscribe to an issue
  update:     Update issue
  view:       Display the title, body, and other information about an issue.

FLAGS
  -R, --repo OWNER/REPO   Select another repository using the OWNER/REPO or `GROUP/NAMESPACE/REPO` format or full URL or git URL

INHERITED FLAGS
  --help   Show help for command

ARGUMENTS
  An issue can be supplied as argument in any of the following formats:
  - by number, e.g. "123"
  - by URL, e.g. "https://gitlab.com/NAMESPACE/REPO/-/issues/123"

EXAMPLES
  glab issue list
  glab issue create --label --confidential
  glab issue view --web
  glab issue note -m "closing because !123 was merged" <issue number>

LEARN MORE
  Use 'glab <command> <subcommand> --help' for more information about a command.


glab issue update --help
================================

Update issue

USAGE
  glab issue update <id> [flags]

FLAGS::

  -a, --assignee strings     assign users via username, prefix with '!' or '-'
   to remove from existing assignees, '+' to add, otherwise replace existing assignees with given users
  -c, --confidential         Make issue confidential
  -d, --description string   Issue description
  -l, --label strings        add labels
      --lock-discussion      Lock discussion on issue
  -m, --milestone string     title of the milestone to assign, pass "" or 0 to unassign
  -p, --public               Make issue public
  -t, --title string         Title of issue
      --unassign             unassign all users
  -u, --unlabel strings      remove labels
      --unlock-discussion    Unlock discussion on issue

INHERITED FLAGS::

      --help              Show help for command
  -R, --repo OWNER/REPO   Select another repository using the OWNER/REPO or
  `GROUP/NAMESPACE/REPO` format or full URL or git URL

EXAMPLES
  glab issue update 42 --label ui,ux
  glab issue update 42 --unlabel working

LEARN MORE
  Use 'glab <command> <subcommand> --help' for more information about a command.


