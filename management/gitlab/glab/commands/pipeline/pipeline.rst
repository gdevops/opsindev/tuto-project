.. index::
   pair: glab; pipeline

.. _glab_pipeline:

====================
glab pipeline
====================

- https://gitlab.com/gitlab-org/cli

glab pipeline --help
=========================


Work with GitLab CI pipelines and jobs

USAGE
  glab ci <command> [flags]

ALIASES
  pipe, pipeline

CORE COMMANDS
  artifact:   Download all Artifacts from the last pipeline
  delete:     Delete a CI pipeline
  get:        Get JSON of a running CI pipeline on current or other branch specified
  lint:       Checks if your `.gitlab-ci.yml` file is valid.
  list:       Get the list of CI pipelines
  retry:      Retry a CI job
  run:        Create or run a new CI pipeline
  status:     View a running CI pipeline on current or other branch specified
  trace:      Trace a CI job log in real time
  view:       View, run, trace/logs, and cancel CI jobs current pipeline

FLAGS
  -R, --repo OWNER/REPO   Select another repository using the OWNER/REPO or `GROUP/NAMESPACE/REPO` format or full URL or git URL

INHERITED FLAGS
  --help   Show help for command

LEARN MORE
  Use 'glab <command> <subcommand> --help' for more information about a command.


glab ci list
==============

::

    glab ci list


::

    showing 4 pipelines on pvergain/test_locust_ci (Page 1)

    (failed) • #27430  main  (about 15 minutes ago)
    (failed) • #27429  main  (about 53 minutes ago)
    (failed) • #27428  main  (about 56 minutes ago)
    (failed) • #27426  main  (about 1 hour ago)


glab pipeline delete $id
============================

::

    glab pipeline delete 27426

::

    Deleting Pipeline #27426
    ✓ Pipeline #27426 Deleted Successfully


glab pipeline run -b main
============================

::

    glab pipeline run -b main

::

    Created pipeline (id: 27441 ), status: created , ref: main , weburl:  https://XXXXXXXXXXXXXX/test_locust_ci/-/pipelines/27441 )



Examples
============

- https://blog.stephane-robert.info/post/gitlab-cli/#effacer-des-pipelines

.. code-block:: bash
   :linenos:

    #!/bin/bash
    # https://blog.stephane-robert.info/post/gitlab-cli/#effacer-des-pipelines

    # set the Personal Access Token GITLAB_TOKEN before
    set -e
    for id in `glab pipeline list --sort asc -P 10| awk '{print substr($3,2)}'| tail -n+2`
    do
        glab pipeline delete $id
    done


instead of


.. code-block:: bash
   :linenos:

    #!/bin/bash
    set -e

    TOKEN=$GITLAB_TOKEN
    # Visible under name of project page.
    PROJECT_ID="85807"
    # Set your gitlab instance url.
    GITLAB_INSTANCE="https://framagit.org/api/v4/projects"
    # How many to delete from the oldest, 100 is the maximum, above will just remove 100.
    PER_PAGE=100

    for PIPELINE in $(curl --header "PRIVATE-TOKEN: $TOKEN" "$GITLAB_INSTANCE/$PROJECT_ID/pipelines?per_page=$PER_PAGE&sort=asc" | jq '.[].id') ; do
        echo "Deleting pipeline $PIPELINE"
        curl --header "PRIVATE-TOKEN: $TOKEN" --request "DELETE" "$GITLAB_INSTANCE/$PROJECT_ID/pipelines/$PIPELINE"
    done
