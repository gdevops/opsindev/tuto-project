.. index::
   pair: gitlab ; CLI
   ! glab

.. _glab:

====================================================================
**glab** (A GitLab CLI tool bringing GitLab to your command line)
====================================================================

- https://gitlab.com/gitlab-org/cli
- https://docs.gitlab.com/ee/integration/glab/
- https://gitlab.com/gitlab-org/cli/-/tree/main/docs/source
- https://blog.stephane-robert.info/post/gitlab-cli/
- https://gitlab.com/gitlab-org/cli/-/tree/main


.. toctree::
   :maxdepth: 5

   commands/commands
