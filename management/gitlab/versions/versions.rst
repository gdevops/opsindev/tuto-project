.. index::
   pair: Versions ; Gitlab

.. _gitlab_versions:

====================
Gitlab versions
====================

- https://about.gitlab.com/releases/
- https://about.gitlab.com/direction
- https://about.gitlab.com/direction/#future-releases
- https://about.gitlab.com/blog/categories/releases/
- https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/CHANGELOG.md

.. toctree::
   :maxdepth: 3

   17/17
   15/15
   14/14
   13/13
   12/12
   11/11
   10/10
