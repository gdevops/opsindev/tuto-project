
.. _gitlab_14_4:

==========================================================================================================
Gitlab 14.4 (2021-10-22) released with Scheduled DAST scans and Integrated error tracking
==========================================================================================================

- https://about.gitlab.com/releases/2021/06/22/gitlab-14-0-released/
- https://x.com/gitlab/status/1451593826494291974?s=20


Announce
=========

- https://x.com/gitlab/status/1451593826494291974?s=20

GitLab 14.4 is released with Scheduled DAST scans and Integrated error
tracking plus over 30 other improvements.

Ethan
======

- https://x.com/ethan_reesor
- https://gitlab.com/firelizzard


Ehtan leapt in and offered to contribute remote repository support in
VS Code for GitLab hosted projects.

This effort was no small feat and consisted of not one, not two, three
or four merge requests, but five total contributions to add this feature


Remote Repositories for GitLab in Visual Studio Code
======================================================

When working in your editor you may need to refer to another project or
upstream library for additional information.
When you don’t have that project already cloned locally, you’re forced
to either leave your editor and browse the project on GitLab, or locate
and then clone the project so you can browse it in your editor.

Both of those tasks break your current context, introduce delays, and
can take you to a less familiar interface for working with code.

GitLab Workflow version 3.33.0 provides an option to open a remote
repository.
Open the command palette and use the GitLab: Open Remote Repository command
to find and then open a project.

Opening a remote repository allows you to browse a read-only version of
a project in your familiar VS Code environment.
You can then quickly find the information you’re looking for, compare an
implementation, or copy a snippet you need.

Thanks to Ethan Reesor for the amazing work and numerous merge requests
required in contributing this feature!

Integrated error tracking inside GitLab without a Sentry instance
====================================================================

- https://docs.gitlab.com/ee/operations/error_tracking.html#integrated-error-tracking
- https://gitlab.com/gitlab-org/gitlab/-/issues/329596

Prior to GitLab 14.4, you could integrate with Sentry Error Tracking by
supplying an endpoint for a Sentry backend (either self-deployed or in
their cloud service).

With Gitlab 14.4, you now have access to a Sentry-compatible backend
built into your GitLab instance.

This allows you to quickly instrument your apps so your errors show up
directly in GitLab without the need for a separate Sentry instance.


Sorting for project-level Value Stream Analytics
==================================================

Value Stream Management for projects just got better.
You can now sort the work items in a stage by time or name.

We’ve also added pagination to make it easier to locate and display the
issues you’re interested in.

Custom commit message for batch suggestions
===============================================

You can now write a custom commit message when applying batch suggestions.
This allows authors and contributors to accept suggestions and follow
commit message best practices for their projects.

It also helps to reduce the number of commits by applying all the suggestions
as a single commit with the desired message. If no custom commit message
is specified, the suggestion is committed with a default commit message.


Edit the current file with a single keystroke
===============================================

- https://docs.gitlab.com/ee/user/project/web_ide/
- https://gitlab.com/gitlab-org/gitlab/-/issues/340095

Now it’s even easier to take advantage of the powerful Web IDE and contribute
to a project from your web browser.

Simply press the . (dot) key on your keyboard to open the Web IDE with
the current context loaded and ready to edit.

Whether you’re navigating individual files in the repository or reviewing
a merge request, this is a great way to start editing without taking
your hand off the keyboard

Improve security of CI_JOB_TOKEN with bots and more
=====================================================

- https://docs.gitlab.com/ee/ci/jobs/ci_job_token.html#limit-gitlab-cicd-job-token-access
- https://gitlab.com/gitlab-org/gitlab/-/issues/328553

The CI_JOB_TOKEN CI/CD variable makes API calls more intuitive within
CI/CD jobs, enabling advanced automation.

For example, the token can be used with bot automation projects that run
pipelines in other projects.
The token is short-lived, but in an effort to make its usage even more
secure we are adding a setting that lets you list the exact projects
that can be accessed with your project’s CI job token.

If the token is used to try to access other projects, it will be denied
access to the API.

In the bot automation example, it gives you additional control over the
exact projects your bot will have access to and an added layer of security
when using the CI_JOB_TOKEN CI/CD variable.

**This setting is currently disabled by default to avoid impacting existing
projects but we strongly recommend enabling it in all your existing projects**.

Limit the runners registered to a group or project
=====================================================

- https://docs.gitlab.com/ee/administration/instance_limits.html#number-of-registered-runners-per-scope

In GitLab 13.12, we started rolling out this feature, limiting the number
of runners registered to a group or project.

As of 14.4, this feature is now fully enabled on GitLab.com and limits
the number of registered runners to 1,000.

For self-managed administrators, this means you can implement limits at
the group and project level, which will help reduce the administrative
overhead resulting from teams registering new runners while not removing
inactive runners.


Cleanup policies for the Dependency Proxy
==========================================

You can use the GitLab Dependency Proxy to proxy and cache container
images from Docker Hub for faster, more reliable builds.

The problem is that, over time, your team may add a lot of items to
the cache. This can result in higher storage costs.

You’ve been able to work around this by using the API to purge the
entire cache.
But that’s inefficient because you only want to remove old, stale items
that are no longer in use. That’s why we’ve added cleanup policies
for the Dependency Proxy.
Now you can programmatically delete image tags from the cache that
haven’t been recently used by your team.

Simply configure the number of days and all cached dependency proxy files
that have not been pulled in that many days are deleted.

We recommend 90 days as a good starting point.

The cleanup policies for the Dependency Proxy can be set using the
GitLab GraphQL API. We’re currently working on adding this setting to
the user interface as well.


Static Analysis analyzer updates
===================================

- https://docs.gitlab.com/ee/user/application_security/sast/analyzers
- https://gitlab.com/gitlab-org/gitlab/-/issues/343053

GitLab Static Analysis is comprised of a set of many security analyzers
that the GitLab Static Analysis team actively manages, maintains, and
updates.

Below are the analyzer updates released during 14.4. These updates bring
additional coverage, bug fixes, and improvements.

- Semgrep updated to version 0.69.1 (MR, Changelog).
    Reverted change to exclude minified files from the scan.
- Flawfinder updated to version 2.14.5 (MR, Changelog).
    Adds 2 new detection rules.
- MobSF updated to version 2.13.2 (MR, Changelog).
    Updates Linux OS packages to the latest versions.
- pmd-apex updated to version 6.3.9 (MR, Changelog).

If you include the GitLab-managed vendored SAST template (SAST.gitlab-ci.yml),
you do not need to do anything to receive these updates.

However, if you override or customize your own CI template, you must
update your CI configurations.

If you want to remain on a specific version of any analyzer, you can now
pin to a minor version of an analyzer.

Pinning to a previous version prevents you from receiving automatic
analyzer updates and requires you to manually bump your analyzer version
in your CI template.

More paid features available to free users
============================================

As a follow-up iteration to the Registration Features program released
in 14.1, we’ve added two more paid features for free GitLab self-managed
users running GitLab EE.

Simply register with GitLab and send us activity data through Service Ping,
and you can enjoy these additional features for free:

- Limit project size at a global, group, and project level.
- Restrict group access by IP address.

Performance improvements
===========================

In every release, we continue to make great strides improving the
performance of GitLab.

We’re committed to making every GitLab instance faster.

This includes GitLab.com, an instance with over 1 million users!

In GitLab 14.4, we’re shipping performance improvements for issues,
projects, milestones, and much more! Some improvements in GitLab 14.4 are:

- `Remove jQuery dependency from integrations settings form <https://gitlab.com/gitlab-org/gitlab/-/issues/337605>`_
- Caching in cleanup policy background jobs.
- Turn off and remove method instrumentation.
- Speed up retrieval of lists in issue boards.










