
.. _gitlab_14_0:

===========================
Gitlab 14.0 (2021-06-22)
===========================

- https://about.gitlab.com/releases/2021/06/22/gitlab-14-0-released/


Streamlined top navigation menu
===================================

- https://about.gitlab.com/releases/2021/06/22/gitlab-14-0-released/#streamlined-top-navigation-menu
- https://gitlab.com/gitlab-org/gitlab/-/issues/332635


.. figure:: consolidated_top_nav.png
   :align: center

   https://about.gitlab.com/releases/2021/06/22/gitlab-14-0-released/#streamlined-top-navigation-menu


GitLab 14.0 introduces an all-new, streamlined top navigation menu to
help you get where you’re going faster and with fewer clicks.

This new, consolidated menu offers the combined functionality of the
previous Projects, Groups, and More menus.

It gives you access to your projects, groups, and instance-level features
with a single click. Additionally, all-new responsive views improve the
navigation experience on smaller screens.


Sidebar navigation redesign
===============================

- https://about.gitlab.com/releases/2021/06/22/gitlab-14-0-released/#sidebar-navigation-redesign
- https://gitlab.com/gitlab-org/gitlab/-/issues/332635


.. figure:: redesigned_left_sidebar.png
   :align: center

   https://about.gitlab.com/releases/2021/06/22/gitlab-14-0-released/#sidebar-navigation-redesign

GitLab is big. And it’s getting bigger. As we’ve introduced new features
and categories, navigating the densely-packed left sidebar has become
less intuitive.

In GitLab 14.0 we’ve redesigned and restructured the left sidebar for
improved usability, consistency, and discoverability.

We’ve moved some links to features around, split up features in the
Operations menu into three distinct menus, improved visual contrast,
and optimized spacing so all the menu items can fit comfortably on a
smaller screen.

These changes are intended to better match your mental model of the
DevOps lifecycle, and provide a more predictable and consistent experience
while navigating within your projects and groups.


Edit wiki pages with the WYSIWYG Markdown editor
===================================================

- https://about.gitlab.com/releases/2021/06/22/gitlab-14-0-released/#edit-wiki-pages-with-the-wysiwyg-markdown-editor
- https://docs.gitlab.com/ee/user/project/wiki/#content-editor


Editing wiki content could be so much easier! Many GitLab wikis use
Markdown formatting, and for some users, Markdown is a barrier to efficient
collaboration.

In this release, you now have access to a rich, modern Markdown editing
experience in your wiki, so you can edit with confidence.

Instant feedback and visual editing tools help make wiki editing more
intuitive, and remove barriers to collaboration.

GitLab saves the changes as Markdown when you’re done, so users who want
to edit the Markdown directly can do so.

You can even type Markdown into the new editor and it will automatically
format the text as you type.

GitLab 14.0 introduces the Content Editor into the Wiki with support
for most of the basic Markdown content types like headers, bold and italic text,
lists, code blocks, and links.

**Full support for the entire GitLab Flavored Markdown specification will
arrive in upcoming releases.**

We also plan to make the Content Editor available in other areas of GitLab
in the future. We welcome input on this early MVC in this feedback issue.


SSH key expiration enforced by default
=======================================

- https://about.gitlab.com/releases/2021/06/22/gitlab-14-0-released/#ssh-key-expiration-enforced-by-default
- https://docs.gitlab.com/ee/user/admin_area/settings/account_and_limit_settings.html#enforce-ssh-key-expiration

Expired SSH keys added to GitLab are now disabled by default. This helps
to make your GitLab instance more secure. Previously, expired SSH keys
added to GitLab were enabled by default, and could be used unless
explicitly disabled by an administrator.

This change affects expired SSH keys used on GitLab.com. If your keys are
expired or will expire soon, you need to update the key and any services
using them. Our documentation on SSH keys has helpful steps on how to
create a new SSH key.

Self-managed administrators can still allow the use of expired keys,
similar to how they can allow use of expired personal access tokens.


Set pronouns on GitLab user profiles
=======================================

- https://about.gitlab.com/releases/2021/06/22/gitlab-14-0-released/#set-pronouns-on-gitlab-user-profiles
- https://docs.gitlab.com/ee/user/profile/#add-your-gender-pronouns

Pronouns have been added to GitLab user profiles. The pronouns appear
next to user names in the Profile tab. You can:

- Decide whether or not to add pronouns to your profile.
- Self-identify and enter whatever pronouns you prefer, without selecting
  from a predefined list.

Besides being more inclusive, GitLab wants help people use the correct
pronouns when replying to comments to respect people’s identity.


Identify which jobs triggered downstream pipelines
=====================================================

- https://about.gitlab.com/releases/2021/06/22/gitlab-14-0-released/#identify-which-jobs-triggered-downstream-pipelines
- https://docs.gitlab.com/ee/ci/parent_child_pipelines.html


Previously, when looking at the pipeline view, it was difficult to
determine which job triggered a downstream pipeline.

Starting in 14.0, every downstream pipeline shows the name of the job that
triggered it. This makes it easier to track the execution flow in complex
pipelines that trigger downstream pipelines.


Predefined CI/CD variable for environment action
===================================================

- https://about.gitlab.com/releases/2021/06/22/gitlab-14-0-released/#predefined-cicd-variable-for-environment-action
- https://docs.gitlab.com/ee/ci/variables/predefined_variables.html

If you want to reuse scripts and configuration between deployment jobs
using the environment: keyword, it can be difficult to exclude certain
behaviors based on the type of action the deployment job performs.

For example, an environment: action of stop might be a job that is
stopping a review_app, and you don’t want your deployment scripts to run.

Now, the value of environment: action: is available as the CI_ENVIRONMENT_ACTION
predefined CI/CD variable, making it easier than ever to configure one
script that can work for all deployment jobs.


Install PyPI packages from your group or subgroup
===================================================

- https://about.gitlab.com/releases/2021/06/22/gitlab-14-0-released/#install-pypi-packages-from-your-group-or-subgroup
- https://docs.gitlab.com/ee/user/packages/pypi_repository/#install-from-the-group-level


You can use your project’s Package Registry to publish and install PyPI
packages.
When you install a PyPI package, you must specify which project the package
resides in. This works well if you have a small number of projects.
If you have multiple projects nested within a group, you might quickly
find yourself adding dozens or even hundreds of different sources.

For large organizations with many teams, it’s common for a team to publish
packages to their project’s Package Registry alongside the source code
and pipelines.
However, they must also be able to easily install dependencies from other
projects within their organization.

You can now install packages from your group, so you don’t have to remember
which package lives in which project. To do this, use the simple API to
specify a package: GET groups/:id/packages/pypi/files/:sha256/:file_identifier.

You can also write the output to a file, or return the package descriptor
as an HTML file. Read the docs for more info and let us know how it goes.

We hope that this helps to make your team and organization more efficient.



GitLab upgraded to Ruby on Rails 6.1
======================================

- https://about.gitlab.com/releases/2021/06/22/gitlab-14-0-released/#gitlab-upgraded-to-ruby-on-rails-61
- https://docs.gitlab.com/ee/development/gemfile.html#gemfile-guidelines
- https://guides.rubyonrails.org/6_1_release_notes.html


Default branch name for new repositories now main
=====================================================

- https://about.gitlab.com/releases/2021/06/22/gitlab-14-0-released/#default-branch-name-for-new-repositories-now-main
- https://about.gitlab.com/releases/2021/06/22/gitlab-14-0-released/#default-browser-performance-testing-job-renamed-in-gitlab-140
- https://gitlab.com/groups/gitlab-org/-/epics/3600
- https://about.gitlab.com/blog/2021/03/10/new-git-default-branch-name/

Every Git repository has an initial branch, which is named master by default.

It’s the first branch to be created automatically when you create a new
repository.

Future Git versions will change the default branch name in Git from master
to main.

In coordination with the Git project and the broader community,
GitLab has changed the default branch name for new projects on both our
SaaS (GitLab.com) and self-managed offerings starting with GitLab 14.0.

This will not affect existing projects.

GitLab has already introduced changes that allow you to change the default
branch name both at the instance level (for self-managed users) and at
the group level (for both SaaS and self-managed users).

We encourage you to make use of these features to set default branch
names on new projects.

For more information, see the `related epic <https://gitlab.com/groups/gitlab-org/-/epics/3600>`_
and `related blog post <https://about.gitlab.com/blog/2021/03/10/new-git-default-branch-name/>`_.


Helm v2 support
=====================

Helm v2 was officially deprecated in November of 2020, with the stable
repository being de-listed from the Helm Hub shortly thereafter.

With the release of GitLab 14.0, which will include the 5.0 release of
the GitLab Helm chart, Helm v2 will no longer be supported.

Users of the chart should upgrade to Helm v3 to deploy GitLab 14.0 and later.


PostgreSQL 11 support
==========================

PostgreSQL 12 will be the minimum required version in GitLab 14.0.

It offers significant improvements to indexing, partitioning, and general
performance benefits.

Starting in GitLab 13.7, all new installations default to version 12.

From GitLab 13.8, single-node instances are automatically upgraded as
well. If you aren’t ready to upgrade, you can opt out of automatic upgrades.


You must upgrade to PostgreSQL 12 before upgrading to GitLab 14.0.

PostgreSQL 12 is the minimum required version starting in GitLab 14.0.

PostgreSQL 11 has been removed and is no longer officially supported.
You will need to plan on some downtime for the PostgreSQL upgrade because
the database must be down while the upgrade is performed.
If you are using the GitLab-provided PostgreSQL database, you should
make sure that your database is PostgreSQL 12 on GitLab 13.12 regardless
of your installation method.


Removal of legacy storage for GitLab Pages
==============================================

To make GitLab Pages cloud-native compatible, starting in GitLab 14.0,
we’re changing the underlying storage architecture used by GitLab Pages
to the recently introduced ZIP storage.

The migration to the new ZIP archives architecture is designed to be
automatic, however, if after the migration you see 404 Not Found for
some Pages, the automatic migration has probably failed.

To ease this transition to ZIP storage, we’ve provided a temporary
use_legacy_storage flag from GitLab 14.0 to 14.2, but we will remove
it in GitLab 14.3.

This flag will allow GitLab and GitLab Pages to use the non-ZIP deployments
to serve content.

Removal date: September 22, 2021







