.. index::
   pair: 11.11 (2019-05-22; Gitlab
   pair: GraphQL ; Gitlab
   pair: biometric ; authentication

.. _gitlab_11_11:

===============================================================================================
Gitlab 11.11 (2019-05-22) with  Multi-Assignment for MRs and container enhancements
===============================================================================================


Reduce overhead with Windows support of Docker containers and provisioning instance-level Kubernetes clusters
===============================================================================================================


We 💖 containers !

Containers require fewer system resources than your traditional or virtual
machine environments while increasing the portability of your application.

With GitLab 11.11, we now support a Windows Container Executor for
GitLab Runners, something that will enable the use of Docker containers
on Windows, allowing for more advanced pipeline orchestration and management.


This month's Most Valuable Person (MVP) is Kia Mei Somabes
============================================================

.. seealso::

   - https://gitlab.com/kiameisomabes

In this release, we added the ability to download folders from repositories
instead of the entire repository contents.

This makes it a lot easier to get what you need if you’re just looking to grab
a few files. Thanks Kia Mei Somabes for the great contribution !


Windows Container Executor for GitLab Runner
===============================================

In GitLab 11.11 we are pleased to add a new executor to the GitLab Runner for
using Docker containers on Windows.

Previously, using the shell executor to orchestrate Docker commands was the
primary approach for Windows, but with this update you are now able to use
Docker containers on Windows directly, in much the same way as if they were
on Linux hosts.

This opens up the door for more advanced kinds of pipeline orchestration
and management for our users of Microsoft platforms.

Included with this update is improved support for PowerShell throughout
GitLab CI/CD, as well as new helper images for various versions of
Windows containers.

Please note that your own Windows runners can be used with GitLab.com, but
are not currently available as part of the shared public fleet.


Download archives of directories within a repository
=======================================================

Depending on the type of project and its size, downloading an archive of the
entire project may be slow or unhelpful – particularly in the case of large
monorepos.

In GitLab 11.11, you can now download an archive of the contents of the current
directory, including subdirectories, so that you download only the files you need.

Thank you, Kia Mei Somabes, for the contribution!


Access deployment details through Environments API
====================================================

.. seealso::

   - https://docs.gitlab.com/ee/api/environments.html#get-a-specific-environment

We have added the ability to request information on a specific environment to
the Environments API, making it easier now to ask, “Which commit is deployed
to my environment right now ?”

This will make automation and reporting easier for users of GitLab’s
environments feature.



Install Prometheus on Group-level clusters
============================================

.. seealso::

   - https://docs.gitlab.com/ee/user/group/clusters/#installing-applications

In this release, GitLab provided the ability to attach a Kubernetes cluster
to an entire group. We’ve also added the ability to install a single Prometheus
instance to that cluster, making monitoring of all the projects within
that cluster easier.


Run all manual jobs for a stage in one click
=============================================

.. seealso::

   - https://docs.gitlab.com/ee/ci/pipelines.html#manual-actions-from-pipeline-graphs

With GitLab 11.11, users who rely on stages with many manual jobs can now
easily run all of the manual jobs in a given stage by using the Play all button
located to the right of the stage name in the pipeline views.

Create a file directly from an environment variable
====================================================

.. seealso::

   - https://docs.gitlab.com/ee/ci/variables/#variable-types
   - :ref:`gitlab_variable_types`

One common use of environment variables is to create a file, particularly for
secrets that should be protected and only available on a certain environment’s
pipeline.

You would do this by setting the variable content to the file content, then
create a file in your job that contains the value.

Using our new file type environment variable, you can do this in one step
without having to modify your .gitlab-ci.yml.


Repository read-write scope for personal access tokens
========================================================

.. seealso::

   - https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#limiting-scopes-of-a-personal-access-token
   - https://gitlab.com/hvlad

Many personal access tokens rely on api level scoping for programmatic changes,
but full API access may be too permissive for some users or organizations.

Thanks to a community contribution, personal access tokens can now be scoped
to only read and write to project repositories – preventing deeper API access
to sensitive areas of GitLab like settings and membership.

Thanks to Horatiu Eugen Vlad for the contribution!


Add basic support for group GraphQL queries
============================================

- https://docs.gitlab.com/ee/api/graphql/#available-queries
- http://gitlab.example.com/-/graphql-explorer
- https://en.wikipedia.org/wiki/GraphQL
- https://graphql.org/
- https://github.com/graphql/graphql-spec
- https://x.com/graphqlweekly


**GraphQL APIs** allows users to request exactly the data they need, making it
possible to get all required data in a limited number of requests.

In this release, GitLab is now supporting basic group information support
in the GraphQL API.

GraphiQL
-----------

The API can be explored by using the GraphiQL IDE, it is available on your
instance on gitlab.example.com/-/graphql-explorer.


Sign in with **UltraAuth** biometric authentication
=====================================================

.. seealso::

   - https://ultraauth.com/
   - https://docs.gitlab.com/ee/integration/ultra_auth.html
   - https://gitlab.com/tannakartikey

UltraAuth is a company specializing in passwordless, biometric authentication.

We’re excited to support their authentication strategy in GitLab !

Thanks to Kartikey Tanna for the contribution !


GitLab Runner 11.11
=====================

GitLab Runner is the open source project that is used to run your CI/CD jobs
and send the results back to GitLab.

- Fix git lfs not getting submodule objects
- Optimize trace handling for big traces
- Allow to configure Feature flags using config.toml
- Allow to use FF to configure /builds folder
- Add PowerShell support for Docker Executor
- Support windows docker volumes configuration


Git 2.21.0 (2019-02-24) or greater required
==============================================

.. seealso:: :ref:`git_2_21_0`

Beginning with GitLab 11.11, Git 2.21.0 is required to run GitLab.

Omnibus GitLab already ships with Git 2.21.0, but users of source installations
that run older versions of Git will have to upgrade.
