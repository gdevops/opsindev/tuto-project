.. index::
   pair: 11.1 ; Gitlab

.. _gitlab_11_1:

======================================================================================
Gitlab 11.1 released with Security Dashboards and enhanced code search  (2018-07-22)
======================================================================================

.. seealso::

   - https://about.gitlab.com/2018/07/22/gitlab-11-1-released/
   - https://about.gitlab.com/direction/#108
