.. index::
   pair: 11.8 ; Gitlab

.. _gitlab_11_8:

=====================================================================================================
Gitlab 11.8 (2019-02-22) released with SAST for JavaScript, Pages for subgroups, and Error Tracking
=====================================================================================================

.. seealso::

   - https://about.gitlab.com/2019/02/22/gitlab-11-8-released/



Pages support for subgroups
============================

.. seealso::

   - https://docs.gitlab.com/ee/administration/pages/
   - https://gitlab.com/gitlab-org/gitlab-ce/issues/30548

Pages have been updated to work with subgroups in GitLab, giving you the
ability to create Pages sites there as well.

Sites set up in this way will have a URL in the format of toplevel-group.gitlab.io/subgroup/project.

This will give your projects, even when part of subgroups, access to the
ability to create documentation or other sites needed as part of releasing
your software.


Specify the first day of the week
====================================

.. seealso::

   - https://docs.gitlab.com/ee/user/profile/preferences.html


Before, calendars in GitLab assumed that weeks began on Sunday. Users can now
elect in their profile preferences to have their first day of the week begin
on Monday, which is reflected throughout the application in date pickers and
contribution graphs.


Thank you Fabian Schneider for the contribution !


Record of a user's last activity in GitLab now includes browsing
====================================================================

.. seealso::

   - https://docs.gitlab.com/ee/user/instance_statistics/user_cohorts.html

GitLab includes a user attribute, last_activity_on, to help admins understand
when a user’s last activity was taken.

This is very helpful for finding active and inactive users.

To ensure we’re capturing read-only activity, we’ve expanded last_activity_on
to update on visits to pages related to dashboards, projects, issues, and
merge requests.


.html extensions are now automatically resolved for Pages sites
==================================================================

.. seealso::

   - https://docs.gitlab.com/ee/user/project/pages/

A file in your Pages site called /sub-page.html can now also be accessed as
/sub-page, giving you more options over how your site appears to your users.



Clean up unused tags from the Container Registry using the API
================================================================

.. seealso::

   - https://docs.gitlab.com/ee/api/container_registry.html#delete-a-repository-tag
   - https://gitlab.com/gitlab-org/gitlab-ce/issues/55978

Many organizations build containers on every commit, to facilitate validation
of code changes, as well as final deployment.

This can lead to a large number of container tags that are used for a short
period of time, and are no longer necessary.

GitLab 11.8 now allows end users to clean up their container registries using
our API, by deleting tags singly or in bulk using a regex.

::

    curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/registry/repositories/2/tags/v10.0.0"



Display cluster environment in serverless function list view
===============================================================

.. seealso::

   - https://docs.gitlab.com/ee/user/project/clusters/serverless/#deploying-functions
   - https://gitlab.com/gitlab-org/gitlab-ce/issues/54986


The Serverless page has been improved and will now group functions deployed to
Knative based on the cluster environment they are deployed to.

In addition, function description is now displayed along with shortcut button
to copy the function endpoint and another open the endpoint in a new tab.


Ensure Cert-Manager works with Auto DevOps URLs
=================================================

.. seealso::

   - https://docs.gitlab.com/ee/topics/autodevops/#auto-devops-base-domain
   - https://gitlab.com/gitlab-org/gitlab-ce/issues/55820


Cert-Manager provides an easy way to add HTTPS support for your Auto DevOps
applications. This release adds support for URLs longer than the default 64
characters supported by Let’s Encrypt, providing more flexibility for your
applications.



GitLab chart improvements
===========================

.. seealso::

   - https://docs.gitlab.com/charts/
   - https://docs.gitlab.com/charts/advanced/external-db/index.html#configuring-gitlab-to-use-an-external-database


- GCS is now a supported bucket type for backups
- Postgres databases with mutual TLS authentication can now be utilized
- ruby has been updated to 2.5.3
