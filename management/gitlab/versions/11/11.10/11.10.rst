.. index::
   pair: 11.10 ; Gitlab


.. _gitlab_11_10:

===============================================================================================
Gitlab 11.10 (2019-04-22) with Pipelines on the Operations Dashboard
===============================================================================================

.. seealso::

   - https://about.gitlab.com/2019/04/22/gitlab-11-10-released/




Description
============

GitLab continues to add features to provide visibility into the DevOps lifecycle.

This release enhances the Operations Dashboard with a powerful feature that
provides an overview of pipeline status.

This is handy even when looking at a single project's pipeline, but is especially
valuable when using multi-project pipelines - common when you have a microservices
architecture and you need to run a pipeline to test and deploy code housed in
multiple different project repositories.

Now you can get instant visibility at a glance into the health of all of your
pipelines on the Operations Dashboard, no matter where they run.


More thorough Container Registry cleanup
==========================================

.. seealso::

   - https://docs.gitlab.com/omnibus/maintenance/#removing-unused-layers-not-referenced-by-manifests

In normal use of the Container Registry with CI pipelines, typically you will
end up pushing many iterative revisions to the same tag.

Due to the way Docker Distribution is implemented, the default behavior is
to preserve all revisions in the system – this ends up consuming a lot of space
under this usage pattern.

By using the -m parameter with registry-garbage-collect, administrators now
have an easy way to wipe out these historical revisions and free up valuable
storage space.


Composable Auto DevOps
=========================

.. seealso::

   - https://docs.gitlab.com/ee/topics/autodevops/#using-components-of-auto-devops

Auto DevOps enables teams to adopt modern DevOps practices with little to no
effort.

Starting in GitLab 11.10 each job of Auto DevOps is being made available as an
independent template. Using the includes feature of GitLab CI, users can
include only certain stages of Auto DevOps while continuing to use their own
custom gitlab-ci.yml.

This will enable teams to include just the desired jobs while taking advantage
of any updates made upstream.


Push and merge when pipeline succeeds
========================================

.. seealso::

   - https://docs.gitlab.com/ee/user/project/merge_requests/#git-push-options

Trunk-based development claims that long-running branches should be avoided in
favor of small, short-lived, single-owner branches.

For the smallest changes it is not uncommon to push directly to the target
branch, but this runs the risk of breaking the build.

In this release, GitLab supports new Git push options to open a merge request
automatically, set the target branch, and enable merge when the pipeline
succeeds via the command line, at the moment that you push to your branch.



Improved integration with external monitoring dashboards
==========================================================

.. seealso::

   - https://docs.gitlab.com/ee/user/project/integrations/prometheus.html#managed-prometheus-on-kubernetes


GitLab can access multiple Prometheus servers (at the environment, project
and soon group levels) but the complexity of multiple endpoints can be difficult
or unsupported by common dashboarding tools.

In this release teams can now interact with a single Prometheus API interface,
making integration with services like Grafana much simpler.


Sort Wiki pages by created date
=================================

.. seealso::

   - https://docs.gitlab.com/ee/user/project/wiki/index.html

A project’s Wiki allows teams to share documentation and other important
information conveniently, side by side with source code and issues.

In this release, the list of pages in a Wiki can be sorted by created date and
title, allowing users to locate recently created content quickly.



See Load Balancer metrics in your Grafana dashboarding
=========================================================

.. seealso::

   - https://docs.gitlab.com/omnibus/settings/grafana.html#dashboards

Ensuring your GitLab instance stays healthy is critical.

We’ve previously given you default dashboards to review in our bundled-in
Grafana instance.

Starting with this release, we now include additional dashboards to monitor
your :ref:`NGINX load balancers <nginx_load_balancer>`.


Show DAST results in the Group Security Dashboard
====================================================

.. seealso::

   - https://docs.gitlab.com/ee/user/application_security/security_dashboard/

We have added Dynamic Application Security Testing (DAST) results in the
Group Security Dashboard to accompany results already present for SAST,
Container Scanning, and Dependency Scanning.


Simple masking of protected variables in logs
===============================================

.. seealso::

   - https://docs.gitlab.com/ee/ci/variables/#masked-variables

GitLab provides several ways to protect and limit the scope of variables in
GitLab CI/CD. However, there are still ways that variables can leak into
build logs, either intentionally or unintentionally.

GitLab takes risk management and audit seriously and continues to add features
to help with compliance efforts. In GitLab 11.10, we’ve added the ability to
mask certain types of variables if they are found in the job trace logs,
adding a level of protection against accidentally leaking the content of
those variables into the logs.
Also, GitLab will now automatically mask many of the built-in token variables.


Allow users to change the path for cloning in CI
==================================================

.. seealso::

   - https://docs.gitlab.com/ee/ci/yaml/#custom-build-directories

By default, the GitLab Runner clones the project to a unique subpath of the
$CI_BUILDS_DIR. However, for some projects, such as Golang projects, there
may be a requirement to have the code cloned to a specific directory to be built.

In GitLab 11.10, we’ve introduced a variable called GIT_CLONE_PATH that
allows users to specify the specific path that the GitLab Runner will
clone to before starting the job.


Enable/disable Auto DevOps at the Group level
===============================================

.. seealso::

   - https://docs.gitlab.com/ee/topics/autodevops/#enablingdisabling-auto-devops-at-the-group-level


Enabling Auto DevOps for your GitLab.com project provides an easy way to get
started with modern DevOps workflows, from build all the way to deploy.

Starting with GitLab 11.10 we’ve added the ability to enable/disable Auto
DevOps for all the projects that are part of any given group


Group Runners for group-level clusters
=========================================

.. seealso::

   - https://docs.gitlab.com/ee/user/group/clusters/#installing-applications

Group-level clusters now support the installation of GitLab Runner.
Group-level Kubernetes runners will appear for child projects as group
runners tagged with the labels cluster and kubernetes.



Show function invocation count for Knative functions
=======================================================

.. seealso::

   - https://docs.gitlab.com/ee/user/project/clusters/serverless/#function-details

Functions deployed with GitLab Serverless will now include the number of
invocations received for the particular function.

Showing the number of invocations requires Prometheus to be installed on
the cluster where Knative is installed


Add control for git clean flags for GitLab CI/CD jobs
======================================================

.. seealso::

   - https://docs.gitlab.com/ee/ci/yaml/#git-clean-flags

By default, GitLab Runner runs a git clean as part of the process of checking
out your code while running a job in GitLab CI/CD.

In GitLab 11.10, we’re adding the ability for users to control the flags
passed to the git clean command.
This will help teams who have dedicated runners or are building projects
from large mono repositories to manage the checkout process prior to
executing their scripts.

The new variable GIT_CLEAN_FLAGS defaults to -ffdx and accepts all the
possible options of the git clean command.




External Authorization in Core
================================

.. seealso::

   - https://docs.gitlab.com/ee/user/admin_area/settings/external_authorization.html

Secure environments may require checking with an additional external
authorization resource to permit project access.

We added support for this additional layer of access control in 10.6, and
we’ve heard requests from the community to move this functionality to Core.

We’re happy to do this for External Authorization and bring this additional
level of security to Core instances, since it’s a feature cared about by
individual contributors.


Allow Developers to create projects in groups in Core
=======================================================

.. seealso::

   - https://docs.gitlab.com/ee/user/group/#default-project-creation-level

We added a configurable option to allow the Developer role to create projects
in groups back in 10.5, and we’re adding this option to Core.

Creating projects is a key capability for productivity in GitLab, and moving
this option to Core helps reduce barriers when members of an instance want
to work on something new.



GitLab Runner 11.10
=====================

.. seealso::

   - https://docs.gitlab.com/runner
   - https://gitlab.com/gitlab-org/gitlab-runner/blob/v11.10.0/CHANGELOG.md

We’re also releasing GitLab Runner 11.10 today! GitLab Runner is the open
source project that is used to run your CI/CD jobs and send the results
back to GitLab.

Most interesting changes:

- Add option to specify clone path.
- Improve support for git clean.
- Allow users to disable debug tracing.
- Use delayed variable expansion for error check in Windows Cmd.
- Fix color output on Windows.

List of all changes can be found in GitLab Runner’s CHANGELOG_.

.. _CHANGELOG: https://gitlab.com/gitlab-org/gitlab-runner/blob/v11.10.0/CHANGELOG.md

Performance improvements
==========================

.. seealso::

   - https://gitlab.com/groups/gitlab-org/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name%5B%5D=performance&milestone_title=11.10

We continue to improve the performance of GitLab with every release for GitLab instances of every size. Some of the improvements in GitLab 11.10 are:

- Users autocomplete is faster.
- Optimize **SQL queries used for listing project issues when searching**.
- Elasticsearch search results no longer hit Gitaly.
- GraphQL queries have a complexity limit applied.
- Disable method instrumentation for diffs to improve performance of merge
  requests when **Prometheus is enabled**.
- Improve performance of GitHub Pull Request import.
- Cache commit lookups based on refname.
- Improves performance of merge requests diffs by memoizing diff file blobs.


Omnibus improvements
=====================

.. seealso::

   - https://docs.gitlab.com/omnibus/

The following improvements have been made to Omnibus in GitLab 11.10:

GitLab 11.10 includes Mattermost 5.9.0, an open source Slack-alternative
whose newest release includes a new Integrations Directory, a simple way
to migrate datafrom Hipchat, and much more.

This version also includes security updates and upgrading is recommended.

We have now integrated Grafana with Omnibus, making it very easy to begin
monitoring your instance of GitLab.

We have added support to clean up old container images from the Docker registry.
We have updated ca-certs to 2019-01-23.
