
.. _gitlab_12_versions:

====================
Gitlab 12 versions
====================

.. seealso::

   - https://about.gitlab.com/releases/
   - https://about.gitlab.com/direction
   - https://about.gitlab.com/direction/#future-releases
   - https://about.gitlab.com/blog/categories/releases/
   - https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CHANGELOG.md

.. toctree::
   :maxdepth: 3

   12.8/12.8
   12.7/12.7
   12.6/12.6
   12.0/12.0
