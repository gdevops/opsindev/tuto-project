
.. _gitlab_15_1:

==========================================================================================================
Gitlab 15.1 (2022-06-22) released with SAML Group Sync and SLSA level 2 build artifact attestation
==========================================================================================================

- https://about.gitlab.com/releases/2022/06/22/gitlab-15-1-released
- https://about.gitlab.com/releases/2022/06/22/gitlab-15-1-released/#mvp
- https://gitlab.com/feistel


Link to included CI/CD configuration from the pipeline editor
==================================================================================

- https://about.gitlab.com/releases/2022/06/22/gitlab-15-1-released/#link-to-included-cicd-configuration-from-the-pipeline-editor
- https://gitlab.com/gitlab-org/gitlab/-/issues/350722


A typical CI/CD configuration uses the include keyword to import configuration
stored in other files or CI/CD templates.

When editing or troubleshooting your configuration though, it can be
difficult to understand how all the configuration works together because
the included configuration is not visible in your .gitlab-ci-yml, you
only see the include entry.

**In this release, we added links to all included configuration files and
templates to the pipeline editor**.
Now you can easily access and view all the CI/CD configuration your
pipeline uses, making it much easier to manage large and complex pipelines.



Prevent users from using known insecure public keys
=========================================================

- https://about.gitlab.com/releases/2022/06/22/gitlab-15-1-released/#prevent-users-from-using-known-insecure-public-keys


When you attempt to add a new SSH key to your GitLab account, the key is
checked against a list of SSH keys that are known to be compromised.

Users can’t add keys from this list to any GitLab account. This helps to
secure your GitLab instance.

Thank you hackercat for your contribution!


Rendered images in Python notebook MRs
===========================================

- https://about.gitlab.com/releases/2022/06/22/gitlab-15-1-released/#rendered-images-in-python-notebook-mrs


Python notebooks are key to data scientists’ and machine learning
engineers’ workflows. These files commonly display charts and graphs
via static images to help visualize the notebook.

With this release, we now render these images in the merge request and
commit view enabling a better user experience when reviewing code
changes including Python notebooks with images.



Retry a downstream pipeline from the pipeline graph
======================================================

- https://about.gitlab.com/releases/2022/06/22/gitlab-15-1-released/#retry-a-downstream-pipeline-from-the-pipeline-graph


Previously, to retry a downstream pipeline, you had to navigate to the
pipeline and select retry. This worked, but was a challenging experience
when there were multiple downstream pipelines.

You had to go into every individual pipeline you wanted to retry and find
the retry option, which was cumbersome.

In this release, we’ve improved the user experience by adding an option
to retry downstream pipelines directly from the pipeline graph, without
the need to go into each pipeline’s details page

