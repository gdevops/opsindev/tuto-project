
.. index::
   pair: Gitlab; Tasks
   pair: Gitlab; 15.3 (2022-08-22)

.. _gitlab_15_3:

==========================================================================================================
Gitlab 15.3 (2022-08-22) released with **tasks for managing your work** and free GitOps features
==========================================================================================================

- https://about.gitlab.com/releases/2022/08/22/gitlab-15-3-released/
- https://docs.gitlab.com/ee/user/tasks.html

create-tasks-in-issues
==================================================================================

- https://about.gitlab.com/releases/2022/08/22/gitlab-15-3-released/#create-tasks-in-issues
- https://docs.gitlab.com/ee/user/tasks.html
- https://gitlab.com/gitlab-org/gitlab/-/issues/363613

**Tasks provide a robust way to refine an issue into smaller, discrete work units**.

Previously in GitLab, you could break down an issue into smaller parts
using **markdown checklists** within the description.

However, these checklist items could not be easily assigned, labeled, or
managed anywhere outside of the description field.

You can now create tasks within issues from the Child Items widget.

Then, you can open the task directly within the issue to quickly update
the title, set the weight, or add a description.

**Tasks break down work within projects for GitLab Free and increase the
planning hierarchy** for our GitLab Premium customers to three levels
(epic, issue, and task).

In our next iteration, you will be able to add labels, milestones, and
iterations to each task.

**Tasks** represent our first step toward evolving issues, epics, incidents,
requirements, and test cases to `work items <https://docs.gitlab.com/ee/development/work_items.html>`_.

If you have feedback or suggestions about tasks, please comment on this issue.


.. _gitlab_work_items:

Work items and work item types
--------------------------------


- https://gitlab.srv.int.id3.eu/help/development/work_items
- https://docs.gitlab.com/ee/user/tasks.html

Challenges
+++++++++++++++

Issues have the potential to be a centralized hub for collaboration.

We need to accept the fact that different issue types require different
fields and different context, depending on what job they are being
used to accomplish. For example:

- A bug needs to list steps to reproduce.
- An incident needs references to stack traces and other contextual
  information relevant only to that incident.

Instead of each object type diverging into a separate model, we can
standardize on an underlying common model that we can customize with
the widgets (one or more attributes) it contains.


Here are some problems with current issues usage and why we are looking
into work items:

- Using labels to show issue types is cumbersome and makes reporting
  views more complex.
- Issue types are one of the top two use cases of labels, so it makes
  sense to provide first class support for them.

Issues are starting to become cluttered as we add more capabilities to
them, and they are not perfect:

- There is no consistent pattern for how to surface relationships to
  other objects.
- There is not a coherent interaction model across different types of
  issues because we use labels for this.
- The various implementations of issue types lack flexibility and
  extensibility.


Epics, issues, requirements, and others all have similar but just subtle
enough differences in common interactions that the user needs to hold a
complicated mental model of how they each behave.

Issues are not extensible enough to support all of the emerging jobs
they need to facilitate.

Codebase maintainability and feature development becomes a bigger challenge
as we grow the Issue type beyond its core role of issue tracking into
supporting the different work item types and handling logic and structure
differences.

New functionality is typically implemented with first class objects that
import behavior from issues via shared concerns.

This leads to duplicated effort and ultimately small differences between
common interactions. This leads to inconsistent UX


Visualize table of contents in the WYSIWYG wiki editor
======================================================


- https://about.gitlab.com/releases/2022/08/22/gitlab-15-3-released/#visualize-table-of-contents-in-the-wysiwyg-wiki-editor


GitLab 15.3 shows a visual representation of the Table of Contents in
the WYSIWYG wiki editor. To add a table of contents while editing a page,
select the plus (+) icon in the toolbar, then select Table of Contents.

The table of contents updates in real time as you create and edit
subheadings on the page, helping you monitor the outline of your
longer wiki pages.


Create annotated tags using the Release CLI
================================================

- https://about.gitlab.com/releases/2022/08/22/gitlab-15-3-released/#create-annotated-tags-using-the-release-cli
- https://gitlab.com/gitlab-org/release-cli/-/issues/62

Previously, you were only able to create lightweight tags when using
the GitLab Release CLI to create a release. With this update, you
can now add an optional tag-message parameter to create an annotated
tag when creating a release.

This enables you to include relevant information along with the new tag
so downstream users and application can have additional context.




