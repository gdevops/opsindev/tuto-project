
.. _gitlab_13_8:

=============================================================================
Gitlab 13.8 (2021-01-22) released with a Pipeline Editor and DORA metrics
=============================================================================

- https://about.gitlab.com/releases/2021/01/22/gitlab-13-8-released/


Introduction
==============

Today, we are excited to introduce a dedicated Pipeline Editor, a deployment
frequency dashboard, and several quality of life improvements that will
make using GitLab every day even more enjoyable.

These are just a few highlights from the **50+ improvements** in this release.


.. _gitlab_send_email_2021_01_22:

Send an email to an issue
==============================

.. seealso::

   - https://docs.gitlab.com/ee/user/project/issues/issue_data_and_actions.html#email

To more efficiently integrate GitLab into your email workflows, each
issue now has a unique email address. Emailing an issue creates a new
comment on your behalf.

You can now say goodbye to manually copying email content and attachments
into your GitLab issues.



Quickly edit Wiki's sidebar
===============================

.. seealso::

   - https://gitlab.com/lifrank1994
   - https://docs.gitlab.com/ee/user/project/wiki/#customizing-sidebar

Creating a Markdown file named **_sidebar** in the Wiki will use the
contents of that file to generate a custom sidebar navigation menu for
your project.

Editing this file, however, was tricky as there was nowhere in the
interface to open _sidebar again.

Thanks to a wonderful community contribution from GitLab user Frank Li,
starting in GitLab 13.8 there is now an Edit sidebar button in the top
right of the Wiki page.

Clicking this button will automatically create the _sidebar file if it
doesn’t already exist and open it in the page editor.
With this quick access, it is more intuitive to create and easier to
maintain your custom Wiki navigation.


Use rich output for Jupyter notebooks
=========================================

.. seealso::

   - https://docs.gitlab.com/ee/user/project/repository/jupyter_notebooks/


Jupyter notebooks are documents that contain live code, equations,
visualizations, and narrative text.

They are widely used for data cleaning and transformation, numerical
simulation, statistical modeling, data visualization, machine learning,
and more.

They are capable of using “rich output” to show an object’s rendered
representation, such as HTML, JPEG, SVG, or LaTeX files.

Previously, opening the notebook in Jupyter was the only way to read it
with rich output.

GitLab 13.8 now shows rich output for Jupyter notebook content by default.

This is an excellent way both to preview changes to your files as well
as consume Jupyter notebook without ever having to leave GitLab.


Improved SAST severity data for JavaScript vulnerabilities
============================================================

.. seealso::

   - https://docs.gitlab.com/ee/user/application_security/sast/analyzers.html#analyzers-data

When available from our security scan analyzers, GitLab Static Application
Security Testing provides severity data for identified vulnerabilities.

We recently updated our JavaScript analyzer, ESLint, to add support for
severity and CWE data.

This data will help increase the usability and accuracy of Security
Approval Rules as fewer vulnerabilities will report ‘Unknown’ severity.

Additionally, this data is shown on vulnerability detail pages providing
more information and links to relevant information about vulnerabilities
making it easier for developers to understand and remediate issues.

In the future, we will augment other analyzers missing vulnerability
metadata and add a mechanism to allow customized vulnerability metadata
enabling organizations to tailor results to match their risk profiles.

Improved pipeline status email subject line
================================================

.. seealso::

   - https://docs.gitlab.com/ee/user/profile/notifications.html

GitLab sends email notifications when your pipeline succeeds, fails, or
is fixed.

In previous releases, these emails looked similar. This made it hard to
tell the pipeline status without reading the entire body of the email.

This release updates the subject line of these email notifications so
you can determine pipeline status with a quick glance.


Single-node instances will now upgrade to PostgreSQL 12 by default
===================================================================

In GitLab 14.0, we plan to require PostgreSQL 12.

PostgreSQL 12 offers significant indexing and partitioning benefits,
along with broader performance improvements.

To prepare, single-node instances using the Omnibus-packaged version of
Postgres will now automatically upgrade to version 12 by default.
If you prefer, you can opt-out of automatic upgrades.

Multi-node instances, and any Geo secondaries, will need to switch from
repmgr to Patroni, prior to upgrading with Patroni.

Geo secondaries can then be updated and re-synchronized.


.. _gitlab_default_main:

Git default branch name change (master => main)
====================================================

- :ref:`tuto_git:git_master_to_main`

Every Git repository has an initial branch.

It’s the first branch to be created automatically when you create a new
repository.

**By default, this initial branch is named master**.

**Git version 2.31.0 (scheduled for release March 15th, 2021) will change
the default branch name in Git from master to main.**

In coordination with the Git project and the broader community, GitLab
will be changing the default branch name for new projects on both our
Saas (GitLab.com) and self-managed offerings **starting with GitLab 14.0**.

This will not affect existing projects.

For more information, see the related `epic <https://gitlab.com/groups/gitlab-org/-/epics/3600>`_
and the `Git mailing list discussion <https://lore.kernel.org/git/xmqqa6vf437i.fsf@gitster.c.googlers.com/T/#t>`_

Deprecation date: Mar 22, 2021


PostgreSQL 11 support
=========================

PostgreSQL 12 will be the minimum required version in GitLab 14.0.

It offers significant improvements to indexing, partitioning, and general
performance benefits.

Starting in GitLab 13.7, all new installations default to version 12.

In GitLab 13.8, single-node instances are automatically upgraded as well.

If you aren’t ready to upgrade, you can opt-out of automatic upgrades.

Multi-node database instances will need to switch from repmgr to Patroni,
prior to upgrading with Patroni. Geo secondaries can then be updated and
re-synchronized.
