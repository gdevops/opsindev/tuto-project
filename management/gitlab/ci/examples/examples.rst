.. index::
   pair: Gitlab-ci ; examples

.. _gitlab_ci_examples:

==============================
Gitlab-ci examples
==============================

.. seealso::

   - https://docs.gitlab.com/ce/ci/examples/README.html
