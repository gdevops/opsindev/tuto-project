.. index::
   pair: Gitlab-ci ; Continuous Integration
   ! Gitlab-ci

.. _gitlab_ci:

=====================================================
Gitlab-ci (Enterprise Edition + Community Edition)
=====================================================


- https://gitlab.com/gitlab-org/gitlab-ee/tree/master/lib/gitlab/ci
- https://docs.gitlab.com/ee/ci/README.html

.. toctree::
   :maxdepth: 3

   introduction/introduction
   pipelines/pipelines
   environments/environments
   variables/variables
   feature_flags/feature_flags
   examples/examples
   autodevops/autodevops
   schedules/schedules
   review_apps/review_apps
   pages/pages
   yaml/yaml
