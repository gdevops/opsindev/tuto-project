.. index::
   pair: Gitlab-ci ; schedules

.. _gitlab_ci_schedules:

==============================
Gitlab-ci schedules
==============================

.. seealso::

   - https://docs.gitlab.com/ce/user/project/pipelines/schedules.html
   - https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/project/pipelines/schedules.md


