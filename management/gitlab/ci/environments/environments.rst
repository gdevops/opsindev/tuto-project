.. index::
   pair: Gitlab-ci ; environments

.. _gitlab_ci_environments:

==============================
Gitlab-ci environments
==============================

.. seealso::

   - https://docs.gitlab.com/ee/ci/environments.html
   - https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/ci/environments.md


