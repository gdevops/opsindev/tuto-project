.. index::
   pair: Gitlab-ci ; introduction

.. _gitlab_ci_intro:

==============================
Gitlab-ci introduction
==============================

.. seealso::

   - https://docs.gitlab.com/ee/ci/introduction/
   - https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/ci/introduction/index.md




Introduction to GitLab CI/CD
=============================

GitLab CI/CD is a powerful tool built into GitLab that allows you to
apply all the continuous methods (Continuous Integration, Delivery, and
Deployment) to your software with no third-party application or
integration needed.

How GitLab CI/CD works
=========================

To use GitLab CI/CD, all you need is an application codebase hosted in a
Git repository, and for your build, test, and deployment scripts to be
specified in a file called ```.gitlab-ci.yml`` <../yaml/README.md>`__,
located in the root path of your repository.

In this file, you can define the scripts you want to run, define include
and cache dependencies, choose commands you want to run in sequence and
those you want to run in parallel, define where you want to deploy your
app, and specify whether you will want to run the scripts automatically
or trigger any of them manually. Once you're familiar with GitLab CI/CD
you can add more advanced steps into the configuration file.

To add scripts to that file, you'll need to organize them in a sequence
that suits your application and are in accordance with the tests you
wish to perform. To visualize the process, imagine that all the scripts
you add to the configuration file are the same as the commands you run
on a terminal in your computer.

Once you've added your ``.gitlab-ci.yml`` configuration file to your
repository, GitLab will detect it and run your scripts with the tool
called `GitLab Runner <https://docs.gitlab.com/runner/>`__, which works
similarly to your terminal.

The scripts are grouped into **jobs**, and together they compose a
**pipeline**. A minimalist example of ``.gitlab-ci.yml`` file could
contain:

.. code:: yaml

    before_script:
      - apt-get install rubygems ruby-dev -y

    run-test:
      script:
        - ruby --version

The ``before_script`` attribute would install the dependencies for your
app before running anything, and a **job** called ``run-test`` would
print the Ruby version of the current system. Both of them compose a
**pipeline** triggered at every push to any branch of the repository.

GitLab CI/CD not only executes the jobs you've set, but also shows you
what's happening during execution, as you would see in your terminal:



You create the strategy for your app and GitLab runs the pipeline for
you according to what you've defined. Your pipeline status is also
displayed by GitLab:


At the end, if anything goes wrong, you can easily `roll
back <../environments.md#retrying-and-rolling-back>`__ all the changes:
