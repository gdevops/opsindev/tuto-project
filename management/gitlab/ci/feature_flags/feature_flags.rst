.. index::
   pair: Gitlab; feature flags

.. _gitlab_feature_flags:

========================================
Gitlab feature flags
========================================

.. seealso::

   - https://docs.gitlab.com/ee/user/project/operations/feature_flags.html
   - https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/project/operations/feature_flags.md


