.. index::
   pair: Gitlab; pages

.. _gitlab_pages:

========================================
Gitlab pages
========================================

.. seealso::

   - https://docs.gitlab.com/ce/user/project/pages/
   - https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/project/pages/index.md


