
.. _gitlab_variable_types:

================
Variable types
================

.. seealso::

   - https://docs.gitlab.com/ee/ci/variables/#custom-environment-variables
   - https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-envvars.html
   - https://kubernetes.io/docs/concepts/configuration/organize-cluster-access-kubeconfig/#the-kubeconfig-environment-variable




.. versionadded:: 11.11
   Variable types

   :ref:`gitlab_11_11`


Description
==============

There are **two types of variables** supported by GitLab:

- “Variable”: the Runner will create an environment variable named same as the
  variable key and set its value to the variable value.
- "File”: the Runner will write the variable value to a temporary file and set
  the path to this file as the value of an environment variable named same as
  the variable key.

Many tools (like AWS CLI and kubectl) provide the ability to customise
configuration using files by either providing the file path as a command
line argument or an environment variable.

Prior to the introduction of variable types, the common pattern was to use
the value of a CI variable, save it in a file, and then use the newly
created file in your script:
