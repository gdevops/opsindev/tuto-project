.. index::
   pair: Gitlab-ci ; variables

.. _gitlab_ci_variables:

==============================
Gitlab-ci variables
==============================

.. seealso::

   - https://docs.gitlab.com/ee/ci/variables/
   - https://docs.gitlab.com/ee/ci/variables/README.html
   - https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/ci/variables/README.md

.. toctree::
   :maxdepth: 3

   variable_types/variable_types
