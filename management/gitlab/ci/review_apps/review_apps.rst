.. index::
   pair: Gitlab-ci ; review_apps

.. _gitlab_ci_review_apps:

=====================================================
Gitlab-ci review_apps
=====================================================

.. seealso::

   - https://docs.gitlab.com/ce/ci/review_apps/
   - https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/ci/review_apps/index.md
