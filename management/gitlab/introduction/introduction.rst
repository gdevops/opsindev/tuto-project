.. index::
   pair: Introduction ; Gitlab

.. _gitlab_intro:

====================
Gitlab introduction
====================

.. seealso::

   - https://en.wikipedia.org/wiki/GitLab
   - https://about.gitlab.com/
   - https://x.com/sytses (Sid Sijbrandij, CEO de Gitlab)




Description
=============

GitLab is a web-based Git-repository manager with wiki and issue-tracking
features, using an open-source license, developed by GitLab Inc.

The software was written by Dmitriy Zaporozhets.
As of December 2016, the company has 150 team members and more than
1400 open-source contributors.


Wikipedia definition
=====================

.. seealso::

   - https://en.wikipedia.org/wiki/GitLab


GitLab is a web-based DevOps lifecycle tool that provides a Git-repository
manager providing wiki, issue-tracking and CI/CD pipeline features, using
an open-source license, developed by GitLab Inc.

The software was created by Dmitriy Zaporozhets and Valery Sizov and is
used by several large tech companies including IBM, Sony, Jülich Research
Center, NASA, Alibaba, Invincea, O’Reilly Media, Leibniz-Rechenzentrum (LRZ),
CERN,[9][10][11] European XFEL, GNOME Foundation, Boeing, Autodata, and SpaceX.

The code was originally written in Ruby[13], with some parts later
rewritten in Go, initially as a source code management solution to collaborate
with his team on software development.

It later evolved to an integrated solution covering the software
development life cycle, and then to the whole DevOps life cycle.

The current technology stack includes Go, Ruby on Rails and Vue.js.


gitlab CE definition
======================

.. seealso::

   - https://gitlab.com/gitlab-org/gitlab-ce


GitLab Community Edition (CE) is an open source end-to-end software
development platform with built-in version control, issue tracking,
code review, CI/CD, and more. Self-host GitLab CE on your own servers,
in a container, or on a cloud provider.
