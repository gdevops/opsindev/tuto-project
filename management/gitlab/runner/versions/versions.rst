.. index::
   pair: Gitlab runner; Versions

.. _gitlab_runner_versions:

==============================
Gitlab runner versions
==============================


.. seealso::

   - https://gitlab.com/gitlab-org/gitlab-runner/blob/master/CHANGELOG.md
   - https://gitlab.com/gitlab-org/gitlab-runner/tags


.. toctree::
   :maxdepth: 3

   11.11/11.11
   11.10/11.10
