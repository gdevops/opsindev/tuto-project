.. index::
   pair: Gitlab ; runner
   ! Gitlab runner

.. _gitlab_runner:

==================================================================================================================================
Gitlab runner (GitLab Runner is the open source project that is used to run your CI/CD jobs and send the results back to GitLab)
==================================================================================================================================

.. seealso::

   - https://gitlab.com/gitlab-org/gitlab-runner
   - https://docs.gitlab.com/runner/

.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
