

.. index::
   ! Glosary

.. _glossary_project:

=========
Glossary
=========

.. glossary::


   Continuous Delivery
       Continuous Delivery is a step beyond Continuous Integration.
       Not only is your application built and tested each time a code change
       is pushed to the codebase, the application is also deployed continuously.

       However, **with continuous delivery, you trigger the deployments manually**

       :Source: https://docs.gitlab.com/ee/ci/introduction/index.html#continuous-deployment

   DAST
       Dynamic Application Security Testing

   SAST
   Static Application Security Testing
       Static Application Security Testing
