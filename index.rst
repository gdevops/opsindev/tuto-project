.. Tuto project documentation master file, created by
   sphinx-quickstart on Sat May 19 12:25:55 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


|FluxWeb| `RSS <https://gdevops.frama.io/opsindev/tuto-project/rss.xml>`_

.. _project_tuto:
.. _tuto_project:

======================================
**Tuto project management (gitlab)**
======================================

- https://gdevops.frama.io/dev/tuto-project/
- https://gitlab.com/gdevops/tuto_project
- https://gitlab.com/gdevops
- :ref:`genindex`


.. toctree::
   :maxdepth: 3

   architecture/architecture

.. toctree::
   :maxdepth: 5

   news/news

.. toctree::
   :maxdepth: 3

   culture/culture
   management/management

.. toctree::
   :maxdepth: 5

   communication/communication

.. toctree::
   :maxdepth: 3

   ci_cd/ci_cd
   software_quality/software_quality

.. toctree::
   :maxdepth: 4

   tests/tests

.. toctree::
   :maxdepth: 3

   glossary/glossary
