.. index::
   ! Project communication

.. _project_communication:

=======================
Communication
=======================

.. toctree::
   :maxdepth: 6

   collaborative/collaborative
   date/date
   instantanee/instantanee
   mobilizon/mobilizon
   securisee/securisee
   texte/texte
   video/video
   vocabulaire_francais/vocabulaire_francais
