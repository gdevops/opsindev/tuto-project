
.. _joe_2021_03_09:

===========================================================================================================================
JORF n°0058 du mardi 9 mars 2021 **Vocabulaire de l'informatique (liste de termes, expressions et définitions adoptés)**
===========================================================================================================================

.. seealso::

   - https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000043228194

:download:`Vocabulaire de l'informatique au format PDF <joe_2021_03_09_0058_0094.pdf>`




JORF n°0058 du 9 mars 2021
===========================

::

    NOR : CTNR2106373K
    JORF n°0058 du 9 mars 2021
    Texte n° 94


I. Termes et définitions
=============================

données FAIR
-------------

Forme développée : données facilement accessibles, interopérables et réutilisables.
Domaine : Informatique-Télécommunications.
Définition : Données dont l'identification, la description normalisée, les
conditions d'accès techniques ou juridiques et le type de licence facilitent
leur mise à disposition et leur exploitation par les personnes intéressées.
Note :
1. Les données FAIR sont principalement utilisées dans les domaines scientifiques.
2. Les données FAIR sont généralement des données liées ou des données ouvertes.
Voir aussi : données liées, données ouvertes.
Équivalent étranger : FAIR data, findable accessible interoperable reusable data.

données liées
--------------

Domaine : Informatique-Télécommunications.
Définition : Données dont la description est normalisée, ce qui permet de les lier, via leur identifiant universel de ressource, avec des données provenant d'autres sources et décrites de la même façon.
Voir aussi : donnée, données ouvertes, graphe de connaissances, identifiant universel de ressource, toile sémantique.
Équivalent étranger : linked data.
faille non corrigée
Domaine : Informatique.
Synonyme : vulnérabilité non corrigée.
Définition : Faille identifiée par des utilisateurs d'un système informatique, à laquelle il n'existe pas encore de parade.
Note : Une faille non corrigée peut être exploitée à des fins malveillantes.
Voir aussi : prime à la faille détectée.
Équivalent étranger : zero-day, zero-day flaw, zero-day vulnerability.


identifiant de ressource internationalisé
---------------------------------------------

Abréviation : IRI.
Domaine : Informatique-Télécommunications.
Définition : Identifiant universel de ressource qui prend en compte les caractères utilisés par les différentes langues du monde, grâce à un répertoire universel dans lequel sont codés ces caractères.
Voir aussi : adresse universelle, données liées, identifiant universel de ressource.
Équivalent étranger : internationalized resource identifier (IRI).
identifiant universel de ressource
Abréviation : IUR.
Forme abrégée : identifiant universel.
Domaine : Informatique-Télécommunications.
Définition : Dénomination conforme à une norme de l'internet, qui permet d'identifier de façon univoque et pérenne dans l'internet une ressource abstraite ou physique.
Note : Une adresse universelle est un type d'identifiant universel de ressource.
Voir aussi : adresse universelle, données liées.
Équivalent étranger : uniform resource identifier (URI)

numérique, n.m.
------------------

Domaine : Informatique-Télécommunications.
Définition : Ensemble des disciplines scientifiques et techniques, des activités économiques et des pratiques sociétales fondées sur le traitement de données numériques.
Voir aussi : numérique (adj.).
Équivalent étranger : digital.


objet personnel connecté
-------------------------

Domaine : Tous domaines.
Définition : Objet connecté que l'on porte sur soi, qui peut être un vêtement ou un accessoire.
Note :
1. Un objet personnel connecté peut être une montre, une paire de lunettes, un bijou, une paire de chaussures ou encore une ceinture.
2. Un objet personnel connecté contribue, par exemple, à la surveillance médicale ou à l'évaluation des performances physiques de son porteur.
Voir aussi : automesure connectée, objet connecté.
Équivalent étranger : wearable device.
Attention : Cette publication annule et remplace celle du terme « cybervêtement » au Journal officiel du 18 mars 2011.


porte dérobée
--------------

Domaine : Informatique.
Synonyme : poterne logicielle, poterne, n.f.
Définition : Point d'entrée d'un logiciel, mis en place lors de la conception ou résultant d'une erreur survenue lors du développement, qui permet aux spécialistes d'agir sur ce logiciel.
Note : Une porte dérobée peut être utilisée lors d'une opération de dépannage ou être exploitée pour conduire une cyberattaque.
Voir aussi : cyberattaque.
Équivalent étranger : backdoor, trapdoor.


préproduction, n.f.
---------------------

Domaine : Informatique.
Définition : Processus consistant à assembler les modules d'une nouvelle application informatique et à tester cette dernière avant de la mettre en service.
Équivalent étranger : staging.
science des données
Domaine : Informatique-Télécommunications.
Définition : Discipline qui fait appel à des méthodes statistiques, mathématiques et informatiques pour analyser des données, en particulier des mégadonnées, afin d'en extraire toute information utile.
Voir aussi : expert en mégadonnées, mégadonnées.
Équivalent étranger : data science.


toile sémantique
------------------

Domaine : Informatique/Internet.
Définition : Partie de la toile dont les données sont structurées et liées de manière à faciliter leur traitement automatique et à améliorer la pertinence des résultats de recherche.
Voir aussi : données liées, graphe de connaissances, toile.
Équivalent étranger : semantic web.
Attention : Cette publication annule et remplace celle du Journal officiel du 27 décembre 2009.


Table d'équivalence Termes anglais
=============================================

backdoor, trapdoor.
--------------------

Informatique.
porte dérobée, poterne logicielle, poterne, n.f.

data science.
-----------------

Informatique-Télécommunications.
science des données.

digital.
-----------

Informatique-Télécommunications.
numérique, n.m.

FAIR data, findable accessible interoperable reusable data.
-------------------------------------------------------------

Informatique-Télécommunications.
données FAIR, données facilement accessibles, interopérables et réutilisables.

internationalized resource identifier (IRI).
----------------------------------------------

Informatique-Télécommunications.
identifiant de ressource internationalisé (IRI).

linked data.
---------------

Informatique-Télécommunications.
données liées.

semantic web.
--------------

Informatique/Internet.
toile sémantique.

staging.
------------

Informatique.
préproduction, n.f.

trapdoor, backdoor.
---------------------

Informatique.
porte dérobée, poterne logicielle, poterne, n.f.

uniform resource identifier (URI).
-------------------------------------

Informatique-Télécommunications.
identifiant universel de ressource (IUR), identifiant universel.

wearable device.
-------------------

Tous domaines.
objet personnel connecté.

zero-day, zero-day flaw, zero-day vulnerability.
--------------------------------------------------

Informatique.
faille non corrigée, vulnérabilité non corrigée.

(1) Il s'agit de termes anglais, sauf mention contraire.
(2) Les termes en caractères gras sont définis dans la partie I (Termes et définitions).


Table d'équivalence  Termes français
=============================================

données FAIR, données facilement accessibles, interopérables et réutilisables.
---------------------------------------------------------------------------------

Informatique-Télécommunications.
FAIR data, findable accessible interoperable reusable data.

données liées.
---------------

Informatique-Télécommunications.
linked data.

faille non corrigée, vulnérabilité non corrigée.
--------------------------------------------------

Informatique.
zero-day, zero-day flaw, zero-day vulnerability.

identifiant de ressource internationalisé (IRI).
---------------------------------------------------

Informatique-Télécommunications.
internationalized resource identifier (IRI).

identifiant universel de ressource (IUR), identifiant universel.
------------------------------------------------------------------

Informatique-Télécommunications.
uniform resource identifier (URI).

numérique, n.m.
------------------

Informatique-Télécommunications.
digital.

objet personnel connecté.
---------------------------

Tous domaines.
wearable device.

porte dérobée, poterne logicielle, poterne, n.f.
---------------------------------------------------

Informatique.
backdoor, trapdoor.

préproduction, n.f.
---------------------

Informatique.
staging.

science des données.
-----------------------

Informatique-Télécommunications.
data science.

toile sémantique.
-------------------

Informatique/Internet.
semantic web.

vulnérabilité non corrigée, faille non corrigée.
---------------------------------------------------

Informatique.
zero-day, zero-day flaw, zero-day vulnerability.

(1) Les termes en caractères gras sont définis dans la partie I (Termes et définitions).
(2) Il s'agit d'équivalents anglais, sauf mention contraire.
