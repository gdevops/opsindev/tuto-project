
.. _comm_texte:

=======================
Communication (texte)
=======================

.. toctree::
   :maxdepth: 3

   discourse/discourse
   mattermost/mattermost
   mailman3/mailman3
