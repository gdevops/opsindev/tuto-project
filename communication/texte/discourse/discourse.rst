.. index::
   pair: Communication ; discourse
   ! discourse

.. _discourse:

================================================================
discourse
================================================================

.. seealso::

   - https://github.com/discourse/discourse
   - https://meta.discourse.org/
   - https://docs.discourse.org/
   - https://www.webarchitects.coop/discourse


.. toctree::
   :maxdepth: 3

   install/install
   used_by/used_by
