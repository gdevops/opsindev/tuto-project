.. index::
   pair: Project management ; mattermost
   ! mattermost

.. _mattermost:

================================================================
mattermost (Open source Slack-alternative in Golang and React)
================================================================

.. seealso::

   - https://en.wikipedia.org/wiki/Mattermost
   - https://github.com/mattermost/mattermost-server
   - https://docs.mattermost.com/
   - https://github.com/mattermost/docs

.. toctree::
   :maxdepth: 3

   versions/versions
   sphinx/sphinx
