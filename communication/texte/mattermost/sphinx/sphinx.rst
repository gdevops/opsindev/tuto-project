.. index::
   pair: sphinx ; mattermost

.. _mattermost_sphinx:

================================================================
mattermost sphinx
================================================================

.. seealso::

   - https://docs.mattermost.com/
   - https://github.com/mattermost/docs
   - https://github.com/mattermost/docs/blob/master/source/conf.py


.. toctree::
   :maxdepth: 3


conf.py
========

.. literalinclude:: python/conf.py
   :linenos:
