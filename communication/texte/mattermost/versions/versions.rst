
.. index::
   pair: Versions ; mattermost

.. _mattermost_versions:

====================
mattermost versions
====================

.. seealso::

   - https://github.com/mattermost/mattermost-server/releases
   - https://docs.mattermost.com/administration/changelog.html
   - https://github.com/mattermost/docs/blob/master/source/administration/changelog.md

.. toctree::
   :maxdepth: 3

   5.10.0/5.10.0
