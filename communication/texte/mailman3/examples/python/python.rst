.. index::
   pair: python; mailman3

.. _mailman3_python:

===========================
mail.python.org/archives
===========================

.. seealso::

   - https://mail.python.org/archives/


.. figure:: mailman3_python.webp
   :align: center
