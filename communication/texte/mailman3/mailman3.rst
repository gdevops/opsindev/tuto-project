.. index::
   pair: Communication; mailman3

.. _mailman3:

=======================
**mailman3**
=======================

.. seealso::

   - https://gitlab.com/mailman
   - https://docs.mailman3.org/en/latest/


.. toctree::
   :maxdepth: 3

   examples/examples
   versions/versions
