
.. _mailman3_versions:

=======================
Versions
=======================

.. seealso::

   - https://gitlab.com/mailman
   - https://docs.mailman3.org/en/latest/
