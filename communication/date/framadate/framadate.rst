.. index::
   pair: communications; framadate

.. _framadate:

=======================
**framadate**
=======================

.. seealso::

   - https://framadate.org/
   - https://stats.chatons.org/category-aidealaprisederendezvous.xhtml
   - https://framagit.org/framasoft/framadate/framadate
   - https://framagit.org/framasoft/framadate/framadate/-/boards


.. figure::  description/framadate.webp
   :align: center

.. toctree::
   :maxdepth: 3

   description/description


