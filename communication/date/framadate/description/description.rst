.. index::
   pair: description; framadate

.. _description:

=======================
Description
=======================

.. seealso::

   - https://fr.wikipedia.org/wiki/Liste_des_produits_et_services_de_Framasoft#Framadate




.. figure::  framadate.webp
   :align: center


Description
============

**Framadate** est un service en ligne permettant de planifier un rendez-vous,
un synchronidateur, sans inscription préalable18, à la manière de Doodle.

Il est basé sur le logiciel libre OpenSondage, lui-même basé sur le
logiciel STUdS ! développé par l'Université de Strasbourg


