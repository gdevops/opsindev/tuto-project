.. index::
   pair: communications; date

.. _comm_date:

=======================
**date**
=======================

.. seealso::

   - https://stats.chatons.org/category-aidealaprisederendezvous.xhtml
   - https://forum.chatons.org/t/une-alternative-a-framadate-opensondage-dans-yunohost-et-polls-dans-nextcloud/1743

.. toctree::
   :maxdepth: 3

   chapril/chapril
   framadate/framadate
   gresille/gresille
   nomagic/nomagic


