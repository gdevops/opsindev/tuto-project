
.. _instances_mobilizon:

=======================
Instances
=======================

.. seealso::

   - https://instances.joinmobilizon.org/instances
   - https://framagit.org/framasoft/joinmobilizon/instances-mobilizon
   - https://framagit.org/framasoft/joinmobilizon/instances-mobilizon/-/boards


.. toctree::
   :maxdepth: 3

   chapril/chapril
   picasoft/picasoft
