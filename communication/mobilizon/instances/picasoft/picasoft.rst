
.. _instances_mobilizon_picasoft:

===================================
https://mobilizon.picasoft.net/
===================================

.. seealso::

   - https://mobilizon.picasoft.net/
   - https://instances.joinmobilizon.org/instances


.. figure:: picasoft.webp
   :align: center
