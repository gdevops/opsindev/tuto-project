
.. _instances_mobilizon_chapril:

===================================
https://mobilizon.chapril.org/
===================================

.. seealso::

   - https://mobilizon.chapril.org/
   - https://mobilizon.chapril.org/about/instance
   - https://instances.joinmobilizon.org/instances



instance du `Chapril <https://www.chapril.org>`_, voir https://www.chapril.org

.. figure:: mobilizon_chapril.png
   :align: center


Règles de l'instance
======================

.. seealso:: https://mobilizon.chapril.org/rules


soyez des gens bons.
