.. index::
   ! Mobilizon

.. _mobilizon:

=======================
Mobilizon
=======================

.. seealso::

   - https://mobilizon.org/fr/
   - https://x.com/joinmobilizon
   - https://framagit.org/framasoft/mobilizon
   - https://framagit.org/framasoft/mobilizon/-/boards
   - https://framagit.org/framasoft/mobilizon/-/labels
   - https://joinmobilizon.org/fr/
   - https://riot.im/app/#/room/#Mobilizon:matrix.org


.. figure:: exemple.webp
   :align: center



.. toctree::
   :maxdepth: 3

   definition/definition
   doc/doc
   faq/faq
   articles/articles
   forum/forum
   installation/installation
   instances/instances
