
.. _mobilizon_def:

=======================
Description
=======================

.. seealso::

   - https://mobilizon.org/fr/
   - https://joinmobilizon.org/fr/





Un outil pratique
====================

Mobilizon est un outil qui vous permet de trouver, créer et organiser
des événements.

Vous pouvez aussi y publier une page pour votre groupe où les membres
pourront s’organiser ensemble.


Une alternative éthique
==========================

Alternative éthique aux **événements**, groupes et pages Facebook, Mobilizon
est un outil conçu pour vous servir. Point.

Pas de like, de follow, ni de mur au scroll infini : Mobilizon vous laisse
la maîtrise de votre attention.


Un logiciel fédéré
=====================

Mobilizon n’est pas une plateforme géante, mais une multitude de sites
web Mobilizon interconnectés.

Cette architecture fédérée permet d’éviter les monopoles et d’offrir une
diversité des conditions d’hébergement.

Mobilizon est un outil en ligne pour vous aider à gérer vos événements, vos profils et vos groupes.
Vos événements

C’est quoi, Mobilizon ?
=========================

Sur Mobilizon vous pouvez créer une fiche détaillée de votre événement,
le publier et le partager.

Vous pouvez aussi y rechercher des événements par mots-clés, lieux ou dates,
et vous y inscrire (sans forcément avoir besoin d’un compte) et les
ajouter à votre agenda.


Vos profils
============

Créer un compte sur une instance Mobilizon vous permettra de vous créer
plusieurs profils (personnel, professionnel, loisirs, militant, etc.),
d’organiser des événements et d’administrer des groupes.

Pensez à découvrir le fonctionnement d’une instance, décrit sur sa page
« à propos », pour comprendre ses règles et sa politique avant de vous
y créer un compte.

Vos groupes
==============

Dans Mobilizon, chaque groupe dispose d’une page publique où l’on peut
consulter les derniers billets et les événements publics du groupe.

Les membres invités à rejoindre un groupe peuvent participer aux discussions,
et gérer un classeur de ressources communes (lien vers un outil d’écriture
collaborative, un wiki, etc.)


La diversité par la fédération
================================

Mobilizon est un logiciel fédéré : des hébergeurs peuvent l’installer
sur un serveur pour créer autant d’instances, de site-web Mobilizon.

Les instances Mobilizon peuvent se fédérer entre elles afin qu’un profil
inscrit sur l’instance A puisse contribuer à un groupe créé sur l’instance B.

Cette multiplicité permet de diversifier les conditions d’hébergement
(gouvernance, CGU, chartes) et d’éviter la formation de plateformes
monopolistiques.

Le respect de vos libertés
================================

Mobilizon est un logiciel libre.

Cela signifie que son code est transparent, publiquement consultable et
qu’il n’y a pas de fonctionnalité cachée.

Le code est construit de manière communautaire, et chacun·e est libre
de le reprendre pour essayer de mener le projet vers de nouvelles voies.

À vous de jouer
=================

- Tester `la démo de Mobilizon <https://demo.mobilizon.org/>`_
- Trouver votre instance sur Mobilizon.org
- Apprendre à utiliser (et installer) Mobilizon sur notre documentation
- Venir discuter sur notre forum






