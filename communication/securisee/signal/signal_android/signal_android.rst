.. index::
   pair: Communication Sécurisée; signal-android

.. _signal_android:

======================================================
Signal-android (A private messenger for Android.)
======================================================

.. seealso::

   - https://github.com/signalapp/Signal-Android
