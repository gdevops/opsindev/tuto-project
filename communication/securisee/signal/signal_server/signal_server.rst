.. index::
   pair: Communication Sécurisée; signal-server

.. _signal_server:

=======================
Signal-server
=======================

.. seealso::

   - https://github.com/signalapp/Signal-Server
