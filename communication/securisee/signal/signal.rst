.. index::
   pair: Communication Sécurisée; signal

.. _signal:

=======================
Signal
=======================

.. seealso::

   - https://x.com/signalapp
   - https://fr.wikipedia.org/wiki/Signal_%28application%29
   - https://signal.org/fr/
   - https://github.com/signalapp


.. toctree::
   :maxdepth: 3

   signal_server/signal_server
   signal_android/signal_android
   signal_desktop/signal_desktop

