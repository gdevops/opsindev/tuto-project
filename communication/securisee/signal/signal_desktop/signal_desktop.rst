.. index::
   pair: Communication Sécurisée; signal-desktop

.. _signal_desktop:

=========================================================================
Signal-desktop (Signal — Private Messenger for Windows, Mac, and Linux )
=========================================================================

.. seealso::

   - https://github.com/signalapp/Signal-Desktop
