

.. _burubiblio_element_tutorial:

===============================
burubiblio element tutorial
===============================

.. seealso::

   - https://docs.burubiblio.ovh/en/guides-informatique/riot/guide-du-debutant-comment-commencer-avec-element-et-matrix




Introduction
==============

Ces jours-ci, choisir le meilleur logiciel peut s'avérer être une tâche
écrasante, parce que maintenant plus que jamais, le choix est abondant
quand il s'agit de logiciel.

Les applications de messagerie et communications en sont un bon exemple
parce qu'il existe tellement d'options.

Et si vous cherchez une application qui soit évolutive et centrée sur la
confidentialité ? Cela peut s'avérer encore plus difficile.

Grâce à l'équipe d'Element, j'ai une excellente option à vous présenter.

Element, connu auparavant sous le nom de Riot.im, est un client multi-plateformes
qui fonctionne sur le protocole Matrix.

Qu'est que ça veut dire ? Est ce que Néo est l'élu ?

Parlons de ce qu'est ce protocole et pourquoi vous devriez considérez
l'utilisation de cette géniale plate-forme de messagerie.

Qu'est ce que Matrix ?
=========================

L'équipe Matrix le décrit comme un "standard libre et ouvert pour une
communication en temps réel, sécurisée, et décentralisée".

Premièrement c'est open source
-----------------------------------

Premièrement c'est open source, plus de regards équivaut à un meilleur
code.
On sait tous à quel point j'aime l'open source.... mais pour les non-initiés,
cela signifie que plus de gens peuvent lire et étudier le code, les vulnérabilités
du code peuvent être découvertes et réparées plus rapidement, et d'autres
projets et contributeurs peuvent construire des intégrations efficaces
dans le projet source.

Deuxièmement, c'est décentralisé.
---------------------------------

C'est un peu plus compliqué, mais pensez y de cette manière, quand vous
enregistrez sur une instance d'une application décentralisée, c'est
votre espace.
Cela pourrait être pour votre famille, une réunion, ou une entreprise.
Tout serait isolé jusqu'à ce que votre instance joigne la fédération
d'instance.
À partir de là, vous@votreserveurmaison.com pourra atteindre le reste du
monde et joindre par exemple ami@unautreserveur.com. ( ndt: à la même manière que l'email )

Troisièmement, Matrix supporte les communications en temps réel
------------------------------------------------------------------

Cela inclus la collaboration, messagerie, voix et vidéo. Matrix fournit
une série d'algorithmes de chiffrement et de ponts pour faciliter les
uilisateurs et les applications à communiquer entre eux.

Vous pouvez utiliser **Etherpad pour de l'édition de document collaboratif
en temps réel, Jitsi pour la vidéo conférence, et Element pour la messagerie**.

Certains des ponts les plus populaires pour Matrix sont IRC et Gitter
( des plate-formes utilisées traditionnellement par les développeurs ).

La communauté open source en a créé quelqu'uns comme Telegram et Google Hangouts.

Matrix permet la communication entre ces plate-formes au sein d'une
instance ou à travers le réseau interconnecté des instances Matrix
éparpillées sur Internet.

Qu'est ce que Element ?
============================

Si Matrix est le réseau, alors le client Element est le véhicule qui vous
permet de traverser ce réseau ;

Element est l'interface pour le texte, la voix et les conversations vidéos.

Comme le protocole Matrix. le client Element est complètement open source !

Avec la possibilité de créer un pont entre votre différentes applications
comme Slack au sein de l'interface Element, vous n'avez plus besoin
d'installer et de conserver tout une série d'applications simplement
pour rester en contact avec amis, famille, travail, groupes de volontaires,
etc... Vous comprenez.

Créez un compte, activez les intégrations dont vous avez besoin, et
chattez avec n'importe qui, n'importe où, sur n'importe quelle plate-forme.

Profitez de conversations privées ou avec des centaines de participants
dans des salons publics.

Bonus: Une des mes fonctionnalités préférées ?
===================================================

La gestion de notifications.

Je peux avoir n'importe quel type de notification pour n'importe quel salon,
le régler pour être notifié seulement si je suis mentionné, où ne jamais
recevoir de notifications.

Mais Element ne s'arrête pas ici, ils ont un des meilleurs systèmes de
gestion de notifications : les notifications par mots-clés.

Disons que je souhaite être alerté à chaque fois que quelqu'un parle de
chiens.
Je peux ajouter le mot clé à ma configuration et recevoir une notif à
chaque fois que quelqu'un utilise le mot "chien", "chat", etc...

Element propose l'option de rejoindre le serveur central sur Matrix.org,
heberger votre propre serveur, ou payer une cotisation mensuelle pour
gérer votre propre instance sur leur serveur sécurisé.

Depuis leur site web, vous pouvez obtenir jusqu'à 5 utilisateurs par
mois pour seulement 2$/mois (USD).

Création du compte
===================


La prochaine étape est un peu délicate, mais restez avec moi.

Un des principaux avantages de Element dont nous avons discuté est le
chiffrement de bout en bout.
Nous avons besoin de générer une clé de Sécurité ( ce qui est différent
de notre mot de passe ! ).

Typiquement je recommande d'utiliser une clé générée par le système.

Vous pouvez ensuite utiliser une note sécurisée ( ou un gestionnaire de
mot de passe ) pour stocker la clé quelque part.
Elle vous servira à retrouver l'historique de vos conversations si vous
perdez vos comptes.


.. toctree::
   :maxdepth: 3

   creation/creation

