

.. _creation_compte:

===================
Création du compte
===================

.. seealso::

   - https://docs.burubiblio.ovh/en/guides-informatique/riot/guide-du-debutant-comment-commencer-avec-element-et-matrix




Produire une clé de Sécurité
================================

La prochaine étape est un peu délicate, mais restez avec moi.

**Un des principaux avantages de Element dont nous avons discuté est le
chiffrement de bout en bout**.

Nous avons besoin de générer une clé de Sécurité ( ce qui est différent
de notre mot de passe ! ).

Typiquement je recommande d'utiliser une clé générée par le système.

Vous pouvez ensuite utiliser une note sécurisée ( ou un gestionnaire de
mot de passe ) pour stocker la clé quelque part.
Elle vous servira à retrouver l'historique de vos conversations si vous
perdez vos comptes.


