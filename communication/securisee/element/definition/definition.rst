
.. _element_definition:

=======================
element definition
=======================

.. seealso::

   - https://github.com/vector-im/element-web
   - https://fr.wikipedia.org/wiki/Chiffrement_de_bout_en_bout



Wikipedia french definition
=============================


**Element** (précédemment Riot.im) est un logiciel libre de messagerie
instantanée basé sur le protocole Matrix et distribué sous la
licence Apache 2.0.

Avec l'utilisation du **protocole de fédération Matrix**, Element laisse
l'utilisateur choisir le serveur auquel il veut se connecter.

Element supporte:

- le `chiffrement de bout en bout <https://fr.wikipedia.org/wiki/Chiffrement_de_bout_en_bout>`_,
- les groupes (appelés communautés),
- les salons et le partage de fichiers entre utilisateurs.

L'inscription ne requiert pas de numéro de téléphone.

L'accès est possible depuis une application web, un client lourd pour
bureau pour tous les systèmes d'exploitation principaux et en tant
qu'application mobile pour Android et iOS.

Le développement de l'application est notamment effectué par la société
New Vector Limited, qui est aussi impliquée dans le développement du
protocole Matrix.


Histoire
===========

Element a été originellement appelé Vector, quand il est sorti de sa
version bêta en juillet 2016.

L'application a été renommée Riot en septembre de la même année.

Le renommage a été effectué par Canadian brand consultancy LP/AD.

En novembre, la première implémentation du chiffrement de bout en bout
a été publiée en tant que version bêta aux utilisateurs.

En avril 2019, une nouvelle application a été publiée sur le Google Play
Store suite à la compromission du serveur de production sur lequel se
trouvaient les clés de signature de l'application Android.

Les développeurs recommandent que n'importe qui utilisant l'application
provenant du Google Play Store la mette à jour vers la dernière version.

Après avoir annoncé le 23 juin 2020 le changement de nom, Riot.im
(le client de messagerie) et New Vector (la société qui développe Riot)
ont été renommés Element le 15 juillet 2020.

Quant à Modular, l'hébergeur de serveurs fédérés, il est renommé
Element Matrix Services.

En juillet 2020, Element a décroché un contrat auprès du système
d'éducation publique allemand qui permettra à 500 000 personnes
d'utiliser la messagerie.


