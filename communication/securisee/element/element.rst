.. index::
   pair: Communication Sécurisée; element
   ! element

.. _element:

================================
element (a Matrix web client)
================================

.. seealso::

   - https://github.com/vector-im/element-web
   - https://x.com/element_hq
   - https://fr.wikipedia.org/wiki/Element_(logiciel)
   - https://element.io/blog/welcome-to-element/
   - https://element.io/help
   - https://fr.wikipedia.org/wiki/Chiffrement_de_bout_en_bout

.. toctree::
   :maxdepth: 3

   definition/definition
   tutorials/tutorials
