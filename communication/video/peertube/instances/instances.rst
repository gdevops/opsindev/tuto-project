.. index::
   pair: peertube; instances

.. _peertube_instances:

======================================================================================================
peertube instances
======================================================================================================

.. seealso::

   - https://joinpeertube.org/instances#instances-list
   - https://joinpeertube.org/


.. toctree::
   :maxdepth: 3

   fediquebec/fediquebec
   globenet/globenet
