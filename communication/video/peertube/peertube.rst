.. index::
   pair: peertube; Vidéo

.. _peertube:

======================================================================================================
peertube (ActivityPub-federated video streaming platform using P2P directly in your web browser)
======================================================================================================

.. seealso::

   - https://github.com/Chocobozzz/PeerTube
   - https://framagit.org/chocobozzz
   - https://joinpeertube.org/


.. toctree::
   :maxdepth: 3

   instances/instances
