.. index::
   pair: jitsi; solutions

.. _jitsi_solutions:

=======================
jitsi solutions
=======================

.. seealso::

   - https://github.com/jitsi



.. toctree::
   :maxdepth: 3

   kmeet/kmeet
   meet_jitsi/meet_jisi
