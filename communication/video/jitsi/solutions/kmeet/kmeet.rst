.. index::
   pair: jitsi; kmeet
   ! kmeet

.. _kmeet:

==============================================================================
**kmeet** (Solution de visioconférence gratuite et sécurisée basée sur jitsi)
==============================================================================

.. seealso::

   - https://www.infomaniak.com/fr/kmeet



Description
==============

**kMeet** est une solution de visioconférence gratuite et sécurisée qui
respecte la vie privée.

Ce service est basé sur le logiciel open source Jitsi Meet.

Lancement sur Debian
======================

::

    ./infomaniak-meet-x86_64.AppImage --no-sandbox


.. figure:: kmeet.webp
   :align: center
