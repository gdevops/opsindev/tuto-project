.. index::
   pair: jitsi; meet
   ! meet.jit.si

.. _meet_jitsi:

==============================================================================
https://meet.jit.si/
==============================================================================

.. seealso::

   - https://meet.jit.si/


.. figure:: meet_jitsi.webp
   :align: center
