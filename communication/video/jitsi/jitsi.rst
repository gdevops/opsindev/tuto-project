.. index::
   pair: jitsi-meet; Vidéo

.. _jitsi_meet:

=====================================================================================================================================
**jitsi-meet** (Secure, Simple and Scalable Video Conferences that you use as a standalone app or embed in your web application)
=====================================================================================================================================

.. seealso::

   - https://github.com/jitsi/jitsi-meet
   - https://github.com/jitsi/jitsi-meet/graphs/contributors


.. toctree::
   :maxdepth: 3

   solutions/solutions
