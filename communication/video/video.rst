.. index::
   pair: Communication; Vidéo

.. _comm_video:

=======================
Video
=======================

.. toctree::
   :maxdepth: 3

   jitsi/jitsi
   peertube/peertube
