.. index::
   pair: Communication; Collaborative

.. _com_collab:

=======================
Collaborative
=======================

.. toctree::
   :maxdepth: 3

   etherpad/etherpad
   wiki/wiki
