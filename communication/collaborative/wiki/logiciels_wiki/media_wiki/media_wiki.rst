.. index::
   pair: MediaWiki; wiki

.. _MediaWiki:

=======================
**MediaWiki**
=======================

.. seealso::

   - https://fr.wikipedia.org/wiki/MediaWiki
   - https://gerrit.wikimedia.org/g/mediawiki/core/




.. figure::  MediaWiki.svg.png
   :align: center

Définition
===========

**MediaWiki** est un moteur de wiki pour le Web.

Il est utilisé par l’ensemble des projets de la Wikimedia Foundation,
des wikis hébergés chez FANDOM, ainsi que par de nombreux autres wikis.

Conçu pour répondre aux besoins de Wikipédia, ce moteur est en 2008
également utilisé par des entreprises comme solution de gestion des
connaissances et comme système de gestion de contenu.

L’entreprise américaine Novell l’utilise notamment pour plusieurs de
ses sites web qui véhiculent un trafic important.

Des associations, comme Wikitravel, Mozilla ou Ékopedia, l'ont aussi adopté.

MediaWiki est écrit en PHP et peut aussi bien fonctionner avec le
système de gestion de base de données MySQL que PostgreSQL.

C'est un logiciel libre distribué selon les termes de la GPL.

MediaWiki comporte de nombreuses fonctionnalités pour les sites à
vocation collaborative.

Par exemple, la gestion des espaces de noms, ou l'utilisation de pages
de discussions associées à chaque article.
