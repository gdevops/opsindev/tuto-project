.. index::
   pair: Communication; wiki

.. _wiki:

=======================
**wiki**
=======================

.. seealso::

   - https://fr.wikipedia.org/wiki/Wiki


.. toctree::
   :maxdepth: 3

   logiciels_wiki/logiciels_wiki
