.. index::
   pair: Collaboration ; pad

.. _colab_etherpad:

=======================
etherpad
=======================

.. seealso::

   - https://github.com/ether/etherpad-lite


.. toctree::
   :maxdepth: 3

   instances/instances
