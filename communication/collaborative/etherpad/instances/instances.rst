.. index::
   pair: Collaboration ; etherpad

.. _instances_etherpad:

=======================
Instances etherpad
=======================

.. seealso::

   - https://framapad.org/fr/info/
   - https://stats.chatons.org/category-traitementdetextecollaboratif.xhtml




Liste des 24 instances etherpad recommandées
===============================================

.. figure:: 24_instances.webp
   :align: center

   Liste des 24 instances recommandées


Voir aussi https://stats.chatons.org/services.xhtml
=======================================================

.. seealso::

   - https://stats.chatons.org/category-traitementdetextecollaboratif.xhtml

Quelques instances etherpad
=================================

.. toctree::
   :maxdepth: 3

   chapril/chapril
   framapad/framapad
   globenet/globenet
