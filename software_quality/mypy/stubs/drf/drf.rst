.. index::
   pair: Mypy ; DRF stubs


.. _mypy_drf_stubs:

===========================================================================================
Mypy django rest framework stubs
===========================================================================================

.. seealso::

   - https://github.com/mkurnikov/djangorestframework-stubs




Description
================

Mypy stubs for DRF 3.9.x. Supports Python 3.6 and 3.7.


Installation
==============

::

    pip install djangorestframework-stubs

To make mypy aware of the plugin, you need to add::

    [mypy]
    plugins =
        mypy_drf_plugin.main

in your mypy.ini file.
