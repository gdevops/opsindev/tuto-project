.. index::
   pair: Mypy ; Django stubs


.. _mypy_django_stubs:

===========================================================================================
Mypy django stubs
===========================================================================================

.. seealso::

   - https://github.com/mkurnikov/django-stubs


.. figure:: begin_2018_07_22.png
   :align: center




Description
================

.. seealso::

   - https://github.com/mkurnikov/django-stubs


This package contains type stubs and mypy plugin to provide more precise
static types and type inference for Django framework.

Django uses some Python "magic" that makes having precise types for some
code patterns problematic.

This is why we need to accompany the stubs with mypy plugins.

The final goal is to be able to get precise types for most common patterns.

Supports Python 3.6/3.7, and Django 2.1.x series.


Installation
==============

::

    pip install django-stubs

::

    pipenv install django-stubs --dev



To make mypy aware of the plugin, you need to add::


    [mypy]
    plugins =
        mypy_django_plugin.main

    in your mypy.ini file.



History
=========

.. toctree::
   :maxdepth: 3

   history/history
