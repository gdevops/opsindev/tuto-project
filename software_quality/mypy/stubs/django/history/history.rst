
.. _mypy_django_stubs_history:

===========================================================================================
Mypy django stubs history
===========================================================================================

.. seealso::

   - https://github.com/mkurnikov/django-stubs
   - https://github.com/mkurnikov/djangorestframework-stubs
   - https://gitter.im/mypy-django/Lobby
   - https://gitter.im/python/typing
   - https://github.com/python/mypy/issues?utf8=%E2%9C%93&q=is%3Aissue+django





Maxim Kurnikov @mkurnikov Mar 05 2019 22:32
=============================================


I've released DRF plugin to pypi, https://pypi.org/project/djangorestframework-stubs/.
Still alpha quality though, testing is very welcome.

Aleksander Vognild Burkow @aleksanb Feb 02 2019 21:22
=======================================================

trying to set it up in your django project?
add this to your mypy.ini in your project root:

    [mypy]
    plugins =
    mypy_django_plugin.main

install mypy and django-stubs through your pip-wrapper of choice
and run it::

    mypy --config-file mypy.ini -p your-package-of-choice


in our case all django apps are scoped under a namespace so we put the name of the root namespace after -p



Maxim Kurnikov @mkurnikov Jan 25 2019 15:56
==============================================

I use pycharm. "get type hint on hover" seems like a feature for dmypy, see
python/mypy#5929
(it's a little different feature, but related)

For pycharm, there's a plugin
https://github.com/dropbox/mypy-PyCharm-plugin
which makes it easier to work with mypy (though I haven't yet tried it,
I'm on the unsupported pycharm version now).


Maxim Kurnikov @mkurnikov Jan 17 2019 16:50
=============================================

I'm going to release it somewhere around Mon/Wed next week.
For your projects, do you think it's useful enough?


Maxim Kurnikov @mkurnikov Dec 24 2018 11:39
==============================================

@lwm @ethanhs
Official discussion related to django stubs is on the step of writing the DEP
https://groups.google.com/forum/#!topic/django-developers/trTEbURFhEY

He mentioned
https://github.com/django/deps/blob/master/final/0001-dep-process.rst#forming-the-team

I can start working on it, I still have about a month of free time for almost full-time OSS development.


Maxim Kurnikov @mkurnikov Dec 21 2018 20:53
=============================================

There's a project https://github.com/dropbox/sqlalchemy-stubs, which is
developed by the same guys that work on mypy. Because of that, they add
features specifically for plugins, so, yes, of course.


Maxim Kurnikov @mkurnikov Nov 13 2018 17:12
=============================================


Just FYI, I've started working on djangorestframework plugins/stubs project here
https://github.com/mkurnikov/djangorestframework-stubs

And more stuff being here
https://github.com/mkurnikov/django-stubs

I moved generated code to a different directory and decided to start from c
lean state, copying stubs from generated ones, if convenient.



Maxim Kurnikov @mkurnikov Oct 14 2018 19:57
===============================================

I've committed some initial code for django plugin in my fork, it's not
really useful much now though.  https://github.com/mkurnikov/django-stubs

grep for DJANGO_SETTINGS_MODULE, change it to yours, figure out the paths
where to put plugin code and add

[mypy]
plugins = mypy_django_plugin.plugins.field_to_python_type

in the config file, it should allow you to run a plugin on your codebase.
There will be enormous amount of false positives,
I'm eliminating them little by little, but there's still a lot of work required.

I've also mentioned our efforts on https://gitter.im/python/typing lately.



Maxim Kurnikov @mkurnikov Jul 29 2018 19:45
==============================================

Here:
https://github.com/mkurnikov/django-stubs

You can install it as usual, with pip install -e ., mypy can find stubs.

They aren't really useful though, MonkeyType can only generate functions,
so there's A LOT of "some module does not have some attribute" errors.

I don't know whether MonkeyType is able to apply annotations to stub files,
if it is, then we should probably make a stubgen version of Django repo
first, and then apply those annotations.

AFAIK, stubgen can generate global variables and imports.
I'll post a code with which I generated those later. I had to patch
MonkeyType and create some stuff on top of it to make it work.

Maxim Kurnikov @mkurnikov 2018-07-28
======================================


Also, https://github.com/zulip/zulip uses Django, and has mypy types
already inline. We should ask them for help, or maybe they've created
django stubs packages already.
