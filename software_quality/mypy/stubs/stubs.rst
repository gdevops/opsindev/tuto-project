.. index::
   pair: Mypy ; stubs


.. _mypy_stubs:

===========================================================================================
Mypy stubs
===========================================================================================

.. seealso::

   - https://mypy.readthedocs.io/en/latest/index.html
   - https://github.com/python/mypy/tree/master/docs


.. toctree::
   :maxdepth: 3

   django/django
   drf/drf
