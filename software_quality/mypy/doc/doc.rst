.. index::
   pair: Mypy ; documentation
   pair: Python3 ; Typing


.. _mypt_documentation:

===========================================================================================
Mypy documentation
===========================================================================================

.. seealso::

   - https://mypy.readthedocs.io/en/latest/index.html
   - https://github.com/python/mypy/tree/master/docs





Python 3 cheat sheet
=======================

.. seealso::

   - https://mypy.readthedocs.io/en/latest/cheat_sheet_py3.html
