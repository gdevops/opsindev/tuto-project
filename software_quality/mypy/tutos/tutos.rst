.. index::
   pair: Python ; typing tutorials


.. _python_typing_tutorials:

===========================================================================================
Python typing tutorials
===========================================================================================



https://realpython.com/python-type-checking/
=============================================

.. seealso::

   - https://realpython.com/python-type-checking/



https://docs.python.org/3/library/typing.html
=================================================

.. seealso::

   - https://docs.python.org/3/library/typing.html
