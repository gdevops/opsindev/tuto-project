.. index::
   pair: mypy ; framework
   ! mypy


.. _mypy:

===========================================================================================
**Mypy** : a program that will type check your Python code.
===========================================================================================

.. seealso::

   - http://mypy-lang.org/
   - https://github.com/python/mypy
   - https://mypy.readthedocs.io/en/latest/index.html
   - https://mypy-lang.blogspot.com/
   - https://realpython.com/python-type-checking/#the-mypy-project
   - https://www.python.org/dev/peps/pep-0561/
   - https://www.python.org/dev/peps/pep-0544/
   - https://www.python.org/dev/peps/pep-0526/
   - https://www.python.org/dev/peps/pep-0484/
   - https://www.python.org/dev/peps/pep-0483/


.. figure:: logo_mypy.svg
   :align: center




Definition 1 : What is mypy ?
================================

.. seealso::

   - https://github.com/python/mypy


**Mypy** is an optional static type checker for Python.

You can add type hints (`PEP 484`_) to your Python programs, and use mypy
to type check them statically.

Find bugs in your programs without even running them !

You can mix dynamic and static typing in your programs.

You can always fall back to dynamic typing when static typing is not
convenient, such as for legacy code.

Here is a small example to whet your appetite (Python 3)


.. code-block:: python
   :linenos:

    from typing import Iterator

    def fib(n: int) -> Iterator[int]:
        a, b = 0, 1
        while a < n:
            yield a
            a, b = b, a + b


.. _`PEP 484`:  https://www.python.org/dev/peps/pep-0484/


Definition 2
=============

.. seealso::

   - http://mypy-lang.org/

Mypy is an experimental optional static type checker for Python that
aims to combine the benefits of dynamic (or "duck") typing and static
typing.

Mypy combines the expressive power and convenience of Python with a
powerful type system and compile-time type checking.

Mypy type checks standard Python programs; run them using any Python
VM with basically no runtime overhead.

Mypy is still in development. Most Python features are supported.


Mypy blog
===================


.. toctree::
   :maxdepth: 3

   blog/blog


Mypy Help
===========

.. toctree::
   :maxdepth: 3

   help/help


Mypy documentation
===================


.. toctree::
   :maxdepth: 3

   doc/doc


Mypy stubs
===================


.. toctree::
   :maxdepth: 5

   stubs/stubs

Python Typing Tutorial
=========================

.. toctree::
   :maxdepth: 3

   tutos/tutos
