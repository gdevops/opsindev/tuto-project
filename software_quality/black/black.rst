.. index::
   pair: format ; black
   pair: Python ; black
   pair: pre-commit ; git
   ! black

.. _black:

===================================================
black (The uncompromising Python code formatter)
===================================================

.. seealso::

   - https://github.com/ambv/black
   - https://black.readthedocs.io/en/stable/
   - :ref:`black_hook`



.. figure:: logo_black.png
   :align: center





Installation
==============

::

    pipenv install black --dev




black with git pre-commit (.pre-commit-config.yaml)
======================================================

.. seealso::

   - https://black.readthedocs.io/en/stable/version_control_integration.html

::

    # .pre-commit-config.yaml
    # ========================
    #
    # - https://gdevops.frama.io/dev/tuto-project/tests/frameworks/pre_commit/pre_commit.html
    #
    # Calling examples
    #
    # - pre-commit run black
    # - pre-commit run isort
    #
    # continuous integration
    # ======================
    #
    # - pre-commit run --all-files
    #
    repos:
      - repo: https://github.com/ambv/black
        rev: 19.3b0
        hooks:
        - id: black
          language_version: python3.7


      - repo: https://github.com/pre-commit/pre-commit-hooks
        rev: v2.1.0
        hooks:
        - id: trailing-whitespace
          description: This hook trims trailing whitespace.
        - id: detect-private-key
        - id: mixed-line-ending
          args: [--fix=lf]
          description: Forces to replace line ending by the UNIX 'lf' character.



black in a makefile
=====================

.. code-block:: makefile
   :linenos:

    # makefile for quality code.
    #
    # ATTENTION:
    #
    # - le marqueur est la tabulation
    #
    # Exemples
    # =========
    # https://github.com/kennethreitz/requests/blob/master/Makefile
    #

    # https://www.gnu.org/prep/standards/html_node/Makefile-Basics.html#Makefile-Basics
    SHELL = /bin/bash

    # Put it first so that "make" without argument is like "make help".
    help:
        @echo " "
        @echo "Targets:"
        @echo " "
        @echo "- make black"
        @echo "- make check_all_files"
        @echo "- make precommit_autoupdate"
        @echo " "


    black:
        pre-commit run black


    check_all_files:
        pre-commit run --all-files

    precommit_autoupdate:
        pre-commit autoupdate


    .PHONY: help  Makefile

    # Catch-all target:
    %: Makefile
        echo "Hello World !"
