.. index::
   ! pyright

.. _vscodium_pyright_project:

======================
VSCodium/pyright
======================

- https://github.com/Microsoft/pyright
- https://github.com/microsoft/pyright/graphs/contributors
