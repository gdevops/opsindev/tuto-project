.. index::
   pair: Gitlab ; Software Quality

.. _gitlab_software_quality:

=========================
Gitlab Software Quality
=========================

.. seealso::

   - https://about.gitlab.com/handbook/engineering/quality/
