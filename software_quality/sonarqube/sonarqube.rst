.. index::
   pair: sonarqube; framework
   pair: continuous ; inspection
   ! sonarqube

.. _sonarqube:

=======================================================
Sonarqube : **continuous inspection** of code quality
=======================================================

.. seealso::

   - https://github.com/SonarSource
   - https://github.com/SonarSource/sonarqube
   - https://www.sonarqube.org/
   - https://blog.sonarsource.com/
   - https://x.com/SonarQube
   - https://fr.wikipedia.org/wiki/SonarQube

.. figure:: icone_sonarsource.png
   :align: center
   :width: 100


.. toctree::
   :maxdepth: 3

   definition/definition
   python/python
   sonarlint/sonarlint
   versions/versions
