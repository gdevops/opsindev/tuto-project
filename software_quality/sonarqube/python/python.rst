

.. index::
   pair: Python plugin ; sonarqube


.. _sonarqube_python:

=======================================================================
SonarQube Python plugin (https://github.com/SonarSource/sonar-python)
=======================================================================

.. seealso::

   - https://github.com/SonarSource/sonar-python
   - https://www.sonarsource.com/products/codeanalyzers/sonarpython.html
   - https://x.com/SonarQube/status/1105886668446404609 (12 mars 2019: After SonarPHP, it's time for SonarPython and SonarJS to support Security Hotspot rules)
   - https://rules.sonarsource.com/python
   - https://rules.sonarsource.com/python/type/Security%20Hotspot





Description
=============

SonarSource delivers what is probably the best static code analyzer you
can find on the market for Python.

Based on our own Python analyzer, it can find code smells, bugs and
security vulnerabilities.

As for any product we develop at SonarSource, it was built on the
following principles: depth, accuracy and speed.

SonarPython covers some well-established quality standards.

The SonarPython capability is available in Eclipse and IntelliJ for
developers (SonarLint) as well as throughout the development chain for
automated code review with self-hosted SonarQube or on-line SonarCloud.


Use in
=========

VSCodium
----------

.. seealso::

   - :ref:`vscodium`
   - :ref:`sonarlint`


News
=====


12 mars 2019: After SonarPHP, it's time for SonarPython and SonarJS to support Security Hotspot rules
---------------------------------------------------------------------------------------------------------

.. seealso::

   - https://x.com/SonarQube/status/1105886668446404609 ()
