.. index::
   pair: sonarqube; definition

.. _sonarqube_def:

=======================================================
Sonarqube definition
=======================================================



Definition
===========

.. seealso::

   - https://en.wikipedia.org/wiki/SonarQube
   - https://github.com/SonarSource


**SonarQube** (formerly Sonar) is an open-source platform developed by
SonarSource for **continuous inspection** of code quality to perform
automatic reviews with static analysis of code to:

- detect bugs,
- code smells, and
- security vulnerabilities

on 20+ programming languages.

SonarQube offers reports on:

- duplicated code,
- coding standards,
- unit tests,
- code coverage,
- code complexity,
- comments,
- bugs,
- and security vulnerabilities.
