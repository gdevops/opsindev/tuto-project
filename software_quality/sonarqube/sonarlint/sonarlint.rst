.. index::
   ! sonarlint

.. _sonarlint:

===========================
Sonarlint (IDE extension)
===========================

.. seealso::

   - https://github.com/SonarSource/sonarlint-vscode
   - https://github.com/SonarSource





Definition
===========

.. seealso::

   - https://en.wikipedia.org/wiki/SonarQube
   - https://github.com/SonarSource


SonarLint is an IDE extension that helps you detect and fix quality
issues as you write code in JavaScript, TypeScript, Python and PHP.


Sonarlint in VSCode
=====================

.. figure:: sonarlint_in_vscode.png
   :align: center


Prerequisite
==============

Java openjdk-8-jre
---------------------

::

    sudo apt install openjdk-8-jre

::

    Lecture des listes de paquets... Fait
    Construction de l'arbre des dépendances
    Lecture des informations d'état... Fait
    Les paquets suivants ont été installés automatiquement et ne sont plus nécessaires :
    linux-headers-4.15.0-44 linux-headers-4.15.0-44-generic linux-image-4.15.0-44-generic linux-modules-4.15.0-44-generic linux-modules-extra-4.15.0-44-generic
    Veuillez utiliser « sudo apt autoremove » pour les supprimer.
    Les paquets supplémentaires suivants seront installés :
    openjdk-8-jre-headless
    Paquets suggérés :
    icedtea-8-plugin fonts-ipafont-gothic fonts-ipafont-mincho fonts-wqy-zenhei
    Les NOUVEAUX paquets suivants seront installés :
    openjdk-8-jre openjdk-8-jre-headless
    0 mis à jour, 2 nouvellement installés, 0 à enlever et 0 non mis à jour.
    Il est nécessaire de prendre 27,4 Mo dans les archives.
    Après cette opération, 100 Mo d'espace disque supplémentaires seront utilisés.
    Souhaitez-vous continuer ? [O/n] O
    Réception de:1 http://mirror.ubuntu.ikoula.com/ubuntu bionic-updates/universe amd64 openjdk-8-jre-headless amd64 8u191-b12-2ubuntu0.18.04.1 [27,3 MB]
    Réception de:2 http://mirror.ubuntu.ikoula.com/ubuntu bionic-updates/universe amd64 openjdk-8-jre amd64 8u191-b12-2ubuntu0.18.04.1 [69,7 kB]
    27,4 Mo réceptionnés en 2min 27s (186 ko/s)
    Sélection du paquet openjdk-8-jre-headless:amd64 précédemment désélectionné.
    (Lecture de la base de données... 385975 fichiers et répertoires déjà installés.)
    Préparation du dépaquetage de .../openjdk-8-jre-headless_8u191-b12-2ubuntu0.18.04.1_amd64.deb ...
    Dépaquetage de openjdk-8-jre-headless:amd64 (8u191-b12-2ubuntu0.18.04.1) ...
    Sélection du paquet openjdk-8-jre:amd64 précédemment désélectionné.
    Préparation du dépaquetage de .../openjdk-8-jre_8u191-b12-2ubuntu0.18.04.1_amd64.deb ...
    Dépaquetage de openjdk-8-jre:amd64 (8u191-b12-2ubuntu0.18.04.1) ...
    Traitement des actions différées (« triggers ») pour mime-support (3.60ubuntu1) ...
    Traitement des actions différées (« triggers ») pour desktop-file-utils (0.23+linuxmint4) ...
    Traitement des actions différées (« triggers ») pour libc-bin (2.27-3ubuntu1) ...
    Traitement des actions différées (« triggers ») pour gnome-menus (3.13.3-11ubuntu1.1) ...
    Traitement des actions différées (« triggers ») pour hicolor-icon-theme (0.17-2) ...
    Paramétrage de openjdk-8-jre-headless:amd64 (8u191-b12-2ubuntu0.18.04.1) ...
    Paramétrage de openjdk-8-jre:amd64 (8u191-b12-2ubuntu0.18.04.1) ...
    update-alternatives: utilisation de « /usr/lib/jvm/java-8-openjdk-amd64/jre/bin/policytool » pour fournir « /usr/bin/policytool » (policytool) en mode automatique
    Traitement des actions différées (« triggers ») pour libc-bin (2.27-3ubuntu1) ...
