.. index::
   ! isort

.. _isort:

======================
isort
======================




Installation
==============

::

    pipenv install isort --dev

::

    Installing isort…
    Adding isort to Pipfile's [dev-packages]…
    ✔ Installation Succeeded
    Pipfile.lock (d2c84d) out of date, updating to (8a8c7e)…
    Locking [dev-packages] dependencies…
    ✔ Success!
    Locking [packages] dependencies…
    ✔ Success!
    Updated Pipfile.lock (d2c84d)!
