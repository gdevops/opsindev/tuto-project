
.. _wily_installation:

==================================================================================================
Wily installation
==================================================================================================

.. seealso::

   - https://github.com/tonybaloney/wily





Installation
=============

::

    pipenv install wily


::

    pipenv graph



::

    wily==1.12.1
      - click [required: Any, installed: 7.0]
      - colorlog [required: Any, installed: 4.0.2]
      - gitpython [required: Any, installed: 2.1.11]
        - gitdb2 [required: >=2.0.0, installed: 2.0.5]
          - smmap2 [required: >=2.0.0, installed: 2.0.5]
      - plotly [required: Any, installed: 3.7.0]
        - decorator [required: >=4.0.6, installed: 4.3.2]
        - nbformat [required: >=4.2, installed: 4.4.0]
          - ipython-genutils [required: Any, installed: 0.2.0]
          - jsonschema [required: >=2.4,!=2.5.0, installed: 3.0.1]
            - attrs [required: >=17.4.0, installed: 19.1.0]
            - pyrsistent [required: >=0.14.0, installed: 0.14.11]
              - six [required: Any, installed: 1.12.0]
            - setuptools [required: Any, installed: 40.8.0]
            - six [required: >=1.11.0, installed: 1.12.0]
          - jupyter-core [required: Any, installed: 4.4.0]
            - traitlets [required: Any, installed: 4.3.2]
              - decorator [required: Any, installed: 4.3.2]
              - ipython-genutils [required: Any, installed: 0.2.0]
              - six [required: Any, installed: 1.12.0]
          - traitlets [required: >=4.1, installed: 4.3.2]
            - decorator [required: Any, installed: 4.3.2]
            - ipython-genutils [required: Any, installed: 0.2.0]
            - six [required: Any, installed: 1.12.0]
        - pytz [required: Any, installed: 2018.9]
        - requests [required: Any, installed: 2.21.0]
          - certifi [required: >=2017.4.17, installed: 2019.3.9]
          - chardet [required: >=3.0.2,<3.1.0, installed: 3.0.4]
          - idna [required: >=2.5,<2.9, installed: 2.8]
          - urllib3 [required: >=1.21.1,<1.25, installed: 1.24.1]
        - retrying [required: >=1.3.3, installed: 1.3.3]
          - six [required: >=1.7.0, installed: 1.12.0]
        - six [required: Any, installed: 1.12.0]
      - progress [required: Any, installed: 1.5]
      - radon [required: Any, installed: 3.0.1]
        - colorama [required: >=0.4,<0.5, installed: 0.4.1]
        - flake8-polyfill [required: Any, installed: 1.0.2]
          - flake8 [required: Any, installed: 3.7.7]
            - entrypoints [required: >=0.3.0,<0.4.0, installed: 0.3]
            - mccabe [required: >=0.6.0,<0.7.0, installed: 0.6.1]
            - pycodestyle [required: >=2.5.0,<2.6.0, installed: 2.5.0]
            - pyflakes [required: >=2.1.0,<2.2.0, installed: 2.1.1]
        - mando [required: >=0.6,<0.7, installed: 0.6.4]
          - six [required: Any, installed: 1.12.0]
      - tabulate [required: Any, installed: 0.8.3]
