.. index::
   pair: Wily ; Versions

.. _wily_versions:

=====================
Wily versions
=====================

.. seealso::

   - https://github.com/tonybaloney/wily/releases


.. toctree::
   :maxdepth: 3

   1.12.2/1.12.2
