.. index::
   pair: Wily ; Software engineering
   ! Wily

.. _wily_software:

==================================================================================================
Wily (A Python application for tracking, reporting on timing and complexity in Python code)
==================================================================================================

.. seealso::

   - https://github.com/tonybaloney/wily
   - https://realpython.com/python-refactoring/#using-wily-to-capture-and-track-your-projects-complexity


.. toctree::
   :maxdepth: 3

   definition/definition
   installation/installation
   versions/versions
   videos/videos
