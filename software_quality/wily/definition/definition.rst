

.. _wily_def:

==================================================================================================
Wily definition
==================================================================================================

.. seealso::

   - https://github.com/tonybaloney/wily
   - https://realpython.com/python-refactoring/#using-wily-to-capture-and-track-your-projects-complexity





wily --help
==============

::

    Usage: wily [OPTIONS] COMMAND [ARGS]...

      Version: 1.12.1

      🦊 Inspect and search through the complexity of your source code.

      To get started, run setup:

        $ wily setup

      To reindex any changes in your source code:

        $ wily build <src>

      Then explore basic metrics with:

        $ wily report <file>

      You can also graph specific metrics in a browser with:

        $ wily graph <file> <metric>

    Options:
      -V, --version         Show the version and exit.
      --debug / --no-debug  Print debug information, used for development
      --config TEXT         Path to configuration file, defaults to wily.cfg
      -p, --path PATH       Root path to the project folder to scan
      -c, --cache PATH      Override the default cache path (defaults to
                            $HOME/.wily/HASH)
      --help                Show this message and exit.

    Commands:
      build         Build the wily cache.
      clean         Clear the .wily/ folder.
      diff          Show the differences in metrics for each file.
      graph         Graph a specific metric for a given file, if a path is...
      index         Show the history archive in the .wily/ folder.
      list-metrics  List the available metrics.
      report        Show metrics for a given file.
      setup         Run a guided setup to build the wily cache.



Usage
======

::


    pipenv shell
    wily build intranet


::

    Defaulting back to the filesystem archiver, not a valid git repo
    Found 0 revisions from 'filesystem' archiver in '/home/pvergain/projects/intranet/intranet/docker-django'.
    Running operators - cyclomatic,halstead,raw,maintainability
    Processing
    Completed building wily history, run `wily report <file>` or `wily index` to see more.


::

    wily report intranet


::

    Using default metrics ['raw.loc', 'maintainability.mi', 'halstead.h1', 'cyclomatic.complexity']
    -----------History for ['raw.loc', 'maintainability.mi', 'halstead.h1', 'cyclomatic.complexity']------------
    ╒════════════╤════════════╤════════════╤═════════════════╤═════════════════════════╤═══════════════════╤═════════════════════════╕
    │ Revision   │ Author     │ Date       │ Lines of Code   │ Maintainability Index   │ Unique Operands   │ Cyclomatic Complexity   │
    ╞════════════╪════════════╪════════════╪═════════════════╪═════════════════════════╪═══════════════════╪═════════════════════════╡
    │ d341822    │ Local User │ 2019-03-07 │ 32662 (0)       │ 87.6952 (0)             │ 390 (0)           │ 10.1028 (0)             │
    ╘════════════╧════════════╧════════════╧═════════════════╧═════════════════════════╧═══════════════════╧═════════════════════════╛
