.. index::
   pair: Wily ; videos
   pair: PyCon 2019; wily

.. _wily_videos:

=========================
Wily videos
=========================

.. contents::
  :depth: 3


Anthony Shaw - Wily Python: Writing simpler and more maintainable Python - PyCon 2019
======================================================================================


.. seealso::

   - https://www.youtube.com/watch?v=dqdsNoApJ80


Everyone starts with the best intentions with their Python projects, "
"this time it's going to be clean, simple and maintainable"".

But code evolves over time, requirements change and codebases can get messy and
complicated quickly.

In this talk, you will learn how to use `wily` to measure and graph how
complicated your Python code is and a series of practical techniques
to simplify it.

`wily` will show you which parts of your projects are becoming or have become
hard to maintain and need a refactor.

Once you know where the skeletons are, you will learn practical techniques
for refactoring ""complex"" code and some resources to use to take your
refactoring to the next level.

Slides can be found at:

- https://speakerdeck.com/pycon2019 and
- https://github.com/PyCon/2019-slides
