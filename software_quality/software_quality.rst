.. index::
   ! Software Quality

.. _software_qaulity:

======================
Quality
======================




Best practices
================

.. toctree::
   :maxdepth: 3

   best_practices/best_practices

Frameworks
===========

.. toctree::
   :maxdepth: 4

   mypy/mypy
   sonarqube/sonarqube
   wily/wily

Tools
======

.. toctree::
   :maxdepth: 4

   black/black
   gitlab/gitlab
   isort/isort
   pyright/pyright

