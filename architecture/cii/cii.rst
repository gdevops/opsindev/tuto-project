
.. index::
   pair: coreinfrastructure ; Design

.. _coreinfrastructure_design:

================================================================
coreinfrastructure Design
================================================================

.. seealso::

   - https://github.com/coreinfrastructure/best-practices-badge/blob/master/doc/design.md
   - https://engineering.videoblocks.com/web-architecture-101-a3224e126947



Design
=======

This document describes the design of the BadgeApp.

No single technology does everything well. Instead, design is all about making
good choices between options that have different trade-offs, so that together
those choices will efficiently meet requirements.
As explained in You Are Not Google, "if you're using a technology that originated
at a large company, but your use case is very different, it's unlikely that
you arrived there deliberately... what's important is that you actually use
the right tool for the job." This document describes our key choices.

Our design is generally standard and straightforward. If you're not familiar
with how web applications are typically designed, see introductory material s
uch as `"Web Architecture 101"`_ by Jonathan Fulton.


.. _`"Web Architecture 101"`:  https://engineering.videoblocks.com/web-architecture-101-a3224e126947
