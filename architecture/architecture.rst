
.. index::
   pair: Project ; Architecture

.. _project_architecture:

=======================
Architecture
=======================

.. seealso::

   - https://en.wikipedia.org/wiki/Software_architecture
   - https://fr.wikipedia.org/wiki/Architecture_logicielle

.. toctree::
   :maxdepth: 3


   architecte/architecte
   binhnguyennus/binhnguyennus
   cii/cii
   donnemartin/donnemartin
   physical_architecture/physical_architecture
