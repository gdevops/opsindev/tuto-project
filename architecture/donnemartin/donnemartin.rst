
.. index::
   pair: Donnemartin ; Architecture

.. _donnemartin_tutorials:

================================================================
Donnemartin tutorial (Learn how to design large-scale systems)
================================================================

.. seealso::

   - https://github.com/donnemartin/system-design-primer


Description
============

Learn how to design large-scale systems.
