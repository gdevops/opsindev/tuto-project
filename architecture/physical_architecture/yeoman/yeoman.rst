
.. index::
   pair: Physical Architecture; yeoman
   pair: CLI; yeoman


.. _yeoman:

====================================================================================================
yeoman (a set of tools for automating development workflow)
====================================================================================================

.. seealso::

   - https://yeoman.io/
   - https://github.com/yeoman/yeoman
   - https://x.com/yeoman


.. toctree::
   :maxdepth: 3

   templates/templates
