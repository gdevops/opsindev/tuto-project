
.. index::
   pair: generator_python; yeoman
   pair: John ; Freeman

.. _yeoman_generator_python:

====================================================================================================
Yeoman generator-python (A Yeoman generator for Python projects)
====================================================================================================

.. seealso::

   - https://jfreeman.dev/blog/2019/05/13/python-on-rails/
   - https://github.com/thejohnfreeman/generator-python
   - https://x.com/thejohnfreeman
   - https://jfreeman.dev/


.. toctree::
   :maxdepth: 3

   definition/definition
   examples/examples
