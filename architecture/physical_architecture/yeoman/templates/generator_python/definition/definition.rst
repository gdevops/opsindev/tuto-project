

.. _yeoman_generator_python_def:

====================================================================================================
generator-python definition
====================================================================================================

.. seealso::

   - https://jfreeman.dev/blog/2019/05/13/python-on-rails/
   - https://github.com/thejohnfreeman/generator-python
   - https://github.com/thejohnfreeman/generator-python/blob/master/README.md



generator-python
================

A `Yeoman <https://yeoman.io/>`__ generator for a Python 3 package with
docs, lint, tests, and continuous integration.

|npm| |code style: Prettier|

Preview
-------

Take a look at the `sample
project <https://github.com/thejohnfreeman/project-template-python>`__
built with this generator.

It is tested on `Travis CI <https://travis-ci.org/thejohnfreeman/project-template-python>`__ and
published on `PyPI <https://pypi.org/project/project_template/>`__ with
documentation on `Read the Docs <https://project-template-python.readthedocs.io/>`__.

Install
-------

.. code:: shell

    $ yarn global add yeoman @thejohnfreeman/generator-python

This generator assumes you are using
`Poetry <https://poetry.eustace.io/>`__ for managing Python packages. It
will use whatever Python version is activated in your environment. I
recommend using `pyenv <https://github.com/pyenv/pyenv>`__ to manage
different versions of Python.

.. code:: shell

    $ curl -sSL https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py | python

Use
---

From within your project directory:

.. code:: shell

    $ yo python

Options
-------

Although there are some parameters to this generator, such as the
package name and author, there are no optional pieces. Instead, I
recommend you include every generated file in your first commit, then
delete the files you don't want in your second commit, and go from
there. That way, if you ever change your mind later, you can resurrect
the file that was generated for you, not by re-running the generator and
potentially overwriting other files, but by `asking Git for the version
you
deleted <https://stackoverflow.com/a/1113140/61890://stackoverflow.com/a/1113140/618906>`__:

.. code:: shell

    $ git checkout $(git rev-list -1 HEAD -- "$file")^ -- "$file"



Why not `Cookiecutter <https://github.com/audreyr/cookiecutter>`__?
-------------------------------------------------------------------

I explain my reasons on `my
blog <https://jfreeman.dev/blog/2019/04/24/cookiecutter-vs-yeoman/>`__.

.. |npm| image:: https://img.shields.io/npm/v/@thejohnfreeman/generator-python.svg
   :target: https://www.npmjs.com/package/@thejohnfreeman/generator-python
.. |code style: Prettier| image:: https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat
   :target: https://github.com/prettier/prettier
