
.. index::
   pair: Physical Architecture; Python
   pair: Physical Architecture; Gitlab

.. _archi_python_layout:

====================================================================================================
Python/Django layout
====================================================================================================

.. seealso::

   - https://docs.python-guide.org/writing/structure/



Python
=======

.. seealso::

   - :ref:`cookiecutter_python`
   - :ref:`yeoman_generator_python`

Django
=======

.. seealso::

   - :ref:`python_django`
   - :ref:`cookicutter_ddpt`


Python/gitlab
================

..seealso::

..   - :ref:`python_gitlab_cookie`
