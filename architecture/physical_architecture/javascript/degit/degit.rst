
.. index::
   pair: Physical Architecture; degit

.. _degit:

====================================================================================================
degit (Straightforward project scaffolding)
====================================================================================================

.. seealso::

   - https://github.com/Rich-Harris/degit


.. toctree::
   :maxdepth: 3

   definition/definition
