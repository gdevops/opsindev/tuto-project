
.. index::
   pair: Physical ; Architecture

.. _physical_architecture:

=======================
Physical architecture
=======================

.. seealso::

   - https://jfreeman.dev/blog/2019/04/24/cookiecutter-vs-yeoman/

.. toctree::
   :maxdepth: 3

   cookiecutter/cookiecutter
   yeoman/yeoman
   javascript/javascript
   python/python
