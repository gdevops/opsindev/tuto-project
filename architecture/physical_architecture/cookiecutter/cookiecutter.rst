
.. index::
   pair: Physical Architecture; cookiecutter
   pair: CLI; cookiecutter


.. _cookiecutter:

====================================================================================================
cookiecutter (A command-line utility that creates projects from cookiecutters (project templates))
====================================================================================================

.. seealso::

   - https://github.com/audreyr/cookiecutter


.. toctree::
   :maxdepth: 3

   definition/definition
