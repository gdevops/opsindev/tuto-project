
.. index::
   pair: Project ; Architecte

.. _architecte:

=======================
Architecte
=======================

.. seealso::

   - https://fr.wikipedia.org/wiki/Architecte_informatique




Définition
===========


En génie informatique, on appelle architecte informatique la personne chargée
de l'analyse technique nécessaire à la conception du diagramme d'architecture,
c'est-à-dire le plan de construction d'un logiciel, d'un réseau, d'une base de
données, etc.

Un architecte informatique peut travailler avec les développeurs du système
actuel ou d'autres architectes informatique et produire par exemple les
diagrammes suivants :

- diagramme d'architecture : un diagramme qui décrit dans les grandes lignes
  les acteurs d'un système informatique - humains ou machines, leur rôle,
  les flux d'informations entre les différents acteurs et les protocoles.
- diagramme d'architecture applicatif : diagramme qui décrit les flux
  d'informations entre un logiciel donné, et les autres acteurs du système
  d'information dans lequel il va être implanté. (usagers, autres logiciels:
  SGBD, annuaire,…)
- diagramme d'architecture technique : diagramme qui décrit les flux
  d'informations entre les différentes pièces d'un logiciel.
- diagramme d'architecture physique : diagramme qui décrit les flux
  d'informations entre les différents appareils d'un système d'informations.
  (serveurs, ordinateurs personnels, routeurs, …)

