
.. index::
   pair: scalability ; Architecture

.. _binhnguyennus_awesome_scalability:

================================================================
binhnguyennus/awesome-scalability
================================================================

.. seealso::

   - https://github.com/binhnguyennus/awesome-scalability




Description
============

The Patterns Behind Scalable, Reliable, and Performant Large-Scale Systems.
